/**
 * Created by hulusionder on 06/10/2021.
 */

"use strict";

//#region Imports

import crypto from "crypto";
import {
    BinaryLike,
    CipherKey
} from "crypto";

//#endregion

export class CryptoHelper {
    //#region Variables

    private static _instance: CryptoHelper;

    private readonly algorithm!: string;
    private readonly iv!: BinaryLike;
    private readonly key!: CipherKey;

    //#endregion

    //#region Getters - Setters

    public static get instance(): CryptoHelper {
        if (!this._instance) {
            this._instance = new CryptoHelper();
        }

        return this._instance;
    }

    //#endregion

    //#region Constructor

    protected constructor() {
        const cryptoAlgorithm =
            process.env.CRYPTO_ALGORITHM || "";
        const cryptoSecretIV =
            process.env.CRYPTO_SECRET_IV || "";
        const cryptoSecretKey =
            process.env.CRYPTO_SECRET_KEY || "";

        this.algorithm = cryptoAlgorithm;
        this.iv = Buffer.from(cryptoSecretIV, "hex");
        this.key = Buffer.from(cryptoSecretKey, "hex");
    }

    //#endregion

    //#region Methods

    //#region Encrypt Methods

    public encrypt(
        text: string): string {
        try {
            let cipher = crypto.createCipheriv(
                this.algorithm, this.key, this.iv);
            let encryptedText = cipher.update(text, "utf8", "base64");
            encryptedText += cipher.final("base64");

            return encryptedText;
        } catch (exception) {
            return "";
        }
    }

    //#endregion

    //#region Decrypt Methods

    public decrypt(
        text: string): string {
        try {
            let decipher = crypto.createDecipheriv(
                this.algorithm, this.key, this.iv);
            let decryptedText = decipher.update(text, "base64", "utf8");
            decryptedText += decipher.final("utf8");

            return decryptedText;
        } catch (exception) {
            return "";
        }
    }

    //#endregion

    //#endregion
}
