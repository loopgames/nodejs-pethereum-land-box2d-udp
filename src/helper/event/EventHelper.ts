/**
 * Created by hulusionder on 10/02/2022
 */

"use strict";

//#region Imports

import { EventEmitter } from "events";

//#endregion

export class EventHelper
    extends EventEmitter {
    //#region Variables

    private static _instance: EventHelper;

    //#endregion

    //#region Getters - Setters

    public static get instance(): EventHelper {
        if (!this._instance) {
            this._instance = new EventHelper();
        }

        return this._instance;
    }

    //#endregion

    //#region Constructor

    constructor() {
        super();
    }

    //#endregion

    //#region Methods


    //#endregion
}
