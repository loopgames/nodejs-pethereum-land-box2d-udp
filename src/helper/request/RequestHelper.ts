/**
 * Created by hulusionder on 06/10/2021.
 */

"use strict";

//#region Imports

import axios, {AxiosRequestConfig} from "axios";

import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {AxiosResponseInterface} from "../../interface/request/AxiosResponseInterface";

//#endregion

export class RequestHelper {
    //#region Variables

    private static _instance: RequestHelper;

    //#endregion

    //#region Constructor

    private constructor() {

    }

    //#endregion

    //#region Singleton Methods

    public static get instance(): RequestHelper {
        if (!this._instance) {
            this._instance = new RequestHelper();
        }

        return this._instance;
    }

    //#endregion

    //#region Methods

    //#region Send Request

    public async sendGetRequest(
        url: string,
        config?: AxiosRequestConfig): Promise<AxiosResponseInterface> {
        try {
            const response =
                await axios.get(url, config);

            return {
                isSuccess: true,
                status: response.status,
                data: response.data
            }
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            }
        }
    }

    public async sendPostRequest(
        url: string,
        data?: any,
        config?: AxiosRequestConfig): Promise<AxiosResponseInterface> {
        try {
            const response =
                await axios.post(url, data, config);

            return {
                isSuccess: true,
                status: response.status,
                data: response.data
            }
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            }
        }
    }

    //#endregion

    //#endregion
}
