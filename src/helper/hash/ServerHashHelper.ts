/**
 * Created by hulusionder on 01/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import {v1 as uuidv1} from "uuid";
import nodeForge from "node-forge";

import {LogManager} from "../../manager/LogManager";

//#endregion

export class ServerHashHelper {
    //#region Variables

    private static _instance: ServerHashHelper;

    private readonly logManager!: LogManager;

    private readonly hashSaltPassword!: string;
    private readonly hashes!: Map<any, any>;

    //#endregion

    //#region Constructor

    private constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);

        const hashSaltPassword =
            process.env.HASH_SALT_PASSWORD_SERVER || "";

        this.hashSaltPassword = hashSaltPassword;

        this.hashes = new Map();
    }

    //#endregion

    //#region Singleton Methods

    public static get instance(): ServerHashHelper {
        if (!this._instance) {
            this._instance = new ServerHashHelper();
        }

        return this._instance;
    }

    //#endregion

    //#region Methods

    private isHashUsed(authorization: string, hash: string): boolean {
        const key = authorization.toString();

        if (Array.isArray(this.hashes.get(key))) {
            for (let oldHash of this.hashes.get(key))
                if (oldHash === hash) {
                    this.logManager.showWarn({
                        warning: util.format("User with authorization: %s sent same request!",
                            authorization)
                    });

                    return true;
                }
        }

        return false;
    }

    private createHashedData(authorization: string, data: any): { data: any, hash: string } {
        const salt = uuidv1();

        let hashedData = {} as any;
        hashedData.data = data;
        hashedData.data.salt = salt;
        hashedData.hash =
            this.getHash(authorization, JSON.stringify(hashedData.data), salt);

        return hashedData;
    }

    private getHash(authorization: string, data: any, salt: string): string {
        const sha256 = nodeForge.md.sha256.create();
        const message = util.format("%s%s%s%s",
            this.hashSaltPassword, salt, authorization, data);

        sha256.update(message);
        return sha256.digest().toHex();
    }

    private validateHash(authorization: string, data: any, hash: string, salt: string): boolean {
        if (!authorization ||
            !data ||
            !hash ||
            !salt) return false;

        const sha256 = nodeForge.md.sha256.create();
        const message = util.format("%s%s%s%s",
            this.hashSaltPassword, salt, authorization, data);

        sha256.update(message);

        const result = sha256.digest().toHex();
        return (result === hash);
    }

    private insertHash(authorization: string, hash: string): void {
        const key = authorization.toString();
        const hashArray = this.hashes.get(key);

        if (Array.isArray(hashArray)) {
            hashArray.unshift(hash);

            if (this.hashes.get(key).length > 2000) {
                hashArray.pop();
            }

            this.hashes.set(key, hashArray);
        } else {
            const hashArrayTemp = [];
            hashArrayTemp.push(hash);
            this.hashes.set(key, hashArrayTemp);

            if (this.hashes.size > 2000 + 100) {
                let counter = 0;

                for (let hashKey of this.hashes.keys()) {
                    this.hashes.delete(hashKey);

                    counter++;
                    if (counter > 200) return;
                }
            }
        }
    }

    public validateHashedRequest(request: any, response: any, next: any): any {
        let requestHeaders = request.headers;
        if (!requestHeaders) {
            requestHeaders = {};
        }

        let authorization = requestHeaders["authorization"];
        if (!authorization) {
            authorization = "";
        }

        let hash = requestHeaders["hash"];
        if (!hash) {
            hash = "";
        }

        let requestBody = request.body;
        if (!requestBody) {
            requestBody = {};
        }

        let salt = requestBody.salt;
        if (!salt) {
            salt = "";
        }

        let data = JSON.stringify(requestBody);
        if (!data) {
            data = "";
        }

        if (this.validateHash(authorization, data, hash, salt)) {
            if (!this.isHashUsed(authorization, hash)) {
                this.insertHash(authorization, hash);
                next();
            } else {
                this.logManager.showError({
                    error: util.format("Hash is already used! Authorization: %s",
                        authorization)
                });

                return response
                    .status(400)
                    .send("Duplicate request");
            }
        } else {
            this.logManager.showWarn({
                warning: util.format("Cannot validate hash for URL: %s - Authorization: %s",
                    request.url, authorization)
            });

            return response
                .status(400)
                .send("Invalid hash");
        }
    }

    public createHashedResponse(request: any, response: any, next: any): void {
        let requestHeaders = request.headers;
        if (!requestHeaders) {
            requestHeaders = {};
        }

        let authorization = requestHeaders["authorization"];
        if (!authorization) {
            authorization = "";
        }

        let responseData = response.data;
        if (!responseData) {
            responseData = {};
        }

        const hashedResponse = this.createHashedData(authorization, responseData);

        response.set("hash", hashedResponse.hash);
        response.data = hashedResponse.data;

        next();
    }

    //#endregion

    //#region Util Methods


    //#endregion
}
