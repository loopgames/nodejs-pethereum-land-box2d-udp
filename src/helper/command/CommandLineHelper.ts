/**
 * Created by hulusionder on 06/10/2021.
 */

"use strict";

//#region Imports

import parseArgs from "minimist";

//#endregion

import crypto from "crypto";

export class CommandLineHelper {
    //#region Variables

    private static _instance: CommandLineHelper;

    private readonly commandLineArguments!: parseArgs.ParsedArgs;

    //#endregion

    //#region Getters - Setters

    public static get instance(): CommandLineHelper {
        if (!this._instance) {
            this._instance = new CommandLineHelper();
        }

        return this._instance;
    }

    //#endregion

    //#region Constructor

    protected constructor() {
        const args = process.argv.slice(2);

        this.commandLineArguments =
            parseArgs(args);
    }

    //#endregion

    //#region Methods

    //#region Get Command Line Argument Methods

    public getCommandLineArgument(
        argumentKey: string): any {
        try {
            return this.commandLineArguments[argumentKey];
        } catch (exception) {
            return undefined;
        }
    }

    //#endregion

    //#endregion
}
