/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import {Lock} from "redlock";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {RedisDatabase} from "../model/database/RedisDatabase";
import {RedisLock} from "../model/lock/RedisLock";
import {RedisLockResponseInterface} from "../interface/redis/RedisLockResponseInterface";
import {BaseController} from "./BaseController";

//#endregion

export class RedisLockController
    extends BaseController {
    //#region Variables

    private readonly redisDatabaseUri!: string;

    private readonly _redisDatabase!: RedisDatabase;
    private readonly _redisLock!: RedisLock;

    //#endregion

    //#region Getters - Setters

    get redisDatabase(): RedisDatabase {
        return this._redisDatabase;
    }

    get redisLock(): RedisLock {
        return this._redisLock;
    }

    //#endregion

    //#region Constructor

    constructor() {
        super();

        const redisDatabaseUri =
            process.env.REDIS_LOCK_URI || "";

        this.redisDatabaseUri = redisDatabaseUri;

        this._redisDatabase = new RedisDatabase();
        this._redisLock = new RedisLock();
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Redis lock controller initialize start!")
            });

            const redisDatabaseOptions = {
                lazyConnect: true
            };

            await this._redisDatabase.init(
                this.redisDatabaseUri, redisDatabaseOptions);

            const redisClient =
                this._redisDatabase.client;
            const redisLockClients = [
                redisClient
            ];
            const redisLockOptions = {
                driftFactor: 0.01,
                retryCount: 4,
                retryDelay: 200,
                retryJitter: 200,
                automaticExtensionThreshold: 500
            };

            await this._redisLock.init(
                redisLockClients, redisLockOptions);

            this.logManager.showLog({
                log: util.format("Redis lock controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Redis lock controller initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Redis lock controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Lock Methods

    //#region Acquire Lock Methods

    public async acquireLock(
        resources: string | string[],
        ttl: number)
        : Promise<RedisLockResponseInterface> {
        return await this._redisLock.acquireLock(
            resources, ttl);
    }

    //#endregion

    //#region Release Lock Methods

    public async releaseLock(
        lock: Lock)
        : Promise<RedisLockResponseInterface> {
        return await this._redisLock.releaseLock(
            lock);
    }

    //#endregion

    //#region Extend Lock Methods

    public async extendLock(
        lock: Lock,
        ttl: number)
        : Promise<RedisLockResponseInterface> {
        return await this._redisLock.extendLock(
            lock, ttl);
    }

    //#endregion

    //#endregion

    //#endregion
}
