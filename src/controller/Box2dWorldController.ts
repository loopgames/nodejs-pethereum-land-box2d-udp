/**
 * Created by hulusionder on 10/02/2022
 */

"use strict";

//#region Imports

import fs from "fs";
import path from "path";
import util from "util";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {Box2dWorld} from "../model/box2d/Box2dWorld";
import {BaseController} from "./BaseController";

//#endregion

export class Box2dWorldController
    extends BaseController {
    //#region Variables

    private readonly _box2dWorld!: Box2dWorld;

    //#endregion

    //#region Getters - Setters

    get box2dWorld(): Box2dWorld {
        return this._box2dWorld;
    }

    //#endregion

    //#region Constructor

    constructor() {
        super();

        this._box2dWorld = new Box2dWorld();
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Box2d world controller initialize start!")
            });

            const box2dWorldDataPath =
                path.join(__dirname, "../../box2dWorldData/box2d_world_data.json");

            const worldJsonString =
                fs.readFileSync(box2dWorldDataPath, "utf8");
            const worldJson =
                JSON.parse(worldJsonString);

            await this._box2dWorld.init(
                worldJson);

            this.logManager.showLog({
                log: util.format("Box2d world controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Box2d world controller initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Box2d world controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#endregion
}
