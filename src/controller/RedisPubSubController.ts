/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import Buffer from "buffer";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {RedisDatabase} from "../model/database/RedisDatabase";
import {RedisDatabaseResponseInterface} from "../interface/redis/RedisDatabaseResponseInterface";
import {BaseController} from "./BaseController";

//#endregion

export class RedisPubSubController
    extends BaseController {
    //#region Variables

    private readonly redisPublisherUri!: string;
    private readonly redisSubscriberUri!: string;

    private readonly _redisDatabasePublisher!: RedisDatabase;
    private readonly _redisDatabaseSubscriber!: RedisDatabase;

    //#endregion

    //#region Getters - Setters

    get redisDatabasePublisher(): RedisDatabase {
        return this._redisDatabasePublisher;
    }

    get redisDatabaseSubscriber(): RedisDatabase {
        return this._redisDatabaseSubscriber;
    }

    //#endregion

    //#region Constructor

    constructor() {
        super();

        const redisPublisherUri =
            process.env.REDIS_PUB_URI || "";
        const redisSubscriberUri =
            process.env.REDIS_SUB_URI || "";

        this.redisPublisherUri = redisPublisherUri;
        this.redisSubscriberUri = redisSubscriberUri;

        this._redisDatabasePublisher = new RedisDatabase();
        this._redisDatabaseSubscriber = new RedisDatabase();
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Redis pub sub controller initialize start!")
            });

            const redisDatabaseOptions = {
                lazyConnect: true
            };

            await this._redisDatabasePublisher.init(
                this.redisPublisherUri, redisDatabaseOptions);
            await this._redisDatabaseSubscriber.init(
                this.redisSubscriberUri, redisDatabaseOptions);

            this.logManager.showLog({
                log: util.format("Redis pub sub controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Redis pub sub controller initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Redis pub sub controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Publisher Methods

    //#region Publish Message Methods

    public async publishMessage(
        channel: string,
        message: string): Promise<RedisDatabaseResponseInterface> {
        return this._redisDatabasePublisher.publishMessage(
            channel, message);
    }

    //#endregion

    //#region Publish Buffer Methods

    public async publishBuffer(
        channel: string,
        buffer: Buffer): Promise<RedisDatabaseResponseInterface> {
        return this._redisDatabasePublisher.publishBuffer(
            channel, buffer);
    }

    //#endregion

    //#region Subscriber Methods

    //#region Subscribe Methods

    public async subscribe(
        args: string[]): Promise<RedisDatabaseResponseInterface> {
        return this._redisDatabaseSubscriber.subscribe(
            args);
    }

    //#endregion

    //#region Unsubscribe Methods

    public async unsubscribe(
        args: string[]): Promise<RedisDatabaseResponseInterface> {
        return this._redisDatabaseSubscriber.unsubscribe(
            args);
    }

    //#endregion

    //#endregion

    //#endregion

    //#endregion
}
