/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import fsExtra from "fs-extra";
import path from "path";
import util from "util";
import {deserialize} from "typescript-json-serializer";
import chokidar from "chokidar";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {CommandLineHelper} from "../helper/command/CommandLineHelper";
import {ServerJsonData} from "../model/serverJson/ServerJsonData";
import {BaseController} from "./BaseController";

//#endregion

export class ServerJsonController
    extends BaseController {
    //#region Variables

    private serverJsonFilePath!: string;

    private chokidarWatcher!: chokidar.FSWatcher;

    public _serverJsonData!: ServerJsonData;

    //#endregion

    //#region Getters - Setters

    get serverJsonData(): ServerJsonData {
        return this._serverJsonData;
    }

    //#endregion

    //#region Constructor

    constructor() {
        super();
    }

    //#endregion

    //#region Methods

    //#region Init

    public init(): void {
        try {
            this.logManager.showLog({
                log: util.format("Server json controller initialize start!")
            });

            const serverJsonCommandLineArgument =
                CommandLineHelper.instance.getCommandLineArgument("SERVER_JSON");

            const serverJsonFilePath =
                serverJsonCommandLineArgument || path.join(__dirname, "../../ServerID/server.json");

            this.serverJsonFilePath = serverJsonFilePath;

            this.readAndCreateServerJsonData(
                serverJsonFilePath);

            this.startWatchServerJsonChanges(
                serverJsonFilePath);

            this.logManager.showLog({
                log: util.format("Server json controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            throw new Error(
                util.format("Server json controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Server Json Methods

    //#region Read And Create Server Json Data

    private readAndCreateServerJsonData(
        serverJsonFilePath: string): void {
        const serverJsonFileData =
            fsExtra.readJSONSync(serverJsonFilePath, "utf8")

        const serverJsonData = deserialize<ServerJsonData>(
            serverJsonFileData, ServerJsonData);

        this._serverJsonData = serverJsonData;

        this.logManager.showLog({
            log: util.format("Server Json Data -> %s",
                JSON.stringify(this._serverJsonData))
        });
    }

    //#endregion

    //#region Start Watch Server Json Changes Methods

    private startWatchServerJsonChanges(
        serverJsonFilePath: string): void {
        this.chokidarWatcher =
            chokidar.watch(
                serverJsonFilePath,
                {
                    ignorePermissionErrors: true
                });

        this.chokidarWatcher.addListener("change", (path) => {
            if (path !== serverJsonFilePath) return;

            this.readAndCreateServerJsonData(
                serverJsonFilePath);
        });
    }

    //#endregion

    //#endregion

    //#endregion
}
