/**
 * Created by hulusionder on 10/02/2022
 */

"use strict";

//#region Imports

import util from "util";
import {Socket} from "dgram";
import {b2Body, b2Vec2} from "@box2d/core";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {ServerReturnCodes} from "../constant/server/ServerReturnCodes";
import {Serializer} from "../model/serializer/Serializer";
import {SerializationMode} from "../constant/serializer/SerializationMode";
import {UdpConstants} from "../constant/udp/UdpConstants";
import {Box2dConstants} from "../constant/box2d/Box2dConstants";
import {LobbyConstants} from "../constant/lobby/LobbyConstants";
import {ClientLobbyMessageTypes} from "../constant/lobby/ClientLobbyMessageTypes";
import {LobbyClientLocations} from "../constant/lobby/LobbyClientLocations";
import {EventHelper} from "../helper/event/EventHelper";
import {CryptoHelper} from "../helper/crypto/CryptoHelper";
import {Box2dBodyData} from "../model/box2d/Box2dBodyData";
import {Pet} from "../model/pet/Pet";
import {LobbyClient} from "../model/lobby/LobbyClient";
import {LobbyClientSenderInfo} from "../model/lobby/LobbyClientSenderInfo";
import {LobbyClientPosition} from "../model/lobby/LobbyClientPosition";
import {LobbyInputPayload} from "../model/lobby/LobbyInputPayload";
import {LobbyWorldStatePayload} from "../model/lobby/LobbyWorldStatePayload";
import {BaseLobbyRequestMessage} from "../model/lobby/message/BaseLobbyRequestMessage";
import {JoinLobbyRequestMessage} from "../model/lobby/message/JoinLobbyRequestMessage";
import {JoinLobbySelfResponseMessage} from "../model/lobby/message/JoinLobbySelfResponseMessage";
import {JoinLobbyOtherResponseMessage} from "../model/lobby/message/JoinLobbyOtherResponseMessage";
import {LeaveLobbyRequestMessage} from "../model/lobby/message/LeaveLobbyRequestMessage";
import {LeaveLobbySelfResponseMessage} from "../model/lobby/message/LeaveLobbySelfResponseMessage";
import {LeaveLobbyOtherResponseMessage} from "../model/lobby/message/LeaveLobbyOtherResponseMessage";
import {ChangePetLobbyRequestMessage} from "../model/lobby/message/ChangePetLobbyRequestMessage";
import {ChangePetLobbySelfResponseMessage} from "../model/lobby/message/ChangePetLobbySelfResponseMessage";
import {ChangePetLobbyOtherResponseMessage} from "../model/lobby/message/ChangePetLobbyOtherResponseMessage";
import {EnterMiniGameLobbyRequestMessage} from "../model/lobby/message/EnterMiniGameLobbyRequestMessage";
import {MoveLobbyRequestMessage} from "../model/lobby/message/MoveLobbyRequestMessage";
import {UpdateLobbyClientStateResponseMessage} from "../model/lobby/message/UpdateLobbyClientStateResponseMessage";
import {UpdateLobbyWorldStateResponseMessage} from "../model/lobby/message/UpdateLobbyWorldStateResponseMessage";
import {LobbyResponse} from "../model/response/LobbyResponse";
import {BaseController} from "./BaseController";
import {ReferenceController} from "./ReferenceController";

//#endregion

export class LobbyController
    extends BaseController {
    //#region Variables

    private socket!: Socket;

    private updateLobbyBox2dLoopObject!: any;
    private updateLobbyBox2dIntervalObject!: NodeJS.Timer;
    private updateLobbyClientsStateIntervalObject!: NodeJS.Timer;

    private updateWorldStateFrameCounter!: number;

    private lobbyClients!: Map<string, LobbyClient>;
    private lobbyClientsSenderInfo!: Map<string, LobbyClientSenderInfo>;

    private lobbyClientsBox2dBody!: Map<string, b2Body>;

    private lobbyClientsLastProcessedInput!: Map<string, LobbyInputPayload>;
    private lobbyClientsWaitingProcessInput!: Map<string, LobbyInputPayload[]>;

    private lobbyClientsLastMessageSentTimestamp!: Map<string, number>;

    //#endregion

    //#region Getters - Setters


    //#endregion

    //#region Constructor

    constructor() {
        super();

        this.updateWorldStateFrameCounter = 0;

        this.lobbyClients = new Map<string, LobbyClient>();
        this.lobbyClientsSenderInfo = new Map<string, LobbyClientSenderInfo>();

        this.lobbyClientsBox2dBody = new Map<string, b2Body>();

        this.lobbyClientsLastProcessedInput = new Map<string, LobbyInputPayload>();
        this.lobbyClientsWaitingProcessInput = new Map<string, LobbyInputPayload[]>();

        this.lobbyClientsLastMessageSentTimestamp = new Map<string, number>();
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Lobby controller initialize start!")
            });

            await this.registerUdpServerEvents();

            this.logManager.showLog({
                log: util.format("Lobby controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Lobby controller initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Lobby controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Register - Unregister Udp Server Event Methods

    private async registerUdpServerEvents(): Promise<void> {
        EventHelper.instance.addListener(
            UdpConstants.UDP_SERVER_EVENTS_CLOSE,
            async () => await this.onUdpServerClose());

        EventHelper.instance.addListener(
            UdpConstants.UDP_SERVER_EVENTS_CONNECT,
            async () => await this.onUdpServerConnect());

        EventHelper.instance.addListener(
            UdpConstants.UDP_SERVER_EVENTS_ERROR,
            async (error) => await this.onUdpServerError(error));

        EventHelper.instance.addListener(
            UdpConstants.UDP_SERVER_EVENTS_LISTENING,
            async (socket) => await this.onUdpServerListen(socket));

        EventHelper.instance.addListener(
            UdpConstants.UDP_SERVER_EVENTS_MESSAGE,
            async (message, senderInfo) => await this.onUdpServerMessage(message, senderInfo));
    }

    private async unregisterUdpServerEvents(): Promise<void> {
        EventHelper.instance.removeListener(
            UdpConstants.UDP_SERVER_EVENTS_CLOSE,
            async () => await this.onUdpServerClose());

        EventHelper.instance.removeListener(
            UdpConstants.UDP_SERVER_EVENTS_CONNECT,
            async () => await this.onUdpServerConnect());

        EventHelper.instance.removeListener(
            UdpConstants.UDP_SERVER_EVENTS_ERROR,
            async (error) => await this.onUdpServerError(error));

        EventHelper.instance.removeListener(
            UdpConstants.UDP_SERVER_EVENTS_LISTENING,
            async (socket) => await this.onUdpServerListen(socket));

        EventHelper.instance.removeListener(
            UdpConstants.UDP_SERVER_EVENTS_MESSAGE,
            async (message, senderInfo) => await this.onUdpServerMessage(message, senderInfo));
    }

    private async unregisterAllUdpServerEvents(): Promise<void> {
        EventHelper.instance.removeAllListeners();
    }

    //#endregion

    //#region Udp Server Event Handler Methods

    private async onUdpServerClose(): Promise<void> {
        this.stopUpdateLobbyBox2dLoop();
        this.stopUpdateLobbyClientsStateLoop();
    }

    private async onUdpServerConnect(): Promise<void> {

    }

    private async onUdpServerError(
        error: string): Promise<void> {

    }

    private async onUdpServerListen(
        socket: Socket): Promise<void> {
        this.socket = socket;

        this.startUpdateLobbyBox2dLoop();
        this.startUpdateLobbyClientsStateLoop();
    }

    private async onUdpServerMessage(
        message: any,
        senderInfo: any): Promise<void> {
        try {
            if (!this.socket) {
                return;
            }

            if (!Buffer.isBuffer(message)) {
                return;
            }

            const baseLobbyRequestMessageReadSerializer =
                new Serializer(
                    SerializationMode.Write, message);
            const baseLobbyRequestMessage =
                BaseLobbyRequestMessage.deserialize(
                    baseLobbyRequestMessageReadSerializer);

            if (!baseLobbyRequestMessage) {
                return;
            }

            const lobbyMessageType =
                baseLobbyRequestMessage.lobbyMessageType;

            switch (lobbyMessageType) {
                // case ClientLobbyMessageTypes.Join:
                //     const joinLobbyRequestMessageReadSerializer =
                //         new Serializer(
                //             SerializationMode.Read, message);
                //     const joinLobbyRequestMessage =
                //         JoinLobbyRequestMessage.deserialize(
                //             joinLobbyRequestMessageReadSerializer);
                //
                //     await this.processJoinLobbyRequestMessage(
                //         senderInfo, joinLobbyRequestMessage);
                //     break;
                // case ClientLobbyMessageTypes.Leave:
                //     const leaveLobbyRequestMessageReadSerializer =
                //         new Serializer(
                //             SerializationMode.Read, message);
                //     const leaveLobbyRequestMessage =
                //         LeaveLobbyRequestMessage.deserialize(
                //             leaveLobbyRequestMessageReadSerializer);
                //
                //     await this.processLeaveLobbyRequestMessage(
                //         senderInfo, leaveLobbyRequestMessage);
                //     break;
                // case ClientLobbyMessageTypes.ChangePet:
                //     const changePetLobbyRequestMessageReadSerializer =
                //         new Serializer(
                //             SerializationMode.Read, message);
                //     const changePetLobbyRequestMessage =
                //         ChangePetLobbyRequestMessage.deserialize(
                //             changePetLobbyRequestMessageReadSerializer);
                //
                //     await this.processChangePetLobbyRequestMessage(
                //         senderInfo, changePetLobbyRequestMessage);
                //     break;
                // case ClientLobbyMessageTypes.EnterMiniGame:
                //     const enterMiniGameLobbyRequestMessageReadSerializer =
                //         new Serializer(
                //             SerializationMode.Read, message);
                //     const enterMiniGameLobbyRequestMessage =
                //         EnterMiniGameLobbyRequestMessage.deserialize(
                //             enterMiniGameLobbyRequestMessageReadSerializer);
                //
                //     await this.processEnterMiniGameLobbyRequestMessage(
                //         senderInfo, enterMiniGameLobbyRequestMessage);
                //     break;
                case ClientLobbyMessageTypes.Move:
                    const moveLobbyRequestMessageReadSerializer =
                        new Serializer(
                            SerializationMode.Read, message);
                    const moveLobbyRequestMessage =
                        MoveLobbyRequestMessage.deserialize(
                            moveLobbyRequestMessageReadSerializer);

                    await this.processMoveLobbyRequestMessage(
                        senderInfo, moveLobbyRequestMessage);
                    break;
            }
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Receive udp server message error! Err: %s",
                    error)
            });
        }
    }

    //#endregion

    //#region Join Request Methods - HTTP

    public async joinLobby(
        lobbyClientId: string,
        pet: Pet,
        lobbyClientPosition: LobbyClientPosition): Promise<LobbyResponse> {
        if (!pet) {
            return new LobbyResponse(
                false, ServerReturnCodes.InternalError);
        }

        // this.logManager.showLog({
        //     log: util.format("User joined to lobby! Id: %s",
        //         lobbyClientId)
        // });

        const movementSpeed =
            Box2dConstants.BOX2D_PET_MOVEMENT_SPEED;
        const petBodyData =
            Box2dConstants.BOX2D_PET_BODY_DATA;

        lobbyClientPosition.x =
            CommonLogicUtil.precisionRoundFloat(
                lobbyClientPosition.x, 4);
        lobbyClientPosition.y =
            CommonLogicUtil.precisionRoundFloat(
                lobbyClientPosition.y, 4);

        petBodyData.box2DBodyDefinitionData.position.x =
            lobbyClientPosition.x;
        petBodyData.box2DBodyDefinitionData.position.y =
            lobbyClientPosition.y;

        this.removeLobbyClient(
            lobbyClientId);
        this.removeLobbyClientSenderInfo(
            lobbyClientId);
        this.removeLobbyClientBox2dBody(
            lobbyClientId);
        this.removeLobbyClientLastProcessedInputPayload(
            lobbyClientId);
        this.removeLobbyClientWaitingProcessInputPayload(
            lobbyClientId);
        this.removeLobbyClientLastMessageSentTimestamp(
            lobbyClientId);

        this.addLobbyClient(
            lobbyClientId,
            movementSpeed,
            pet,
            lobbyClientPosition,
            LobbyClientLocations.InLobby);
        this.addLobbyClientBox2dBody(
            lobbyClientId,
            petBodyData);

        const lobbyClient =
            this.getLobbyClient(
                lobbyClientId);
        if (!lobbyClient) {
            return new LobbyResponse(
                false, ServerReturnCodes.InternalError);
        }

        const lobbyClientBox2dBody =
            this.getLobbyClientBox2dBody(
                lobbyClientId);
        if (!lobbyClientBox2dBody) {
            return new LobbyResponse(
                false, ServerReturnCodes.InternalError);
        }

        const scaleFactor =
            this.calculatePetScaleFactor(
                pet.star);
        ReferenceController.box2dWorldController.box2dWorld.changeBodyShapeRadius(
            lobbyClientBox2dBody, scaleFactor);

        this.setLobbyClientLastMessageSentTimestamp(
            lobbyClientId,
            CommonLogicUtil.getCurrentSecondTimestamp());

        this.sendJoinOtherResponseMessage(
            lobbyClient);

        let lobbyClientsArr =
            this.getLobbyClientsValues();

        const responseData = {
            selfLobbyClient: lobbyClient,
            otherLobbyClients: lobbyClientsArr
        };

        return new LobbyResponse(
            true, ServerReturnCodes.OK, responseData);
    }

    //#endregion

    //#region Join Request Methods - UDP

    private async processJoinLobbyRequestMessage(
        senderInfo: any,
        joinLobbyRequestMessage: JoinLobbyRequestMessage): Promise<void> {
        const token =
            joinLobbyRequestMessage.token;

        const verifyTokenResponse =
            await ReferenceController.authController.verifyToken(
                token);

        if (!verifyTokenResponse.isSuccess) {
            return;
        }

        const payload =
            verifyTokenResponse.payload;
        const user =
            payload.user;
        const lobbyClientId =
            user.thirdPartyId;

        if (!lobbyClientId) {
            return;
        }

        // this.logManager.showLog({
        //     log: util.format("User joined to lobby! Id: %s",
        //         lobbyClientId)
        // });

        const pet =
            joinLobbyRequestMessage.pet;
        if (!pet) {
            return;
        }

        const lobbyClientPosition =
            joinLobbyRequestMessage.lobbyClientPosition;
        if (!lobbyClientPosition) {
            return;
        }

        const movementSpeed =
            Box2dConstants.BOX2D_PET_MOVEMENT_SPEED;
        const petBodyData =
            Box2dConstants.BOX2D_PET_BODY_DATA;

        lobbyClientPosition.x =
            CommonLogicUtil.precisionRoundFloat(
                lobbyClientPosition.x, 4);
        lobbyClientPosition.y =
            CommonLogicUtil.precisionRoundFloat(
                lobbyClientPosition.y, 4);

        petBodyData.box2DBodyDefinitionData.position.x =
            lobbyClientPosition.x;
        petBodyData.box2DBodyDefinitionData.position.y =
            lobbyClientPosition.y;

        this.removeLobbyClient(
            lobbyClientId);
        this.removeLobbyClientSenderInfo(
            lobbyClientId);
        this.removeLobbyClientBox2dBody(
            lobbyClientId);
        this.removeLobbyClientLastProcessedInputPayload(
            lobbyClientId);
        this.removeLobbyClientWaitingProcessInputPayload(
            lobbyClientId);
        this.removeLobbyClientLastMessageSentTimestamp(
            lobbyClientId);

        this.addLobbyClient(
            lobbyClientId,
            movementSpeed,
            pet,
            lobbyClientPosition,
            LobbyClientLocations.InLobby);
        this.addLobbyClientSenderInfo(
            lobbyClientId,
            senderInfo.address,
            senderInfo.port);
        this.addLobbyClientBox2dBody(
            lobbyClientId,
            petBodyData);

        const lobbyClient =
            this.getLobbyClient(
                lobbyClientId);
        if (!lobbyClient) {
            return;
        }

        const lobbyClientSenderInfo =
            this.getLobbyClientSenderInfo(
                lobbyClientId);
        if (!lobbyClientSenderInfo) {
            return;
        }

        const lobbyClientBox2dBody =
            this.getLobbyClientBox2dBody(
                lobbyClientId);
        if (!lobbyClientBox2dBody) {
            return;
        }

        const scaleFactor =
            this.calculatePetScaleFactor(
                pet.star);
        ReferenceController.box2dWorldController.box2dWorld.changeBodyShapeRadius(
            lobbyClientBox2dBody, scaleFactor);

        this.setLobbyClientLastMessageSentTimestamp(
            lobbyClientId,
            CommonLogicUtil.getCurrentSecondTimestamp());

        let lobbyClientsArr =
            this.getLobbyClientsValues();

        this.sendJoinOtherResponseMessage(
            lobbyClient);

        this.sendJoinSelfResponseMessage(
            lobbyClient,
            lobbyClientsArr);
    }

    //#endregion

    //#region Leave Request Methods - HTTP

    public async leaveLobby(
        lobbyClientId: string): Promise<LobbyResponse> {
        const lobbyClient =
            this.getLobbyClient(
                lobbyClientId);
        if (!lobbyClient) {
            return new LobbyResponse(
                false, ServerReturnCodes.InternalError);
        }

        // this.logManager.showLog({
        //     log: util.format("User leaved from lobby! Id: %s",
        //         lobbyClientId)
        // });

        if (lobbyClient.lobbyClientLocation ===
            LobbyClientLocations.InMiniGame) {
            return new LobbyResponse(
                false, ServerReturnCodes.InternalError);
        }

        this.removeLobbyClient(
            lobbyClientId);
        this.removeLobbyClientSenderInfo(
            lobbyClientId);
        this.removeLobbyClientBox2dBody(
            lobbyClientId);
        this.removeLobbyClientLastProcessedInputPayload(
            lobbyClientId);
        this.removeLobbyClientWaitingProcessInputPayload(
            lobbyClientId);
        this.removeLobbyClientLastMessageSentTimestamp(
            lobbyClientId);

        this.sendLeaveOtherResponseMessage(
            lobbyClientId);

        const responseData = {};

        return new LobbyResponse(
            true, ServerReturnCodes.OK, responseData);
    }

    //#endregion

    //#region Leave Request Methods - UDP

    private async processLeaveLobbyRequestMessage(
        senderInfo: any,
        leaveLobbyRequestMessage: LeaveLobbyRequestMessage): Promise<void> {
        const lobbyClientId =
            leaveLobbyRequestMessage.id;

        const lobbyClient =
            this.getLobbyClient(
                lobbyClientId);
        if (!lobbyClient) {
            return;
        }

        // this.logManager.showLog({
        //     log: util.format("User leaved from lobby! Id: %s",
        //         lobbyClientId)
        // });

        if (lobbyClient.lobbyClientLocation ===
            LobbyClientLocations.InMiniGame) {
            return;
        }

        this.removeLobbyClient(
            lobbyClientId);
        this.removeLobbyClientSenderInfo(
            lobbyClientId);
        this.removeLobbyClientBox2dBody(
            lobbyClientId);
        this.removeLobbyClientLastProcessedInputPayload(
            lobbyClientId);
        this.removeLobbyClientWaitingProcessInputPayload(
            lobbyClientId);
        this.removeLobbyClientLastMessageSentTimestamp(
            lobbyClientId);

        this.sendLeaveOtherResponseMessage(
            lobbyClientId);
    }

    //#endregion

    //#region Change Pet Request Methods - HTTP

    public async changePet(
        lobbyClientId: string,
        pet: Pet): Promise<LobbyResponse> {
        const lobbyClient =
            this.getLobbyClient(
                lobbyClientId);
        if (!lobbyClient) {
            return new LobbyResponse(
                false, ServerReturnCodes.InternalError);
        }

        // this.logManager.showLog({
        //     log: util.format("User changed pet! Id: %s",
        //         lobbyClientId)
        // });

        const lobbyClientBox2dBody =
            this.getLobbyClientBox2dBody(
                lobbyClientId);
        if (!lobbyClientBox2dBody) {
            return new LobbyResponse(
                false, ServerReturnCodes.InternalError);
        }

        if (!pet) {
            return new LobbyResponse(
                false, ServerReturnCodes.InternalError);
        }

        lobbyClient.pet = pet;

        const scaleFactor =
            this.calculatePetScaleFactor(
                pet.star);
        ReferenceController.box2dWorldController.box2dWorld.changeBodyShapeRadius(
            lobbyClientBox2dBody, scaleFactor);

        this.sendChangePetOtherResponseMessage(
            lobbyClient);

        const responseData = {
            lobbyClient
        };

        return new LobbyResponse(
            true, ServerReturnCodes.OK, responseData);
    }

    //#endregion

    //#region Change Pet Request Methods - UDP

    private async processChangePetLobbyRequestMessage(
        senderInfo: any,
        changePetLobbyRequestMessage: ChangePetLobbyRequestMessage): Promise<void> {
        const lobbyClientId =
            changePetLobbyRequestMessage.id;

        const lobbyClient =
            this.getLobbyClient(
                lobbyClientId);
        if (!lobbyClient) {
            return;
        }

        // this.logManager.showLog({
        //     log: util.format("User changed pet! Id: %s",
        //         lobbyClientId)
        // });

        const lobbyClientBox2dBody =
            this.getLobbyClientBox2dBody(
                lobbyClientId);
        if (!lobbyClientBox2dBody) {
            return;
        }

        const pet =
            changePetLobbyRequestMessage.pet;
        if (!pet) {
            return;
        }

        lobbyClient.pet = pet;

        const scaleFactor =
            this.calculatePetScaleFactor(
                pet.star);
        ReferenceController.box2dWorldController.box2dWorld.changeBodyShapeRadius(
            lobbyClientBox2dBody, scaleFactor);

        this.sendChangePetSelfResponseMessage(
            lobbyClient);
        this.sendChangePetOtherResponseMessage(
            lobbyClient);
    }

    //#endregion

    //#region Enter Mini Game Request Methods - HTTP

    public async enterMiniGame(
        lobbyClientId: string): Promise<LobbyResponse> {
        const lobbyClient =
            this.getLobbyClient(
                lobbyClientId);
        if (!lobbyClient) {
            return new LobbyResponse(
                false, ServerReturnCodes.InternalError);
        }

        // this.logManager.showLog({
        //     log: util.format("User entered mini game! Id: %s",
        //         lobbyClientId)
        // });

        lobbyClient.lobbyClientLocation =
            LobbyClientLocations.InMiniGame;

        // this.sendEnterMiniGameOtherResponseMessage(
        //     lobbyClientId);

        const responseData = {};

        return new LobbyResponse(
            true, ServerReturnCodes.OK, responseData);
    }

    //#endregion

    //#region Enter Mini Game Request Methods - UDP

    private async processEnterMiniGameLobbyRequestMessage(
        senderInfo: any,
        enterMiniGameLobbyRequestMessage: EnterMiniGameLobbyRequestMessage): Promise<void> {
        const lobbyClientId =
            enterMiniGameLobbyRequestMessage.id;

        const lobbyClient =
            this.getLobbyClient(
                lobbyClientId);
        if (!lobbyClient) {
            return;
        }

        // this.logManager.showLog({
        //     log: util.format("User entered mini game! Id: %s",
        //         lobbyClientId)
        // });

        lobbyClient.lobbyClientLocation =
            LobbyClientLocations.InMiniGame;

        // this.sendEnterMiniGameOtherResponseMessage(
        //     lobbyClientId);
    }

    //#endregion

    //#region Move Request Methods

    private async processMoveLobbyRequestMessage(
        senderInfo: any,
        moveLobbyRequestMessage: MoveLobbyRequestMessage): Promise<void> {
        const lobbyClientId =
            moveLobbyRequestMessage.id;

        const lobbyClient =
            this.getLobbyClient(
                lobbyClientId);
        if (!lobbyClient) {
            return;
        }

        this.addLobbyClientSenderInfoIfNotExist(
            lobbyClientId,
            senderInfo.address,
            senderInfo.port);

        this.setLobbyClientLastMessageSentTimestamp(
            lobbyClientId,
            CommonLogicUtil.getCurrentSecondTimestamp());

        const lobbyInputPayload =
            moveLobbyRequestMessage.lobbyInputPayload;
        const lobbyClientInput =
            lobbyInputPayload.lobbyClientInput;
        const lobbyClientPosition =
            lobbyInputPayload.lobbyClientPosition;

        const lobbyClientLastProcessedInputPayload =
            this.getLobbyClientLastProcessedInputPayload(
                lobbyClientId);
        if (lobbyClientLastProcessedInputPayload) {
            if (lobbyClientLastProcessedInputPayload.tick >=
                lobbyInputPayload.tick) {
                return;
            }
        }

        lobbyClientInput.horizontal =
            CommonLogicUtil.precisionRoundFloat(
                lobbyClientInput.horizontal, 4);
        lobbyClientInput.vertical =
            CommonLogicUtil.precisionRoundFloat(
                lobbyClientInput.vertical, 4);

        lobbyClientPosition.x =
            CommonLogicUtil.precisionRoundFloat(
                lobbyClientPosition.x, 4);
        lobbyClientPosition.y =
            CommonLogicUtil.precisionRoundFloat(
                lobbyClientPosition.y, 4);

        this.addLobbyClientWaitingProcessInputPayload(
            lobbyClientId,
            lobbyInputPayload);

        // console.log("Tick First -> " + lobbyInputPayload.tick +
        //     " - lobbyInputPayload.horizontal -> " + lobbyInputPayload.lobbyClientInput.horizontal +
        //     " - lobbyInputPayload.vertical -> " + lobbyInputPayload.lobbyClientInput.vertical +
        //     " - lobbyInputPayload.lobbyClientPosition.x -> " + lobbyInputPayload.lobbyClientPosition.x +
        //     " - lobbyInputPayload.lobbyClientPosition.y -> " + lobbyInputPayload.lobbyClientPosition.y);
    }

    //#endregion

    //#region Process Waiting Inputs Methods

    private processWaitingInputs(): void {
        if (this.lobbyClientsWaitingProcessInput.size <= 0) {
            return;
        }

        this.lobbyClientsWaitingProcessInput.forEach(
            (lobbyClientsWaitingProcessInputs, lobbyClientId, map) => {
                const lobbyClient =
                    this.getLobbyClient(
                        lobbyClientId);
                if (!lobbyClient) {
                    return;
                }

                const lobbyClientBox2dBody =
                    this.getLobbyClientBox2dBody(
                        lobbyClientId);
                if (!lobbyClientBox2dBody) {
                    return;
                }

                const lobbyClientWaitingProcessInputPayloads =
                    this.getLobbyClientWaitingProcessInputPayloads(
                        lobbyClientId);
                if (!lobbyClientWaitingProcessInputPayloads ||
                    lobbyClientWaitingProcessInputPayloads.length <= 0) {
                    return;
                }

                let totalInputHorizontal = 0;
                let totalInputVertical = 0;

                let lobbyClientProcessInputPayload;

                while (lobbyClientWaitingProcessInputPayloads.length > 0) {
                    const lobbyClientWaitingProcessInputPayload =
                        lobbyClientWaitingProcessInputPayloads.shift();
                    if (!lobbyClientWaitingProcessInputPayload) {
                        continue;
                    }

                    const lobbyClientInput =
                        lobbyClientWaitingProcessInputPayload.lobbyClientInput;
                    if (!lobbyClientInput) {
                        continue;
                    }

                    lobbyClientProcessInputPayload = lobbyClientWaitingProcessInputPayload;

                    totalInputHorizontal +=
                        lobbyClientInput.horizontal;
                    totalInputVertical +=
                        lobbyClientInput.vertical;

                    this.setLobbyClientLastProcessedInputPayload(
                        lobbyClientId,
                        lobbyClientWaitingProcessInputPayload);
                }

                const inputVector =
                    new b2Vec2(
                        totalInputHorizontal,
                        totalInputVertical);
                const inputVectorScaled =
                    inputVector.Scale(
                        lobbyClient.movementSpeed);

                lobbyClientBox2dBody.SetLinearVelocity(inputVectorScaled);

                // this.fixLobbyClientPosition(
                //     lobbyClientId);

                // const lobbyClientBox2dBodyPosition =
                //     lobbyClientBox2dBody
                //         .GetPosition()
                //         .Clone();
                //
                // if(lobbyClientProcessInputPayload){
                //     console.log("Tick -> " + lobbyClientProcessInputPayload.tick +
                //         " - lobbyClientBox2dBodyPosition.x -> " + lobbyClientBox2dBodyPosition.x +
                //         " - lobbyClientBox2dBodyPosition.y -> " + lobbyClientBox2dBodyPosition.y +
                //         " - lobbyClientWaitingProcessInputPayload.x -> " + lobbyClientProcessInputPayload.lobbyClientPosition.x +
                //         " - lobbyClientWaitingProcessInputPayload.y -> " + lobbyClientProcessInputPayload.lobbyClientPosition.y +
                //         " - inputVectorScaled.x -> " + inputVectorScaled.x +
                //         " - inputVectorScaled.y -> " + inputVectorScaled.y +
                //         " - lobbyClientInput.horizontal -> " + lobbyClientProcessInputPayload.lobbyClientInput.horizontal +
                //         " - lobbyClientInput.vertical -> " + lobbyClientProcessInputPayload.lobbyClientInput.vertical);
                // }
            });
    }

    //#endregion

    //#region Fix Lobby Clients Positions Methods

    private fixLobbyClientsPositions(): void {
        this.lobbyClients.forEach(
            (lobbyClient, lobbyClientId, map) => {
                if (lobbyClient.lobbyClientLocation ===
                    LobbyClientLocations.InMiniGame) {
                    return;
                }

                const lobbyClientBox2dBody =
                    this.getLobbyClientBox2dBody(
                        lobbyClientId);
                if (!lobbyClientBox2dBody) {
                    return;
                }

                const lobbyClientLastProcessedInputPayload =
                    this.getLobbyClientLastProcessedInputPayload(
                        lobbyClientId);
                if (!lobbyClientLastProcessedInputPayload) {
                    return;
                }

                const lobbyClientWaitingProcessInputPosition =
                    new b2Vec2(
                        lobbyClientLastProcessedInputPayload.lobbyClientPosition.x,
                        lobbyClientLastProcessedInputPayload.lobbyClientPosition.y);

                const lobbyClientBox2dBodyPosition =
                    lobbyClientBox2dBody
                        .GetPosition()
                        .Clone();

                const positionDistance =
                    this.getDistanceBetweenTwoVector(
                        lobbyClientBox2dBodyPosition,
                        lobbyClientWaitingProcessInputPosition);

                if (positionDistance >= Box2dConstants.BOX2D_PET_POSITION_FIX_MAX_RANGE) {

                } else if (positionDistance >= Box2dConstants.BOX2D_PET_POSITION_FIX_MIN_RANGE) {
                    const fixVector =
                        lobbyClientWaitingProcessInputPosition.Subtract(
                            lobbyClientBox2dBodyPosition);
                    fixVector.Normalize();
                    const fixVectorScaled =
                        fixVector.Scale(
                            lobbyClient.movementSpeed *
                            Box2dConstants.BOX2D_PET_MOVEMENT_FIX_SPEED_MULTIPLIER);

                    // console.log("-------> Applying force to -> " + lobbyClientId +
                    //     " - lobbyClientBox2dBodyPosition.x -> " + lobbyClientBox2dBodyPosition.x +
                    //     " - lobbyClientBox2dBodyPosition.y -> " + lobbyClientBox2dBodyPosition.y +
                    //     " - Tick -> " + lobbyClientLastProcessedInputPayload.tick +
                    //     " - positionDistance -> " + positionDistance +
                    //     " - fixVector.x -> " + fixVector.x +
                    //     " - fixVector.y -> " + fixVector.y);

                    lobbyClientBox2dBody.SetLinearVelocity(fixVectorScaled);
                }
            });
    }

    private fixLobbyClientPosition(
        lobbyClientId: string): void {
        const lobbyClient =
            this.getLobbyClient(
                lobbyClientId);
        if (!lobbyClient) {
            return;
        }

        if (lobbyClient.lobbyClientLocation ===
            LobbyClientLocations.InMiniGame) {
            return;
        }

        const lobbyClientBox2dBody =
            this.getLobbyClientBox2dBody(
                lobbyClientId);
        if (!lobbyClientBox2dBody) {
            return;
        }

        const lobbyClientLastProcessedInputPayload =
            this.getLobbyClientLastProcessedInputPayload(
                lobbyClientId);
        if (!lobbyClientLastProcessedInputPayload) {
            return;
        }

        const lobbyClientWaitingProcessInputPosition =
            new b2Vec2(
                lobbyClientLastProcessedInputPayload.lobbyClientPosition.x,
                lobbyClientLastProcessedInputPayload.lobbyClientPosition.y);

        const lobbyClientBox2dBodyPosition =
            lobbyClientBox2dBody
                .GetPosition()
                .Clone();

        lobbyClientBox2dBodyPosition.x =
            CommonLogicUtil.precisionRoundFloat(
                lobbyClientBox2dBodyPosition.x, 4);
        lobbyClientBox2dBodyPosition.y =
            CommonLogicUtil.precisionRoundFloat(
                lobbyClientBox2dBodyPosition.y, 4);

        const positionDistance =
            this.getDistanceBetweenTwoVector(
                lobbyClientBox2dBodyPosition,
                lobbyClientWaitingProcessInputPosition);

        if (positionDistance >= Box2dConstants.BOX2D_PET_POSITION_FIX_MAX_RANGE) {

        } else if (positionDistance >= Box2dConstants.BOX2D_PET_POSITION_FIX_MIN_RANGE) {
            const fixVector =
                lobbyClientWaitingProcessInputPosition.Subtract(
                    lobbyClientBox2dBodyPosition);
            fixVector.Normalize();
            const fixVectorScaled =
                fixVector.Scale(
                    lobbyClient.movementSpeed *
                    Box2dConstants.BOX2D_PET_MOVEMENT_FIX_SPEED_MULTIPLIER);

            // console.log("-------> Applying force to -> " + lobbyClientId +
            //     " - lobbyClientBox2dBodyPosition.x -> " + lobbyClientBox2dBodyPosition.x +
            //     " - lobbyClientBox2dBodyPosition.y -> " + lobbyClientBox2dBodyPosition.y +
            //     " - Tick -> " + lobbyClientLastProcessedInputPayload.tick +
            //     " - positionDistance -> " + positionDistance +
            //     " - fixVector.x -> " + fixVector.x +
            //     " - fixVector.y -> " + fixVector.y);

            lobbyClientBox2dBody.SetLinearVelocity(fixVectorScaled);
        }
    }

    //#endregion

    //#region Update Lobby Clients World State Methods

    private updateLobbyClientsWorldState(): void {
        if (this.lobbyClients.size <= 0) {
            return;
        }

        const lobbyClientsToRemove: string[] = [];
        const lobbyWorldStatePayloads: LobbyWorldStatePayload[] = [];

        this.lobbyClients.forEach(
            (lobbyClient, lobbyClientId, map) => {
                const lobbyClientLastMessageSentTimestamp =
                    this.getLobbyClientLastMessageSentTimestamp(
                        lobbyClientId);
                if (lobbyClientLastMessageSentTimestamp) {
                    const lobbyClientInactiveTimestamp =
                        CommonLogicUtil.getCurrentSecondTimestamp() -
                        lobbyClientLastMessageSentTimestamp;

                    if (lobbyClient.lobbyClientLocation ===
                        LobbyClientLocations.InMiniGame) {
                        if (lobbyClientInactiveTimestamp >=
                            LobbyConstants.LOBBY_CLIENT_REMOVE_INACTIVE_USER_IN_MINI_GAME_SECONDS) {
                            // console.log("Remove in mini game user -> " + lobbyClientId);

                            lobbyClientsToRemove.push(lobbyClientId);
                            return;
                        }
                    } else {
                        if (lobbyClientInactiveTimestamp >=
                            LobbyConstants.LOBBY_CLIENT_REMOVE_INACTIVE_USER_IN_LOBBY_SECONDS) {
                            // console.log("Remove in lobby user -> " + lobbyClientId);

                            lobbyClientsToRemove.push(lobbyClientId);
                            return;
                        }
                    }
                }

                if (lobbyClient.lobbyClientLocation ===
                    LobbyClientLocations.InMiniGame) {
                    return;
                }

                const lobbyClientPosition =
                    lobbyClient.lobbyClientPosition;
                if (!lobbyClientPosition) {
                    return;
                }

                const lobbyClientBox2dBody =
                    this.getLobbyClientBox2dBody(
                        lobbyClientId);
                if (!lobbyClientBox2dBody) {
                    return;
                }

                const lobbyClientLastProcessedInputPayload =
                    this.getLobbyClientLastProcessedInputPayload(
                        lobbyClientId);
                if (!lobbyClientLastProcessedInputPayload) {
                    return;
                }

                const lobbyClientInput =
                    lobbyClientLastProcessedInputPayload.lobbyClientInput;

                const lobbyClientBox2dBodyPosition =
                    lobbyClientBox2dBody
                        .GetPosition()
                        .Clone();

                lobbyClientPosition.x =
                    CommonLogicUtil.precisionRoundFloat(
                        lobbyClientBox2dBodyPosition.x, 4);
                lobbyClientPosition.y =
                    CommonLogicUtil.precisionRoundFloat(
                        lobbyClientBox2dBodyPosition.y, 4);

                lobbyWorldStatePayloads.push(
                    new LobbyWorldStatePayload(
                        lobbyClientId,
                        lobbyClientLastProcessedInputPayload.tick,
                        lobbyClientInput,
                        lobbyClientPosition));

                // console.log("Tick -> " + lobbyClientLastProcessedInputPayload.tick +
                //     " - lobbyClientPosition.x -> " + lobbyClientPosition.x +
                //     " - lobbyClientPosition.y -> " + lobbyClientPosition.y);
            });

        this.sendUpdateLobbyWorldStateResponseMessage(
            lobbyWorldStatePayloads);

        this.removeInactiveLobbyClients(
            lobbyClientsToRemove);

        // console.log("Lobby Clients Count -> " + this.lobbyClients.size +
        //     " - Sending length -> " + lobbyWorldStatePayloads.length +
        //     " - lobbyClientsToRemove -> " + lobbyClientsToRemove.length);
    }

    //#endregion

    //#region Remove Inactive Lobby Clients Methods

    private removeInactiveLobbyClients(
        lobbyClientsToRemove: string[]): void {
        for (const lobbyClientId of lobbyClientsToRemove) {
            const lobbyClient =
                this.getLobbyClient(
                    lobbyClientId);
            if (!lobbyClient) {
                continue;
            }

            // this.logManager.showLog({
            //     log: util.format("Removing inactive user! Id: %s",
            //         lobbyClientId)
            // });

            this.removeLobbyClient(
                lobbyClientId);
            this.removeLobbyClientSenderInfo(
                lobbyClientId);
            this.removeLobbyClientBox2dBody(
                lobbyClientId);
            this.removeLobbyClientLastProcessedInputPayload(
                lobbyClientId);
            this.removeLobbyClientWaitingProcessInputPayload(
                lobbyClientId);
            this.removeLobbyClientLastMessageSentTimestamp(
                lobbyClientId);

            this.sendLeaveOtherResponseMessage(
                lobbyClientId);
        }
    }

    //#endregion

    //#region Update Lobby Box2d Loop Methods

    private startUpdateLobbyBox2dLoop(): void {
        this.updateLobbyBox2dIntervalObject =
            setInterval(
                () => this.updateLobbyBox2dLoop(),
                Box2dConstants.BOX2D_UPDATE_MILLISECONDS);
    }

    private stopUpdateLobbyBox2dLoop(): void {
        clearInterval(this.updateLobbyBox2dIntervalObject);
    }

    private updateLobbyBox2dLoop(): void {
        this.processWaitingInputs();

        this.box2dStep();

        this.resetLobbyClientsBox2dBodyVelocities();
        this.fixLobbyClientsPositions();

        this.box2dStep();

        this.resetLobbyClientsBox2dBodyVelocities();

        if (++this.updateWorldStateFrameCounter % 2 == 0) {
            this.updateWorldStateFrameCounter = 0;

            this.updateLobbyClientsWorldState();
        }
    }

    //#endregion

    //#region Update Lobby Clients State Methods

    private startUpdateLobbyClientsStateLoop(): void {
        this.updateLobbyClientsStateIntervalObject =
            setInterval(
                () => this.updateLobbyClientsStateLoop(),
                LobbyConstants.LOBBY_CLIENTS_STATE_UPDATE_INTERVAL_MILLISECONDS);
    }

    private stopUpdateLobbyClientsStateLoop(): void {
        clearInterval(this.updateLobbyClientsStateIntervalObject);
    }

    private updateLobbyClientsStateLoop(): void {
        let lobbyClientIdsArr =
            this.getLobbyClientsKeys();
        let lobbyClientsArr =
            this.getLobbyClientsValues();

        this.sendUpdateLobbyClientStateResponseMessage(
            lobbyClientIdsArr, lobbyClientsArr);
    }

    //#endregion

    //#region Box2d Step Methods

    private box2dStep(): void {
        ReferenceController.box2dWorldController.box2dWorld.world.Step(
            (1 / Box2dConstants.BOX2D_UPDATE_RATE),
            {
                velocityIterations: Box2dConstants.BOX2D_VELOCITY_ITERATIONS,
                positionIterations: Box2dConstants.BOX2D_POSITION_ITERATIONS
            });
    }

    //#endregion

    //#region Reset Lobby Clients Box2d Body Velocities Methods

    private resetLobbyClientsBox2dBodyVelocities(): void {
        this.lobbyClients.forEach(
            (lobbyClient, lobbyClientId, map) => {
                const lobbyClientBox2dBody =
                    this.getLobbyClientBox2dBody(
                        lobbyClientId);
                if (!lobbyClientBox2dBody) {
                    return;
                }

                lobbyClientBox2dBody.SetLinearVelocity(new b2Vec2(0, 0));
                lobbyClientBox2dBody.SetAngularVelocity(0);
            });
    }

    //#endregion

    //#region Get - Add - Remove Lobby Client Methods

    public getLobbyClientsKeys(): string[] {
        const lobbyClientsKeysArr =
            [...this.lobbyClients.keys()];

        return lobbyClientsKeysArr;
    }

    public getLobbyClientsValues(): LobbyClient[] {
        const lobbyClientsValuesArr =
            [...this.lobbyClients.values()];

        return lobbyClientsValuesArr;
    }

    public getLobbyClientsValuesWithLocationFilter(
        lobbyClientLocation: LobbyClientLocations): LobbyClient[] {
        const lobbyClientsValuesArr =
            [...this.lobbyClients.values()];

        const lobbyClientsValuesArrWithLocationFilter =
            lobbyClientsValuesArr.filter(
                lobbyClient => lobbyClient.lobbyClientLocation === lobbyClientLocation);

        return lobbyClientsValuesArrWithLocationFilter;
    }

    private getLobbyClient(
        lobbyClientId: string): LobbyClient | undefined {
        if (!this.lobbyClients.has(lobbyClientId)) {
            return undefined;
        }

        return this.lobbyClients.get(lobbyClientId);
    }

    private addLobbyClient(
        lobbyClientId: string,
        movementSpeed: number,
        pet: Pet,
        lobbyClientPosition: LobbyClientPosition,
        lobbyClientLocation: LobbyClientLocations): void {
        const lobbyClient =
            new LobbyClient(
                lobbyClientId,
                movementSpeed,
                pet,
                lobbyClientPosition,
                lobbyClientLocation);

        this.lobbyClients.set(
            lobbyClientId,
            lobbyClient);
    }

    private removeLobbyClient(
        lobbyClientId: string): void {
        if (!this.lobbyClients.has(lobbyClientId)) {
            return;
        }

        this.lobbyClients.delete(lobbyClientId);
    }

    //#endregion

    //#region Get - Add - Remove Lobby Client Sender Info Methods

    public getLobbyClientsSenderInfoKeys(): string[] {
        const lobbyClientsSenderInfoKeysArr =
            [...this.lobbyClientsSenderInfo.keys()];

        return lobbyClientsSenderInfoKeysArr;
    }

    public getLobbyClientsSenderInfoValues(): LobbyClientSenderInfo[] {
        const lobbyClientsSenderInfoValuesArr =
            [...this.lobbyClientsSenderInfo.values()];

        return lobbyClientsSenderInfoValuesArr;
    }

    private getLobbyClientSenderInfo(
        lobbyClientId: string): LobbyClientSenderInfo | undefined {
        if (!this.lobbyClientsSenderInfo.has(lobbyClientId)) {
            return undefined;
        }

        return this.lobbyClientsSenderInfo.get(lobbyClientId);
    }

    private addLobbyClientSenderInfo(
        lobbyClientId: string,
        address: string,
        port: number): void {
        const lobbyClientSenderInfo =
            new LobbyClientSenderInfo(
                lobbyClientId,
                address,
                port);

        this.lobbyClientsSenderInfo.set(
            lobbyClientId,
            lobbyClientSenderInfo);
    }

    private addLobbyClientSenderInfoIfNotExist(
        lobbyClientId: string,
        address: string,
        port: number): void {
        if (this.lobbyClientsSenderInfo.has(lobbyClientId)) {
            return;
        }

        const lobbyClientSenderInfo =
            new LobbyClientSenderInfo(
                lobbyClientId,
                address,
                port);

        this.lobbyClientsSenderInfo.set(
            lobbyClientId,
            lobbyClientSenderInfo);
    }

    private removeLobbyClientSenderInfo(
        lobbyClientId: string): void {
        if (!this.lobbyClientsSenderInfo.has(lobbyClientId)) {
            return;
        }

        this.lobbyClientsSenderInfo.delete(lobbyClientId);
    }

    //#endregion

    //#region Get - Add - Remove Lobby Client Box2d Body Methods

    private getLobbyClientBox2dBody(
        lobbyClientId: string): b2Body | undefined {
        if (!this.lobbyClientsBox2dBody.has(lobbyClientId)) {
            return undefined;
        }

        return this.lobbyClientsBox2dBody.get(lobbyClientId);
    }

    private addLobbyClientBox2dBody(
        lobbyClientId: string,
        petBodyData: Box2dBodyData): void {
        const petBox2dBody =
            ReferenceController.box2dWorldController.box2dWorld.createBodyFromBox2dBodyData(
                petBodyData);

        this.lobbyClientsBox2dBody.set(
            lobbyClientId,
            petBox2dBody);
    }

    private removeLobbyClientBox2dBody(
        lobbyClientId: string): void {
        if (!this.lobbyClientsBox2dBody.has(lobbyClientId)) {
            return;
        }

        this.lobbyClientsBox2dBody.delete(lobbyClientId);
    }

    //#endregion

    //#region Get - Set - Remove Lobby Client Last Processed Input Payload Methods

    private getLobbyClientLastProcessedInputPayload(
        lobbyClientId: string): LobbyInputPayload | undefined {
        if (!this.lobbyClientsLastProcessedInput.has(lobbyClientId)) {
            return undefined;
        }

        return this.lobbyClientsLastProcessedInput.get(lobbyClientId);
    }

    private setLobbyClientLastProcessedInputPayload(
        lobbyClientId: string,
        lobbyInputPayload: LobbyInputPayload): void {
        this.lobbyClientsLastProcessedInput.set(
            lobbyClientId,
            lobbyInputPayload);
    }

    private removeLobbyClientLastProcessedInputPayload(
        lobbyClientId: string): void {
        if (!this.lobbyClientsLastProcessedInput.has(lobbyClientId)) {
            return;
        }

        this.lobbyClientsLastProcessedInput.delete(lobbyClientId);
    }

    //#endregion

    //#region Get - Add - Remove Lobby Client Waiting Process Input Payload Methods

    private getLobbyClientWaitingProcessInputPayloads(
        lobbyClientId: string): LobbyInputPayload[] | undefined {
        if (!this.lobbyClientsWaitingProcessInput.has(lobbyClientId)) {
            return undefined;
        }

        return this.lobbyClientsWaitingProcessInput.get(lobbyClientId);
    }

    private addLobbyClientWaitingProcessInputPayload(
        lobbyClientId: string,
        lobbyInputPayload: LobbyInputPayload): void {
        if (!this.lobbyClientsWaitingProcessInput.has(lobbyClientId)) {
            this.lobbyClientsWaitingProcessInput.set(
                lobbyClientId, []);
        }

        const lobbyInputPayloads =
            this.lobbyClientsWaitingProcessInput.get(lobbyClientId);
        if (!lobbyInputPayloads) {
            return;
        }

        lobbyInputPayloads.push(
            lobbyInputPayload);

        this.lobbyClientsWaitingProcessInput.set(
            lobbyClientId,
            lobbyInputPayloads);
    }

    private removeLobbyClientWaitingProcessInputPayload(
        lobbyClientId: string): void {
        if (!this.lobbyClientsWaitingProcessInput.has(lobbyClientId)) {
            return;
        }

        this.lobbyClientsWaitingProcessInput.delete(lobbyClientId);
    }

    //#endregion

    //#region Get - Set - Remove Lobby Client Last Message Sent Timestamp Methods

    private getLobbyClientLastMessageSentTimestamp(
        lobbyClientId: string): number | undefined {
        if (!this.lobbyClientsLastMessageSentTimestamp.has(lobbyClientId)) {
            return undefined;
        }

        return this.lobbyClientsLastMessageSentTimestamp.get(lobbyClientId);
    }

    private setLobbyClientLastMessageSentTimestamp(
        lobbyClientId: string,
        messageSentTimestamp: number): void {
        this.lobbyClientsLastMessageSentTimestamp.set(
            lobbyClientId,
            messageSentTimestamp);
    }

    private removeLobbyClientLastMessageSentTimestamp(
        lobbyClientId: string): void {
        if (!this.lobbyClientsLastMessageSentTimestamp.has(lobbyClientId)) {
            return;
        }

        this.lobbyClientsLastMessageSentTimestamp.delete(lobbyClientId);
    }

    //#endregion

    //#region Send Lobby Response Message Methods

    private sendJoinSelfResponseMessage(
        lobbyClient: LobbyClient,
        lobbyClientsArr: LobbyClient[]): void {
        try {
            const lobbyClientId =
                lobbyClient.id;

            const joinLobbySelfResponseMessage =
                new JoinLobbySelfResponseMessage(ServerReturnCodes.OK,
                    lobbyClient, lobbyClientsArr);

            const joinLobbySelfResponseMessageResponseBuffer =
                new Uint8Array(
                    joinLobbySelfResponseMessage.getTotalBufferSize() + 1);
            const joinLobbySelfResponseMessageWriteSerializer =
                new Serializer(
                    SerializationMode.Write, joinLobbySelfResponseMessageResponseBuffer);

            joinLobbySelfResponseMessage.serialize(
                joinLobbySelfResponseMessageWriteSerializer);

            this.broadcastMessageToSpecificLobbyClient(
                joinLobbySelfResponseMessageResponseBuffer,
                lobbyClientId);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Send udp server join self response message error! Err: %s",
                    error)
            });
        }
    }

    private sendJoinOtherResponseMessage(
        lobbyClient: LobbyClient): void {
        try {
            const lobbyClientId =
                lobbyClient.id;

            const joinLobbyOtherResponseMessage =
                new JoinLobbyOtherResponseMessage(ServerReturnCodes.OK,
                    lobbyClient);

            const joinLobbyOtherResponseMessageResponseBuffer =
                new Uint8Array(
                    joinLobbyOtherResponseMessage.getTotalBufferSize() + 1);
            const joinLobbyOtherResponseMessageWriteSerializer =
                new Serializer(
                    SerializationMode.Write, joinLobbyOtherResponseMessageResponseBuffer);

            joinLobbyOtherResponseMessage.serialize(
                joinLobbyOtherResponseMessageWriteSerializer);

            this.broadcastMessageToAllLobbyClientsExceptGivenOnes(
                joinLobbyOtherResponseMessageResponseBuffer,
                [lobbyClientId]);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Send udp server join other response message error! Err: %s",
                    error)
            });
        }
    }

    private sendLeaveSelfResponseMessage(
        lobbyClientId: string): void {
        try {
            const leaveLobbySelfResponseMessage =
                new LeaveLobbySelfResponseMessage(ServerReturnCodes.OK,
                    lobbyClientId);

            const leaveLobbySelfResponseMessageResponseBuffer =
                new Uint8Array(
                    leaveLobbySelfResponseMessage.getTotalBufferSize() + 1);
            const leaveLobbySelfResponseMessageWriteSerializer =
                new Serializer(
                    SerializationMode.Write, leaveLobbySelfResponseMessageResponseBuffer);

            leaveLobbySelfResponseMessage.serialize(
                leaveLobbySelfResponseMessageWriteSerializer);

            this.broadcastMessageToSpecificLobbyClient(
                leaveLobbySelfResponseMessageResponseBuffer,
                lobbyClientId);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Send udp server leave self response message error! Err: %s",
                    error)
            });
        }
    }

    private sendLeaveOtherResponseMessage(
        lobbyClientId: string): void {
        try {
            const leaveLobbyOtherResponseMessage =
                new LeaveLobbyOtherResponseMessage(ServerReturnCodes.OK,
                    lobbyClientId);

            const leaveLobbyOtherResponseMessageResponseBuffer =
                new Uint8Array(
                    leaveLobbyOtherResponseMessage.getTotalBufferSize() + 1);
            const leaveLobbyOtherResponseMessageWriteSerializer =
                new Serializer(
                    SerializationMode.Write, leaveLobbyOtherResponseMessageResponseBuffer);

            leaveLobbyOtherResponseMessage.serialize(
                leaveLobbyOtherResponseMessageWriteSerializer);

            this.broadcastMessageToAllLobbyClientsExceptGivenOnes(
                leaveLobbyOtherResponseMessageResponseBuffer,
                [lobbyClientId]);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Send udp server leave other response message error! Err: %s",
                    error)
            });
        }
    }

    private sendChangePetSelfResponseMessage(
        lobbyClient: LobbyClient): void {
        try {
            const lobbyClientId =
                lobbyClient.id;

            const changePetLobbySelfResponseMessage =
                new ChangePetLobbySelfResponseMessage(ServerReturnCodes.OK,
                    lobbyClient);

            const changePetLobbySelfResponseMessageResponseBuffer =
                new Uint8Array(
                    changePetLobbySelfResponseMessage.getTotalBufferSize() + 1);
            const changePetLobbySelfResponseMessageWriteSerializer =
                new Serializer(
                    SerializationMode.Write, changePetLobbySelfResponseMessageResponseBuffer);

            changePetLobbySelfResponseMessage.serialize(
                changePetLobbySelfResponseMessageWriteSerializer);

            this.broadcastMessageToSpecificLobbyClient(
                changePetLobbySelfResponseMessageResponseBuffer,
                lobbyClientId);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Send udp server send change pet self response message error! Err: %s",
                    error)
            });
        }
    }

    private sendChangePetOtherResponseMessage(
        lobbyClient: LobbyClient): void {
        try {
            const lobbyClientId =
                lobbyClient.id;

            const changePetLobbyOtherResponseMessage =
                new ChangePetLobbyOtherResponseMessage(ServerReturnCodes.OK,
                    lobbyClient);

            const changePetLobbyOtherResponseMessageResponseBuffer =
                new Uint8Array(
                    changePetLobbyOtherResponseMessage.getTotalBufferSize() + 1);
            const changePetLobbyOtherResponseMessageWriteSerializer =
                new Serializer(
                    SerializationMode.Write, changePetLobbyOtherResponseMessageResponseBuffer);

            changePetLobbyOtherResponseMessage.serialize(
                changePetLobbyOtherResponseMessageWriteSerializer);

            this.broadcastMessageToAllLobbyClientsExceptGivenOnes(
                changePetLobbyOtherResponseMessageResponseBuffer,
                [lobbyClientId]);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Send udp server change pet other response message error! Err: %s",
                    error)
            });
        }
    }

    private sendEnterMiniGameOtherResponseMessage(
        lobbyClientId: string): void {
        try {

        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Send udp server enter mini game other response message error! Err: %s",
                    error)
            });
        }
    }

    public sendUpdateLobbyWorldStateResponseMessage(
        lobbyWorldStatePayloads: LobbyWorldStatePayload[]): void {
        try {
            const updateLobbyWorldStateResponseMessage =
                new UpdateLobbyWorldStateResponseMessage(ServerReturnCodes.OK,
                    lobbyWorldStatePayloads);

            const updateLobbyWorldStateResponseMessageResponseBuffer =
                new Uint8Array(
                    updateLobbyWorldStateResponseMessage.getTotalBufferSize() + 1);
            const updateLobbyWorldStateResponseMessageWriteSerializer =
                new Serializer(
                    SerializationMode.Write, updateLobbyWorldStateResponseMessageResponseBuffer);

            updateLobbyWorldStateResponseMessage.serialize(
                updateLobbyWorldStateResponseMessageWriteSerializer);

            this.broadcastMessageToAllLobbyClients(
                updateLobbyWorldStateResponseMessageResponseBuffer);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Send udp server update lobby world state response message error! Err: %s",
                    error)
            });
        }
    }

    private sendUpdateLobbyClientStateResponseMessage(
        otherLobbyClientIds: string[],
        otherLobbyClients: LobbyClient[]): void {
        try {
            const updateLobbyClientStateResponseMessage =
                new UpdateLobbyClientStateResponseMessage(ServerReturnCodes.OK,
                    otherLobbyClientIds,
                    otherLobbyClients);

            const updateLobbyClientStateResponseMessageResponseBuffer =
                new Uint8Array(
                    updateLobbyClientStateResponseMessage.getTotalBufferSize() + 1);
            const updateLobbyClientStateResponseMessageWriteSerializer =
                new Serializer(
                    SerializationMode.Write, updateLobbyClientStateResponseMessageResponseBuffer);

            updateLobbyClientStateResponseMessage.serialize(
                updateLobbyClientStateResponseMessageWriteSerializer);

            this.broadcastMessageToAllLobbyClients(
                updateLobbyClientStateResponseMessageResponseBuffer);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Send udp server update lobby client state response message error! Err: %s",
                    error)
            });
        }
    }

    //#endregion

    //#region Broadcast Methods

    private broadcastMessageToAllLobbyClients(
        message: any): void {
        try {
            this.lobbyClientsSenderInfo.forEach(
                (lobbyClientSenderInfo, lobbyClientId, map) => {
                    const lobbyClientAddress =
                        lobbyClientSenderInfo.address;
                    const lobbyClientPort =
                        lobbyClientSenderInfo.port;

                    this.socket.send(
                        message, lobbyClientPort, lobbyClientAddress);
                });
        } catch (exception) {
            this.logManager.showError({
                error: util.format("Broadcast message to all lobby clients exception! Exception: %s",
                    exception)
            });
        }
    }

    private broadcastMessageToAllLobbyClientsExceptGivenOnes(
        message: any,
        exceptLobbyClientIds: string[]): void {
        try {
            this.lobbyClientsSenderInfo.forEach(
                (lobbyClientSenderInfo, lobbyClientId, map) => {
                    if (exceptLobbyClientIds.includes(lobbyClientId)) {
                        return;
                    }

                    const lobbyClientAddress =
                        lobbyClientSenderInfo.address;
                    const lobbyClientPort =
                        lobbyClientSenderInfo.port;

                    this.socket.send(
                        message, lobbyClientPort, lobbyClientAddress);
                });
        } catch (exception) {
            this.logManager.showError({
                error: util.format("Broadcast message to all lobby clients except given one exception! Exception: %s",
                    exception)
            });
        }
    }

    private broadcastMessageToSpecificLobbyClient(
        message: any,
        lobbyClientId: string): void {
        try {
            const lobbyClientSenderInfo =
                this.getLobbyClientSenderInfo(
                    lobbyClientId);
            if (!lobbyClientSenderInfo) {
                return;
            }

            const lobbyClientAddress =
                lobbyClientSenderInfo.address;
            const lobbyClientPort =
                lobbyClientSenderInfo.port;

            this.socket.send(
                message, lobbyClientPort, lobbyClientAddress);
        } catch (exception) {
            this.logManager.showError({
                error: util.format("Broadcast message to specific lobby client exception! Exception: %s",
                    exception)
            });
        }
    }

    //#endregion

    //#region Util Methods

    private getDistanceBetweenTwoVector(
        vector1: any,
        vector2: any): number {
        const xDiff = vector1.x - vector2.x;
        const yDiff = vector1.y - vector2.y;

        return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
    }

    private calculatePetScaleFactor(
        star: number): number {
        star =
            CommonLogicUtil.precisionRoundFloat(
                star, 2);

        const starNormalized =
            this.inverseLerp(
                Box2dConstants.BOX2D_PET_MIN_STAR, Box2dConstants.BOX2D_PET_MAX_STAR, star);
        const newScale =
            this.lerp(
                Box2dConstants.BOX2D_PET_MIN_SCALE, Box2dConstants.BOX2D_PET_MAX_SCALE, starNormalized);

        const scaleFactor =
            newScale / Box2dConstants.BOX2D_PET_MIN_SCALE;

        const petScaleFactor =
            CommonLogicUtil.precisionRoundFloat(
                Box2dConstants.BOX2D_PET_INITIAL_SCALE * scaleFactor, 4);

        return petScaleFactor;
    }

    private lerp(
        start: number,
        end: number,
        t: number) {
        return start * (1 - t) + end * t;
    }

    private inverseLerp(
        start: number,
        end: number,
        a: number) {
        return this.clamp(
            0, 1, (a - start) / (end - start));
    }

    private clamp(
        min: number,
        max: number,
        value: number) {
        return Math.min(max, Math.max(min, value));
    }

    //#endregion

    //#endregion
}
