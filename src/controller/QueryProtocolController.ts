/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import {Socket} from "dgram";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {QueryProtocolConstants} from "../constant/queryProtocol/QueryProtocolConstants";
import {LobbyClientLocations} from "../constant/lobby/LobbyClientLocations";
import {EventHelper} from "../helper/event/EventHelper";
import {A2SProtocol} from "../model/a2s/A2SProtocol";
import {QueryProtocolData} from "../model/queryProtocol/QueryProtocolData";
import {A2SRulesResponsePacketKeyValue} from "../model/a2s/A2SRulesResponsePacketKeyValue";
import {A2SPlayerResponsePacketPlayer} from "../model/a2s/A2SPlayerResponsePacketPlayer";
import {BaseController} from "./BaseController";
import {ReferenceController} from "./ReferenceController";

//#endregion

export class QueryProtocolController
    extends BaseController {
    //#region Variables

    private socket!: Socket;

    private serverId!: number;

    private nextServerId!: number;
    private playerIndex!: number;

    private serversQueryData!: Map<number, QueryProtocolData>;

    private a2sProtocol!: A2SProtocol;

    private updateServerQueryProtocolDataIntervalObject!: NodeJS.Timer;

    //#endregion

    //#region Getters - Setters


    //#endregion

    //#region Constructor

    constructor() {
        super();

        this.serverId = 0;

        this.nextServerId = 1;
        this.playerIndex = 1;

        this.serversQueryData = new Map<number, QueryProtocolData>();

        this.a2sProtocol = new A2SProtocol();
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Query protocol controller initialize start!")
            });

            this.serverId =
                this.registerServerWithQueryData(
                    this.createQueryProtocolData());

            await this.registerQueryProtocolServerEvents();

            this.logManager.showLog({
                log: util.format("Query protocol controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            throw new Error(
                util.format("Query protocol controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Register - Unregister Udp Server Event Methods

    private async registerQueryProtocolServerEvents(): Promise<void> {
        EventHelper.instance.addListener(
            QueryProtocolConstants.QUERY_PROTOCOL_SERVER_EVENTS_CLOSE,
            async () => await this.onQueryProtocolServerClose());

        EventHelper.instance.addListener(
            QueryProtocolConstants.QUERY_PROTOCOL_SERVER_EVENTS_CONNECT,
            async () => await this.onQueryProtocolServerConnect());

        EventHelper.instance.addListener(
            QueryProtocolConstants.QUERY_PROTOCOL_SERVER_EVENTS_ERROR,
            async (error) => await this.onQueryProtocolServerError(error));

        EventHelper.instance.addListener(
            QueryProtocolConstants.QUERY_PROTOCOL_SERVER_EVENTS_LISTENING,
            async (socket) => await this.onQueryProtocolServerListen(socket));

        EventHelper.instance.addListener(
            QueryProtocolConstants.QUERY_PROTOCOL_SERVER_EVENTS_MESSAGE,
            async (message, senderInfo) => await this.onQueryProtocolServerMessage(message, senderInfo));
    }

    private async unregisterQueryProtocolServerEvents(): Promise<void> {
        EventHelper.instance.removeListener(
            QueryProtocolConstants.QUERY_PROTOCOL_SERVER_EVENTS_CLOSE,
            async () => await this.onQueryProtocolServerClose());

        EventHelper.instance.removeListener(
            QueryProtocolConstants.QUERY_PROTOCOL_SERVER_EVENTS_CONNECT,
            async () => await this.onQueryProtocolServerConnect());

        EventHelper.instance.removeListener(
            QueryProtocolConstants.QUERY_PROTOCOL_SERVER_EVENTS_ERROR,
            async (error) => await this.onQueryProtocolServerError(error));

        EventHelper.instance.removeListener(
            QueryProtocolConstants.QUERY_PROTOCOL_SERVER_EVENTS_LISTENING,
            async (socket) => await this.onQueryProtocolServerListen(socket));

        EventHelper.instance.removeListener(
            QueryProtocolConstants.QUERY_PROTOCOL_SERVER_EVENTS_MESSAGE,
            async (message, senderInfo) => await this.onQueryProtocolServerMessage(message, senderInfo));
    }

    private async unregisterAllQueryProtocolServerEvents(): Promise<void> {
        EventHelper.instance.removeAllListeners();
    }

    //#endregion

    //#region Query Protocol Server Event Handler Methods

    private async onQueryProtocolServerClose(): Promise<void> {

    }

    private async onQueryProtocolServerConnect(): Promise<void> {

    }

    private async onQueryProtocolServerError(
        error: string): Promise<void> {

    }

    private async onQueryProtocolServerListen(
        socket: Socket): Promise<void> {
        this.socket = socket;

        this.startUpdateServerQueryProtocolDataLoop();
    }

    private async onQueryProtocolServerMessage(
        message: any,
        senderInfo: any): Promise<void> {
        try {
            if (!this.socket) {
                return;
            }

            if (!Buffer.isBuffer(message)) {
                return;
            }

            const clientAddress =
                senderInfo.address;
            const clientPort =
                senderInfo.port;

            const clientId =
                this.createClientId(
                    clientAddress,
                    clientPort);

            const queryProtocolResponse =
                this.a2sProtocol.receiveData(
                    message, clientId, this.serverId);

            if (!queryProtocolResponse ||
                queryProtocolResponse.length <= 0) {
                return;
            }

            this.socket.send(
                queryProtocolResponse, clientPort, clientAddress,
                (error: Error | null, bytes: number) => {
                    if (error) {
                        this.logManager.showError({
                            error: util.format("Send query response error! Err: %s",
                                error)
                        });

                        return;
                    }
                });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Receive query protocol server message error! Err: %s",
                    error)
            });
        }
    }

    //#endregion

    //#region Query Protocol Methods

    //#region Register Server Methods

    public registerServer(): number {
        const serverId =
            this.nextServerId++;

        this.serversQueryData.set(
            serverId, new QueryProtocolData());

        return serverId;
    }

    public registerServerWithQueryData(
        queryProtocolData: QueryProtocolData): number {
        const serverId =
            this.nextServerId++;

        this.serversQueryData.set(
            serverId, queryProtocolData);

        return serverId;
    }

    //#endregion

    //#region Query Protocol Data Methods

    public getServerQueryProtocolData(
        serverId: number): QueryProtocolData {
        const serversQueryData =
            this.serversQueryData.get(serverId);

        if (!serversQueryData) {
            return new QueryProtocolData();
        }

        return serversQueryData;
    }

    public updateServerQueryProtocolData(
        serverId: number,
        queryProtocolData: QueryProtocolData): void {
        if (!this.serversQueryData.has(serverId)) {
            return;
        }

        this.serversQueryData.set(
            serverId, queryProtocolData)
    }

    //#endregion

    //#region Update Server Query Protocol Data Loop Methods

    private startUpdateServerQueryProtocolDataLoop(): void {
        this.updateServerQueryProtocolDataIntervalObject =
            setInterval(
                () => this.updateServerQueryProtocolDataLoop(),
                QueryProtocolConstants.QUERY_PROTOCOL_SERVER_UPDATE_MILLISECONDS);
    }

    private stopUpdateServerQueryProtocolDataLoop(): void {
        clearInterval(this.updateServerQueryProtocolDataIntervalObject);
    }

    private updateServerQueryProtocolDataLoop(): void {
        const serverQueryProtocolData =
            this.createQueryProtocolData();

        this.updateServerQueryProtocolData(
            this.serverId, serverQueryProtocolData);
    }

    //#endregion

    //#endregion

    //#region Util Methods

    private createClientId(
        address: string,
        port: number): string {
        const clientId =
            util.format("%s:%s",
                address, port);

        return clientId;
    }

    private createQueryProtocolData(): QueryProtocolData {
        const queryProtocolData =
            new QueryProtocolData();

        const lobbyClientsValues =
            ReferenceController.lobbyController.getLobbyClientsValuesWithLocationFilter(
                LobbyClientLocations.InLobby);

        queryProtocolData.a2sServerInfo.protocol = 0;
        queryProtocolData.a2sServerInfo.serverName = "Pethereum Land Box2D";
        queryProtocolData.a2sServerInfo.serverMap = "Zone 1";
        queryProtocolData.a2sServerInfo.folder = "";
        queryProtocolData.a2sServerInfo.gameName = "Pethereum";
        queryProtocolData.a2sServerInfo.steamId = -1;
        queryProtocolData.a2sServerInfo.playerCount = lobbyClientsValues.length;
        queryProtocolData.a2sServerInfo.maxPlayers = 100;
        queryProtocolData.a2sServerInfo.botCount = 0;
        queryProtocolData.a2sServerInfo.serverType = CommonLogicUtil.convertCharToByte("d");
        queryProtocolData.a2sServerInfo.visibility = 0;
        queryProtocolData.a2sServerInfo.valveAntiCheat = 0;
        queryProtocolData.a2sServerInfo.version = "0.1";
        queryProtocolData.a2sServerInfo.extraDataFlag = 0;

        const a2sRulesResponsePacketKeyValue =
            new A2SRulesResponsePacketKeyValue();

        a2sRulesResponsePacketKeyValue.ruleName = "DefaultRuleName";
        a2sRulesResponsePacketKeyValue.ruleValue = "DefaultRuleValue";

        queryProtocolData.a2sServerRules.numRules = 1;
        queryProtocolData.a2sServerRules.rules.push(a2sRulesResponsePacketKeyValue);

        queryProtocolData.a2sPlayerInfo.numPlayers = lobbyClientsValues.length;

        for (const lobbyClient of lobbyClientsValues) {
            const a2sPlayerResponsePacketPlayer =
                new A2SPlayerResponsePacketPlayer();

            a2sPlayerResponsePacketPlayer.index = this.playerIndex;
            a2sPlayerResponsePacketPlayer.playerName = lobbyClient.pet.name ?? "PlayerDefault";
            a2sPlayerResponsePacketPlayer.score = 0;
            a2sPlayerResponsePacketPlayer.duration = 0;

            queryProtocolData.a2sPlayerInfo.players.push(a2sPlayerResponsePacketPlayer);

            this.playerIndex++;
        }

        return queryProtocolData;
    }

    //#endregion

    //#endregion
}
