/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {AuthVerifyResponseInterface} from "../interface/auth/AuthVerifyResponseInterface";
import {JsonWebTokenConstants} from "../constant/jwt/JsonWebTokenConstants";
import {BaseController} from "./BaseController";
import {ReferenceController} from "./ReferenceController";

//#endregion

export class AuthController
    extends BaseController {
    //#region Variables

    //#endregion

    //#region Constructor

    constructor() {
        super();
    }

    //#endregion

    //#region Methods

    //#region Json Web Token Methods

    public async verifyToken(
        token: any): Promise<AuthVerifyResponseInterface> {
        try {
            // this.logManager.showLog({
            //     log: util.format("Verify token start!")
            // });

            const verifyTokenOptions = {
                algorithms: [JsonWebTokenConstants.JWT_ALGORITHM]
            };

            const verifyTokenResponse =
                await ReferenceController.jsonWebTokenController.verify(
                    token, verifyTokenOptions);

            if (!verifyTokenResponse.isSuccess) {
                if (verifyTokenResponse.error) {
                    this.logManager.showError({
                        error: util.format("Verify token error! Err: %s",
                            verifyTokenResponse.error)
                    });
                }

                return {
                    isSuccess: false,
                    error: verifyTokenResponse.error
                }
            }

            // this.logManager.showLog({
            //     log: util.format("Verify token success!")
            // });

            const payload =
                verifyTokenResponse.result;

            return {
                isSuccess: true,
                payload
            }

        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Verify token error! Err: %s",
                    error)
            });

            return {
                isSuccess: false,
                error
            }
        }
    }

    //#endregion

    //#endregion
}
