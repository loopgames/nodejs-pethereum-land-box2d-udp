/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import {v1 as uuidv1} from "uuid";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {BaseController} from "./BaseController";

//#endregion

export class ServerController
    extends BaseController {
    //#region Variables

    private _serverId!: string;

    private _isActive!: boolean;

    //#endregion

    //#region Getters - Setters

    get serverId(): string {
        return this._serverId;
    }

    get isActive(): boolean {
        return this._isActive;
    }

    //#endregion

    //#region Constructor

    constructor() {
        super();

        this._serverId = "";

        this._isActive = false;
    }

    //#endregion

    //#region Methods

    //#region Init

    public init(): void {
        try {
            this.logManager.showLog({
                log: util.format("Server controller initialize start!")
            });

            this._serverId = uuidv1();

            this._isActive = true;

            this.logManager.showLog({
                log: util.format("Server controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Server controller initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Server controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#endregion
}
