/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {ServerController} from "./ServerController";
import {WinstonLogController} from "./WinstonLogController";
import {AuthController} from "./AuthController";
import {JsonWebTokenController} from "./JsonWebTokenController";
import {ServerJsonController} from "./ServerJsonController";
import {QueryProtocolController} from "./QueryProtocolController";
import {ChatController} from "./ChatController";
import {Box2dWorldController} from "./Box2dWorldController";
import {LobbyController} from "./LobbyController";
import {RateLimiterController} from "./RateLimiterController";
import {RedisDatabaseController} from "./RedisDatabaseController";
import {RedisPubSubController} from "./RedisPubSubController";
import {RedisLockController} from "./RedisLockController";

//#endregion

//#region Variables

//#region Non-Lazy Initialization


//#endregion

//#endregion

export abstract class ReferenceController {
    //#region Controller References

    private static _serverController: ServerController;

    private static _winstonLogController: WinstonLogController;

    private static _authController: AuthController;
    private static _jsonWebTokenController: JsonWebTokenController;

    private static _serverJsonController: ServerJsonController;
    private static _queryProtocolController: QueryProtocolController;

    private static _chatController: ChatController;

    private static _box2dWorldController: Box2dWorldController;
    private static _lobbyController: LobbyController;

    private static _rateLimiterController: RateLimiterController;

    private static _redisDatabaseController: RedisDatabaseController;
    private static _redisPubSubController: RedisPubSubController;
    private static _redisLockController: RedisLockController;

    //#endregion

    //#region Getters - Setters

    static get serverController(): ServerController {
        return this._serverController;
    }

    static get winstonLogController(): WinstonLogController {
        return this._winstonLogController;
    }

    static get authController(): AuthController {
        return this._authController;
    }

    static get jsonWebTokenController(): JsonWebTokenController {
        return this._jsonWebTokenController;
    }

    static get serverJsonController(): ServerJsonController {
        return this._serverJsonController;
    }

    static get queryProtocolController(): QueryProtocolController {
        return this._queryProtocolController;
    }

    static get chatController(): ChatController {
        return this._chatController;
    }

    static get box2dWorldController(): Box2dWorldController {
        return this._box2dWorldController;
    }

    static get lobbyController(): LobbyController {
        return this._lobbyController;
    }

    static get rateLimiterController(): RateLimiterController {
        return this._rateLimiterController;
    }

    static get redisDatabaseController(): RedisDatabaseController {
        return this._redisDatabaseController;
    }

    static get redisPubSubController(): RedisPubSubController {
        return this._redisPubSubController;
    }

    static get redisLockController(): RedisLockController {
        return this._redisLockController;
    }

    //#endregion

    //#region Methods

    //#region Init

    public static init(): void {
        this._serverController = new ServerController();

        this._winstonLogController = new WinstonLogController();

        this._authController = new AuthController();
        this._jsonWebTokenController = new JsonWebTokenController();

        this._serverJsonController = new ServerJsonController();
        this._queryProtocolController = new QueryProtocolController();

        this._chatController = new ChatController();

        this._box2dWorldController = new Box2dWorldController();
        this._lobbyController = new LobbyController();

        this._rateLimiterController = new RateLimiterController();

        this._redisDatabaseController = new RedisDatabaseController();
        this._redisPubSubController = new RedisPubSubController();
        this._redisLockController = new RedisLockController();
    }

    //#endregion

    //#endregion
}
