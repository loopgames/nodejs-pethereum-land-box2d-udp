/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import path from "path";
import winston, {createLogger, Logger} from "winston";
import DailyRotateFile from "winston-daily-rotate-file";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {CommandLineHelper} from "../helper/command/CommandLineHelper";
import {BaseController} from "./BaseController";
import {ReferenceController} from "./ReferenceController";

//#endregion

export class WinstonLogController
    extends BaseController {
    //#region Variables

    private _isInitialized!: boolean;

    private _winstonLogger!: Logger;

    //#endregion

    //#region Getters - Setters

    get winstonLogger(): Logger {
        return this._winstonLogger;
    }

    //#endregion

    //#region Constructor

    constructor() {
        super();

        this._isInitialized = false;
    }

    //#endregion

    //#region Methods

    //#region Init

    public init(): void {
        try {
            this.logManager.showLog({
                log: util.format("Winston controller initialize start!")
            });

            const combinedLogPathCommandLineArgument =
                CommandLineHelper.instance.getCommandLineArgument("COMBINED_LOG_PATH");
            const errorLogPathCommandLineArgument =
                CommandLineHelper.instance.getCommandLineArgument("ERROR_LOG_PATH");

            const combinedLogPath =
                combinedLogPathCommandLineArgument || "./logs/combined";
            const errorLogPath =
                errorLogPathCommandLineArgument || "./logs/error";

            const serverId = ReferenceController.serverController.serverId;

            const winstonCombinedTransport = new DailyRotateFile({
                level: "info",
                dirname: combinedLogPath,
                filename: util.format("%s-lobbyudp-combined-%DATE%.log", serverId),
                datePattern: "YYYY-MM-DD-HH",
                zippedArchive: true,
                maxSize: "300k",
                maxFiles: 5
            });
            const winstonErrorTransport = new DailyRotateFile({
                level: "error",
                dirname: errorLogPath,
                filename: util.format("%s-lobbyudp-error-%DATE%.log", serverId),
                datePattern: "YYYY-MM-DD-HH",
                zippedArchive: true,
                maxSize: "300k",
                maxFiles: 5
            });

            this._winstonLogger = createLogger({
                level: "info",
                format: winston.format.json(),
                defaultMeta: {service: 'user-service'},
                transports: [
                    winstonCombinedTransport,
                    winstonErrorTransport
                ],
            });

            this._isInitialized = true;

            this.logManager.showLog({
                log: util.format("Winston controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Winston controller initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Winston controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Winston Methods

    public writeLog(
        log: string): void {
        try {
            if (!this._isInitialized) return;

            let logFormat =
                this.getWriteFormat(
                    log);

            this.winstonLogger.info(
                logFormat);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);
        }
    }

    public writeWarn(
        warning: string): void {
        try {
            if (!this._isInitialized) return;

            let warnFormat =
                this.getWriteFormat(
                    warning);

            this.winstonLogger.warn(
                warnFormat);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);
        }
    }

    public writeError(
        error: string): void {
        try {
            if (!this._isInitialized) return;

            let errorFormat =
                this.getWriteFormat(
                    error);

            this.winstonLogger.error(
                errorFormat);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);
        }
    }

    //#endregion

    //#region Util Methods

    private getWriteFormat(
        log: string): string {
        const timestamp =
            CommonLogicUtil.getCurrentSecondTimestamp();

        const writeFormat =
            util.format("[%s] - %s", timestamp, log);

        return writeFormat;
    }

    //#endregion

    //#endregion
}
