/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import {credential} from "firebase-admin";
import applicationDefault = credential.applicationDefault;

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {FirebaseApp} from "../model/firebase/FirebaseApp";
import {BaseController} from "./BaseController";

//#endregion

export class FirebaseController
    extends BaseController {
    //#region Variables

    private readonly firebaseProjectId!: string;

    private readonly _firebaseApp!: FirebaseApp;

    //#endregion

    //#region Getters - Setters

    get firebaseApp(): FirebaseApp {
        return this._firebaseApp;
    }

    //#endregion

    //#region Constructor

    constructor() {
        super();

        const firebaseProjectId =
            process.env.FIREBASE_PROJECT_ID || "";

        this.firebaseProjectId = firebaseProjectId;

        this._firebaseApp = new FirebaseApp();
    }

    //#endregion

    //#region Methods

    //#region Init

    public init(): void {
        try {
            this.logManager.showLog({
                log: util.format("Firebase controller initialize start!")
            });

            const firebaseOptions = {
                credential: applicationDefault(),
                projectId: this.firebaseProjectId
            };

            this._firebaseApp.init(
                firebaseOptions);

            this.logManager.showLog({
                log: util.format("Firebase controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Firebase controller initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Firebase controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#endregion
}
