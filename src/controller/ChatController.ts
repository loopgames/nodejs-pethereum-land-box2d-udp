/**
 * Created by hulusionder on 10/02/2022
 */

"use strict";

//#region Imports

import util from "util";
import {v1 as uuidv1} from "uuid";
import {Namespace, Server, Socket} from "socket.io";
import {deserialize} from "typescript-json-serializer";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {ServerReturnCodes} from "../constant/server/ServerReturnCodes";
import {SocketConstants} from "../constant/socket/SocketConstants";
import {ClientChatMessageTypes} from "../constant/chat/ClientChatMessageTypes";
import {EventHelper} from "../helper/event/EventHelper";
import {CryptoHelper} from "../helper/crypto/CryptoHelper";
import {BaseChatRequestMessage} from "../model/chat/message/BaseChatRequestMessage";
import {BaseChatResponseMessage} from "../model/chat/message/BaseChatResponseMessage";
import {JoinChatRequestMessage} from "../model/chat/message/JoinChatRequestMessage";
import {JoinChatSelfResponseMessage} from "../model/chat/message/JoinChatSelfResponseMessage";
import {JoinChatOtherResponseMessage} from "../model/chat/message/JoinChatOtherResponseMessage";
import {LeaveChatRequestMessage} from "../model/chat/message/LeaveChatRequestMessage";
import {LeaveChatSelfResponseMessage} from "../model/chat/message/LeaveChatSelfResponseMessage";
import {LeaveChatOtherResponseMessage} from "../model/chat/message/LeaveChatOtherResponseMessage";
import {SendMessageChatRequestMessage} from "../model/chat/message/SendMessageChatRequestMessage";
import {UpdateChatResponseMessage} from "../model/chat/message/UpdateChatResponseMessage";
import {ChatUser} from "../model/chat/ChatUser";
import {ChatMessagePayload} from "../model/chat/ChatMessagePayload";
import {BaseController} from "./BaseController";
import {ReferenceController} from "./ReferenceController";

//#endregion

export class ChatController
    extends BaseController {
    //#region Variables

    private serverId!: string;

    private io!: Server;
    private nsp!: Namespace;

    private chatUsers!: Map<string, ChatUser>;

    private chatUsersSocket!: Map<string, Socket>;

    private chatUsersLastMessageSentTimestamp!: Map<string, number>;
    private chatUsersLastPongSentTimestamp!: Map<string, number>;

    //#endregion

    //#region Getters - Setters


    //#endregion

    //#region Constructor

    constructor() {
        super();

        this.serverId = uuidv1();

        this.chatUsers = new Map<string, ChatUser>();

        this.chatUsersSocket = new Map<string, Socket>();

        this.chatUsersLastMessageSentTimestamp = new Map<string, number>();
        this.chatUsersLastPongSentTimestamp = new Map<string, number>();
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Chat controller initialize start!")
            });

            await this.registerSocketServerEvents();

            this.logManager.showLog({
                log: util.format("Chat controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Chat controller initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Chat controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Register - Unregister Socket Server Event Methods

    private async registerSocketServerEvents(): Promise<void> {
        EventHelper.instance.addListener(
            SocketConstants.SOCKET_PREDEFINED_EVENTS_LISTENING,
            async (io, nsp) => await this.onSocketServerListening(io, nsp));

        EventHelper.instance.addListener(
            SocketConstants.SOCKET_PREDEFINED_EVENTS_CONNECTION,
            async (socket) => await this.onSocketServerConnection(socket));
    }

    private async unregisterSocketServerEvents(): Promise<void> {
        EventHelper.instance.removeListener(
            SocketConstants.SOCKET_PREDEFINED_EVENTS_LISTENING,
            async (io, nsp) => await this.onSocketServerListening(io, nsp));

        EventHelper.instance.removeListener(
            SocketConstants.SOCKET_PREDEFINED_EVENTS_CONNECTION,
            async (socket) => await this.onSocketServerConnection(socket));
    }

    private unregisterAllWssServerEvents(): void {
        EventHelper.instance.removeAllListeners();
    }

    //#endregion

    //#region Wss Server Event Handler Methods

    private async onSocketServerListening(
        io: Server,
        nsp: Namespace): Promise<void> {
        this.io = io;
        this.nsp = nsp;
    }

    private async onSocketServerConnection(
        socket: Socket): Promise<void> {
        if (!this.io) {
            return;
        }
        if (!this.nsp) {
            return;
        }

        const chatUserId =
            (socket as any).chatUserId;
        if (!chatUserId) {
            socket.disconnect(true);
            return;
        }

        // this.logManager.showLog({
        //     log: util.format("User connected to chat! Id: %s",
        //         chatUserId)
        // });

        await this.registerSocketEvents(
            socket);

        this.removeChatUser(
            chatUserId);
        this.removeChatUserSocket(
            chatUserId);
        this.removeChatUserLastMessageSentTimestamp(
            chatUserId);
        this.removeChatUserLastPongSentTimestamp(
            chatUserId);

        this.addChatUser(
            chatUserId);
        this.addChatUserSocket(
            chatUserId,
            socket);

        const chatUser =
            this.getChatUser(
                chatUserId);
        if (!chatUser) {
            return;
        }

        this.setChatUserLastPongSentTimestamp(
            chatUserId,
            CommonLogicUtil.getCurrentSecondTimestamp());

        let chatUsersArr =
            this.getChatUsersValues();

        const joinChatSelfResponseMessage =
            new JoinChatSelfResponseMessage(true, ServerReturnCodes.OK, chatUser, chatUsersArr);
        const stringifyJoinChatSelfResponseMessage =
            JSON.stringify(joinChatSelfResponseMessage.serialize());
        const encryptedJoinChatSelfResponseMessage =
            CryptoHelper.instance.encrypt(stringifyJoinChatSelfResponseMessage);

        const joinChatOtherResponseMessage =
            new JoinChatOtherResponseMessage(true, ServerReturnCodes.OK, chatUser);
        const stringifyJoinChatOtherResponseMessage =
            JSON.stringify(joinChatOtherResponseMessage.serialize());
        const encryptedJoinChatOtherResponseMessage =
            CryptoHelper.instance.encrypt(stringifyJoinChatOtherResponseMessage);

        this.broadcastMessageToSpecificChatUser(
            encryptedJoinChatSelfResponseMessage,
            chatUserId);

        this.broadcastMessageToAllChatUsersExceptGivenOne(
            encryptedJoinChatOtherResponseMessage,
            socket);
    }

    private async onSocketServerClose(): Promise<void> {

    }

    //#endregion

    //#region Register - Unregister Socket Event Methods

    private async registerSocketEvents(
        socket: Socket): Promise<void> {
        if (!this.io) {
            return;
        }
        if (!this.nsp) {
            return;
        }

        socket.addListener(
            SocketConstants.SOCKET_PREDEFINED_EVENTS_DISCONNECT,
            async (reason) => this.onSocketDisconnect(socket, reason));

        socket.on(
            SocketConstants.SOCKET_PREDEFINED_EVENTS_MESSAGE,
            (message) => this.onSocketMessage(socket, message));
    }

    private async unregisterSocketEvents(
        socket: Socket): Promise<void> {
        if (!this.io) {
            return;
        }
        if (!this.nsp) {
            return;
        }

        socket.removeListener(
            SocketConstants.SOCKET_PREDEFINED_EVENTS_DISCONNECT,
            async (reason) => this.onSocketDisconnect(socket, reason));

        socket.removeListener(
            SocketConstants.SOCKET_PREDEFINED_EVENTS_MESSAGE,
            async (message) => this.onSocketMessage(socket, message));
    }

    private unregisterAllSocketEvents(): void {
        if (!this.io) {
            return;
        }
        if (!this.nsp) {
            return;
        }

        EventHelper.instance.removeAllListeners();
    }

    //#endregion

    //#region Socket Event Handler Methods

    private async onSocketDisconnect(
        socket: Socket,
        reason: string): Promise<void> {
        await this.unregisterSocketEvents(
            socket);

        const chatUserId =
            (socket as any).chatUserId;
        if (!chatUserId) {
            socket.disconnect(true);
            return;
        }

        // this.logManager.showLog({
        //     log: util.format("User disconnected from chat! Id: %s",
        //         chatUserId)
        // });

        this.removeChatUser(
            chatUserId);
        this.removeChatUserSocket(
            chatUserId);
        this.removeChatUserLastMessageSentTimestamp(
            chatUserId);
        this.removeChatUserLastPongSentTimestamp(
            chatUserId);
    }

    private async onSocketMessage(
        socket: Socket,
        message: string): Promise<void> {
        try {
            const chatUserId =
                (socket as any).chatUserId;
            if (!chatUserId) {
                socket.disconnect(true);
                return;
            }

            // const rateLimiterConsumeResponse =
            //     await ReferenceController.rateLimiterController.consume(
            //         chatUserId, 1);
            //
            // if (!rateLimiterConsumeResponse.isSuccess) {
            //     socket.disconnect(true);
            //     return;
            // }

            if (Buffer.isBuffer(message)) {
                message =
                    Buffer.from(message).toString();
            }

            const decryptedMessage =
                CryptoHelper.instance.decrypt(message);
            if (!decryptedMessage) {
                return;
            }

            const parsedMessage =
                JSON.parse(decryptedMessage);
            if (!parsedMessage) {
                return;
            }

            const baseChatRequestMessage = deserialize<BaseChatRequestMessage>(
                parsedMessage, BaseChatRequestMessage);
            if (!baseChatRequestMessage) {
                return;
            }

            const chatMessageType =
                baseChatRequestMessage.chatMessageType;

            switch (chatMessageType) {
                case ClientChatMessageTypes.Join:
                    const joinChatRequestMessage = deserialize<JoinChatRequestMessage>(
                        parsedMessage, JoinChatRequestMessage);

                    this.processJoinChatRequestMessage(
                        socket, chatUserId, joinChatRequestMessage);
                    break;
                case ClientChatMessageTypes.Leave:
                    const leaveChatRequestMessage = deserialize<LeaveChatRequestMessage>(
                        parsedMessage, LeaveChatRequestMessage);

                    this.processLeaveChatRequestMessage(
                        socket, chatUserId, leaveChatRequestMessage);
                    break;
                case ClientChatMessageTypes.SendMessage:
                    const sendMessageChatRequestMessage = deserialize<SendMessageChatRequestMessage>(
                        parsedMessage, SendMessageChatRequestMessage);

                    this.processSendMessageChatRequestMessage(
                        socket, chatUserId, sendMessageChatRequestMessage);
                    break;
            }
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Receive socket server message error! Err: %s",
                    error)
            });
        }
    }

    //#endregion

    //#region Join Request Methods

    private processJoinChatRequestMessage(
        socket: Socket,
        chatUserId: string,
        joinChatRequestMessage: JoinChatRequestMessage): void {
        if (!chatUserId) {
            return;
        }

        // this.logManager.showLog({
        //     log: util.format("User joined to chat! Id: %s",
        //         chatUserId)
        // });

        this.removeChatUser(
            chatUserId);
        this.removeChatUserSocket(
            chatUserId);

        this.addChatUser(
            chatUserId);
        this.addChatUserSocket(
            chatUserId,
            socket);

        const chatUser =
            this.getChatUser(
                chatUserId);
        if (!chatUser) {
            return;
        }

        this.setChatUserLastPongSentTimestamp(
            chatUserId,
            CommonLogicUtil.getCurrentSecondTimestamp());

        let chatUsersArr =
            this.getChatUsersValues();

        const joinChatSelfResponseMessage =
            new JoinChatSelfResponseMessage(true, ServerReturnCodes.OK, chatUser, chatUsersArr);
        const stringifyJoinChatSelfResponseMessage =
            JSON.stringify(joinChatSelfResponseMessage.serialize());
        const encryptedJoinChatSelfResponseMessage =
            CryptoHelper.instance.encrypt(stringifyJoinChatSelfResponseMessage);

        const joinChatOtherResponseMessage =
            new JoinChatOtherResponseMessage(true, ServerReturnCodes.OK, chatUser);
        const stringifyJoinChatOtherResponseMessage =
            JSON.stringify(joinChatOtherResponseMessage.serialize());
        const encryptedJoinChatOtherResponseMessage =
            CryptoHelper.instance.encrypt(stringifyJoinChatOtherResponseMessage);

        this.broadcastMessageToSpecificChatUser(
            encryptedJoinChatSelfResponseMessage,
            chatUserId);

        this.broadcastMessageToAllChatUsersExceptGivenOne(
            encryptedJoinChatOtherResponseMessage,
            socket);
    }

    //#endregion

    //#region Leave Request Methods

    private processLeaveChatRequestMessage(
        socket: Socket,
        chatUserId: string,
        leaveChatRequestMessage: LeaveChatRequestMessage): void {
        const chatUser =
            this.getChatUser(
                chatUserId);
        if (!chatUser) {
            return;
        }

        // this.logManager.showLog({
        //     log: util.format("User leaved from chat! Id: %s",
        //         chatUserId)
        // });

        this.removeChatUser(
            chatUserId);
        this.removeChatUserSocket(
            chatUserId);

        const leaveChatSelfResponseMessage =
            new LeaveChatSelfResponseMessage(true, ServerReturnCodes.OK, chatUserId);
        const stringifyLeaveChatSelfResponseMessage =
            JSON.stringify(leaveChatSelfResponseMessage.serialize());
        const encryptedLeaveChatSelfResponseMessage =
            CryptoHelper.instance.encrypt(stringifyLeaveChatSelfResponseMessage);

        const leaveChatOtherResponseMessage =
            new LeaveChatOtherResponseMessage(true, ServerReturnCodes.OK, chatUserId);
        const stringifyLeaveChatOtherResponseMessage =
            JSON.stringify(leaveChatOtherResponseMessage.serialize());
        const encryptedLeaveChatOtherResponseMessage =
            CryptoHelper.instance.encrypt(stringifyLeaveChatOtherResponseMessage);

        this.broadcastMessageToSpecificChatUser(
            encryptedLeaveChatSelfResponseMessage,
            chatUserId);

        this.broadcastMessageToAllChatUsersExceptGivenOne(
            encryptedLeaveChatOtherResponseMessage,
            socket);
    }

    //#endregion

    //#region Send Message Request Methods

    private processSendMessageChatRequestMessage(
        socket: Socket,
        chatUserId: string,
        sendMessageChatRequestMessage: SendMessageChatRequestMessage): void {
        const message =
            sendMessageChatRequestMessage.message;

        // console.log("Send Message -> " + chatUserId);

        this.setChatUserLastMessageSentTimestamp(
            chatUserId,
            CommonLogicUtil.getCurrentSecondTimestamp());

        const chatUser =
            this.getChatUser(
                chatUserId);
        if (!chatUser) {
            return;
        }

        const chatMessagePayload =
            new ChatMessagePayload(chatUserId, message);
        const updateChatResponseMessage =
            new UpdateChatResponseMessage(true, ServerReturnCodes.OK, chatMessagePayload);
        const stringifyUpdateChatResponseMessage =
            JSON.stringify(updateChatResponseMessage.serialize());
        const encryptedUpdateChatResponseMessage =
            CryptoHelper.instance.encrypt(stringifyUpdateChatResponseMessage);

        this.broadcastMessageToAllChatUsers(
            encryptedUpdateChatResponseMessage);
    }

    //#endregion

    //#region Get - Add - Remove Chat User Methods

    public getChatUsersKeys(): string[] {
        let chatUsersKeysArr =
            [...this.chatUsers.keys()];

        return chatUsersKeysArr;
    }

    public getChatUsersValues(): ChatUser[] {
        let chatUsersValuesArr =
            [...this.chatUsers.values()];

        return chatUsersValuesArr;
    }

    private addChatUser(
        chatUserId: string): void {
        const chatUser =
            new ChatUser(
                chatUserId);

        this.chatUsers.set(
            chatUserId,
            chatUser);
    }

    private getChatUser(
        chatUserId: string): ChatUser | undefined {
        if (!this.chatUsers.has(chatUserId)) {
            return undefined;
        }

        return this.chatUsers.get(chatUserId);
    }

    private removeChatUser(
        chatUserId: string): void {
        if (!this.chatUsers.has(chatUserId)) {
            return;
        }

        this.chatUsers.delete(chatUserId);
    }

    //#endregion

    //#region Get - Add - Remove Chat User Socket Methods

    public getChatUsersSocketKeys(): string[] {
        let chatUsersSocketKeysArr =
            [...this.chatUsersSocket.keys()];

        return chatUsersSocketKeysArr;
    }

    public getChatUsersSocketValues(): Socket[] {
        let chatUsersSocketValuesArr =
            [...this.chatUsersSocket.values()];

        return chatUsersSocketValuesArr;
    }

    private addChatUserSocket(
        chatUserId: string,
        socket: Socket): void {
        this.chatUsersSocket.set(
            chatUserId,
            socket);
    }

    private getChatUserSocket(
        chatUserId: string): Socket | undefined {
        if (!this.chatUsersSocket.has(chatUserId)) {
            return undefined;
        }

        return this.chatUsersSocket.get(chatUserId);
    }

    private removeChatUserSocket(
        chatUserId: string): void {
        if (!this.chatUsersSocket.has(chatUserId)) {
            return;
        }

        this.chatUsersSocket.delete(chatUserId);
    }

    //#endregion

    //#region Set - Get - Remove Chat User Last Message Sent Timestamp Methods

    private setChatUserLastMessageSentTimestamp(
        chatUserId: string,
        messageSentTimestamp: number): void {
        this.chatUsersLastMessageSentTimestamp.set(
            chatUserId,
            messageSentTimestamp);
    }

    private getChatUserLastMessageSentTimestamp(
        chatUserId: string): number | undefined {
        if (!this.chatUsersLastMessageSentTimestamp.has(chatUserId)) {
            return undefined;
        }

        return this.chatUsersLastMessageSentTimestamp.get(chatUserId);
    }

    private removeChatUserLastMessageSentTimestamp(
        chatUserId: string): void {
        if (!this.chatUsersLastMessageSentTimestamp.has(chatUserId)) {
            return;
        }

        this.chatUsersLastMessageSentTimestamp.delete(chatUserId);
    }

    //#endregion

    //#region Set - Get - Remove Chat User Last Message Sent Timestamp Methods

    private setChatUserLastPongSentTimestamp(
        chatUserId: string,
        pongSentTimestamp: number): void {
        this.chatUsersLastPongSentTimestamp.set(
            chatUserId,
            pongSentTimestamp);
    }

    private getChatUserLastPongSentTimestamp(
        chatUserId: string): number | undefined {
        if (!this.chatUsersLastPongSentTimestamp.has(chatUserId)) {
            return undefined;
        }

        return this.chatUsersLastPongSentTimestamp.get(chatUserId);
    }

    private removeChatUserLastPongSentTimestamp(
        chatUserId: string): void {
        if (!this.chatUsersLastPongSentTimestamp.has(chatUserId)) {
            return;
        }

        this.chatUsersLastPongSentTimestamp.delete(chatUserId);
    }

    //#endregion

    //#region Broadcast Methods

    private broadcastMessageToAllChatUsers(
        message: string): void {
        try {
            this.nsp.emit(
                SocketConstants.SOCKET_PREDEFINED_EVENTS_MESSAGE, message);
        } catch (exception) {
            this.logManager.showError({
                error: util.format("Broadcast message to all chat users exception! Exception: %s",
                    exception)
            });
        }
    }

    private broadcastMessageToAllChatUsersExceptGivenOne(
        message: string,
        socket: Socket): void {
        try {
            socket.broadcast.emit(
                SocketConstants.SOCKET_PREDEFINED_EVENTS_MESSAGE, message);
        } catch (exception) {
            this.logManager.showError({
                error: util.format("Broadcast message to all chat users except given one exception! Exception: %s",
                    exception)
            });
        }
    }

    private broadcastMessageToSpecificChatUser(
        message: string,
        socketId: string): void {
        try {
            this.nsp
                .to(socketId)
                .emit(SocketConstants.SOCKET_PREDEFINED_EVENTS_MESSAGE, message);
        } catch (exception) {
            this.logManager.showError({
                error: util.format("Broadcast message to specific chat user exception! Exception: %s",
                    exception)
            });
        }
    }

    //#endregion

    //#region Util Methods

    //#endregion

    //#endregion
}
