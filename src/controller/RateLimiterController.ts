/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import fs from "fs";
import util from "util";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {BaseController} from "./BaseController";
import {RedisDatabase} from "../model/database/RedisDatabase";
import {RedisRateLimiter} from "../model/rateLimiter/RedisRateLimiter";
import {RedisRateLimiterResponseInterface} from "../interface/rateLimiter/RedisRateLimiterResponseInterface";

//#endregion

export class RateLimiterController
    extends BaseController {
    //#region Variables

    private readonly redisDatabaseUri!: string;

    private readonly _redisDatabase!: RedisDatabase;
    private readonly _rateLimiterRedis!: RedisRateLimiter;

    //#endregion

    //#region Getters - Setters

    get redisDatabase(): RedisDatabase {
        return this._redisDatabase;
    }

    get rateLimiterRedis(): RedisRateLimiter {
        return this._rateLimiterRedis;
    }

    //#endregion

    //#region Constructor

    constructor() {
        super();

        const redisDatabaseUri =
            process.env.REDIS_DATABASE_URI || "";

        this.redisDatabaseUri = redisDatabaseUri;

        this._redisDatabase = new RedisDatabase();
        this._rateLimiterRedis = new RedisRateLimiter();
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Rate limit controller initialize start!")
            });

            const redisDatabaseOptions = {
                lazyConnect: true
            };

            await this._redisDatabase.init(
                this.redisDatabaseUri, redisDatabaseOptions);

            const redisClient =
                this._redisDatabase.client;

            const redisRateLimiterOptions = {
                storeClient: redisClient,
                points: 4,
                duration: 1,
                execEvenly: false,
                blockDuration: 0,
                keyPrefix: "rl:ws",
            };

            await this._rateLimiterRedis.init(
                redisRateLimiterOptions);

            this.logManager.showLog({
                log: util.format("Rate limit controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Rate limit controller initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Rate limit controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Rate Limiter Methods

    //#region Consume Methods

    public async consume(
        remoteAddress: string,
        pointsToConsume?: number | undefined): Promise<RedisRateLimiterResponseInterface> {
        return await this._rateLimiterRedis.consume(
            remoteAddress, pointsToConsume);
    }

    //#endregion

    //#endregion

    //#endregion
}
