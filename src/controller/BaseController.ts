/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {LogManager} from "../manager/LogManager";

//#endregion

export abstract class BaseController {
    //#region Variables

    protected readonly logManager!: LogManager;

    //#endregion

    //#region Constructor

    protected constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);
    }

    //#endregion
}
