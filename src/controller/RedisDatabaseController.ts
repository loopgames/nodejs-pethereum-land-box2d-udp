/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import {KeyType, ValueType} from "ioredis";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {RedisDatabase} from "../model/database/RedisDatabase";
import {RedisDatabaseResponseInterface} from "../interface/redis/RedisDatabaseResponseInterface";
import {BaseController} from "./BaseController";

//#endregion

export class RedisDatabaseController
    extends BaseController {
    //#region Variables

    private readonly redisDatabaseUri!: string;

    private readonly _redisDatabase!: RedisDatabase;

    //#endregion

    //#region Getters - Setters

    get redisDatabase(): RedisDatabase {
        return this._redisDatabase;
    }

    //#endregion

    //#region Constructor

    constructor() {
        super();

        const redisDatabaseUri =
            process.env.REDIS_DATABASE_URI || "";

        this.redisDatabaseUri = redisDatabaseUri;

        this._redisDatabase = new RedisDatabase();
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Redis database controller initialize start!")
            });

            const redisDatabaseOptions = {
                lazyConnect: true
            };

            await this._redisDatabase.init(
                this.redisDatabaseUri, redisDatabaseOptions);

            this.logManager.showLog({
                log: util.format("Redis database controller initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Redis database controller initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Redis database controller initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Database Methods

    //#region Health Check Methods

    public async healthCheck(): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.healthCheck();
    }

    //#endregion

    //#region Select Database Methods

    public async selectDatabase(
        index: number): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.selectDatabase(
            index);
    }

    //#endregion

    //#region Flush Database Methods

    public async flushDatabase(): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.flushDatabase();
    }

    public async flushAllDatabases(): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.flushAllDatabases();
    }

    //#endregion

    //#region Rename Methods

    public async renameKey(
        key: KeyType,
        newKey: KeyType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.renameKey(
            key, newKey);
    }

    //#endregion

    //#region Expire Methods

    public async expireKeyAt(
        key: KeyType,
        timestamp: number): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.expireKeyAt(
            key, timestamp);
    }

    public async getKeyRemainingTime(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getKeyRemainingTime(
            key);
    }

    //#endregion

    //#region List Methods

    public async getListLength(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getListLength(
            key);
    }

    public async getListElement(
        key: KeyType,
        index: number): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getListElement(
            key, index);
    }

    public async setListElement(
        key: KeyType,
        index: number,
        value: ValueType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.setListElement(
            key, index, value);
    }

    public async insertListElementToHead(
        key: KeyType,
        element: ValueType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.insertListElementToHead(
            key, element);
    }

    public async insertListElementsToHead(
        key: KeyType,
        elements: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.insertListElementsToHead(
            key, elements);
    }

    public async insertListElementToHeadOnlyIfKeyExist(
        key: KeyType,
        element: ValueType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.insertListElementToHeadOnlyIfKeyExist(
            key, element);
    }

    public async insertListElementsToHeadOnlyIfKeyExist(
        key: KeyType,
        elements: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.insertListElementsToHeadOnlyIfKeyExist(
            key, elements);
    }

    public async insertListElementToTail(
        key: KeyType,
        element: ValueType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.insertListElementToTail(
            key, element);
    }

    public async insertListElementsToTail(
        key: KeyType,
        elements: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.insertListElementsToTail(
            key, elements);
    }

    public async insertListElementToTailOnlyIfKeyExist(
        key: KeyType,
        element: ValueType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.insertListElementToTailOnlyIfKeyExist(
            key, element);
    }

    public async insertListElementsToTailOnlyIfKeyExist(
        key: KeyType,
        elements: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.insertListElementsToTailOnlyIfKeyExist(
            key, elements);
    }

    public async insertListElementWithPivot(
        key: KeyType,
        direction: 'BEFORE' | 'AFTER',
        pivot: string,
        value: ValueType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.insertListElementWithPivot(
            key, direction, pivot, value);
    }

    public async removeAndGetFirstListElement(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.removeAndGetFirstListElement(
            key);
    }

    public async removeAndGetFirstListElements(
        key: KeyType,
        count: number): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.removeAndGetFirstListElements(
            key, count);
    }

    public async removeAndGetLastListElement(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.removeAndGetLastListElement(
            key);
    }

    public async removeAndGetLastListElements(
        key: KeyType,
        count: number): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.removeAndGetLastListElements(
            key, count);
    }

    public async getListElementsInRange(
        key: KeyType,
        start: number,
        stop: number): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getListElementsInRange(
            key, start, stop);
    }

    public async removeListElement(
        key: KeyType,
        count: number,
        value: ValueType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.removeListElement(
            key, count, value);
    }

    public async trimListElements(
        key: KeyType,
        start: number,
        stop: number): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.trimListElements(
            key, start, stop);
    }

    public async findListElementIndex(
        key: KeyType,
        value: ValueType,
        rank?: number,
        count?: number,
        maxlen?: number): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.findListElementIndex(
            key, value, rank, count, maxlen);
    }

    //#endregion

    //#region Key Value Methods

    public async isKeyExist(
        keys: KeyType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.isKeyExist(
            keys);
    }

    public async setKeyValue(
        key: KeyType,
        value: KeyType,
        expiryMode?: string | any[] | undefined,
        time?: string | number | undefined,
        setMode?: string | number | undefined): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.setKeyValue(
            key, value, expiryMode, time, setMode);
    }

    public async getKeyValue(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getKeyValue(
            key);
    }

    public async scan(
        cursor: number | string,
        matchOption: "match" | "MATCH",
        pattern: string,
        countOption: "count" | "COUNT",
        count: number): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.scan(
            cursor, matchOption, pattern, countOption, count);
    }

    public async deleteKey(
        keys: KeyType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.deleteKey(
            keys);
    }

    //#endregion

    //#region Hash Methods

    public async isHashFieldExists(
        key: KeyType,
        field: string): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.isHashFieldExists(
            key, field);
    }

    public async getHashFields(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getHashFields(
            key);
    }

    public async getHashValue(
        key: KeyType,
        field: string): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getHashValue(
            key, field);
    }

    public async getHashValues(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getHashValues(
            key);
    }

    public async getHashValuesWithGivenKeys(
        key: KeyType,
        args: KeyType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getHashValuesWithGivenKeys(
            key, args);
    }

    public async getAllHashValues(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getAllHashValues(
            key);
    }

    public async setHashValue(
        key: KeyType,
        args: ValueType[] | {[p: string]: ValueType} | Map<string, ValueType>): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.setHashValue(
            key, args);
    }

    public async scanHash(
        key: KeyType,
        args: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.scanHash(
            key, args);
    }

    public async deleteHashKey(
        key: KeyType,
        args: KeyType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.deleteHashKey(
            key, args);
    }

    //#endregion

    //#region Sorted Methods

    public async getSortedMembers(
        key: KeyType,
        args: KeyType | number[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getSortedMembers(
            key, args);
    }

    public async setSortedMember(
        key: KeyType,
        args: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.setSortedMember(
            key, args);
    }

    public async getSortedMemberWithScore(
        key: KeyType,
        member: string): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getSortedMemberWithScore(
            key, member);
    }

    public async setSortedMemberWithScore(
        key: KeyType,
        args: (KeyType | number)[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.setSortedMemberWithScore(
            key, args);
    }

    public async getSortedKeyCount(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getSortedKeyCount(
            key);
    }

    public async getSortedMembersInRangeByAscendingOrder(
        key: KeyType,
        start: number,
        stop: number,
        withScores: "WITHSCORES"): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getSortedMembersInRangeByAscendingOrder(
            key, start, stop, withScores);
    }

    public async getSortedMembersInRangeByDescendingOrder(
        key: KeyType,
        start: number,
        stop: number,
        withScores: "WITHSCORES"): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getSortedMembersInRangeByDescendingOrder(
            key, start, stop, withScores);
    }

    public async getRankOfMemberByAscendingOrder(
        key: KeyType,
        member: string): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getRankOfMemberByAscendingOrder(
            key, member);
    }

    public async getRankOfMemberByDescendingOrder(
        key: KeyType,
        member: string): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getRankOfMemberByDescendingOrder(
            key, member);
    }

    public async getScoreOfMember(
        key: KeyType,
        member: string): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.getScoreOfMember(
            key, member);
    }

    public async deleteSortedMember(
        key: KeyType,
        args: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        return await this._redisDatabase.deleteSortedMember(
            key, args);
    }

    //#endregion

    //#endregion

    //#endregion
}
