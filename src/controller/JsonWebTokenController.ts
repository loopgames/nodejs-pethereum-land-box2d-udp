/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import fs from "fs";
import path from "path";
import util from "util";
import jwt from "jsonwebtoken";

import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {JsonWebToken} from "../model/jwt/JsonWebToken";
import {JsonWebTokenResponseInterface} from "../interface/jwt/JsonWebTokenResponseInterface";
import {BaseController} from "./BaseController";

//#endregion

export class JsonWebTokenController
    extends BaseController {
    //#region Variables

    private readonly verifyPublicKeySecret!: string;

    public jsonWebToken!: JsonWebToken;

    //#endregion

    //#region Constructor

    constructor() {
        super();

        const jwtPublicPemPath =
            path.join(__dirname, "../../ssl/jwt/public.pem");

        const verifyPublicKeySecret =
            fs.readFileSync(jwtPublicPemPath, "utf8");

        this.verifyPublicKeySecret = verifyPublicKeySecret;

        this.jsonWebToken = new JsonWebToken();
    }

    //#endregion

    //#region Methods

    //#region Init

    public init(): void {
        try {
            this.logManager.showLog({
                log: util.format("Json web token initialize start!")
            });

            this.jsonWebToken.init(
                this.verifyPublicKeySecret);

            this.logManager.showLog({
                log: util.format("Json web token initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            throw new Error(
                util.format("Json web token initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Json Web Token Methods

    //#region Verify Methods

    public async verify(
        payload: any,
        options?: jwt.VerifyOptions | undefined)
        : Promise<JsonWebTokenResponseInterface> {
        return await this.jsonWebToken.verify(
            payload, options);
    }

    //#endregion

    //#endregion

    //#endregion
}
