/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum ServerReturnCodes {
    // Game
    NotSent = 0,
    InvalidResponse = 1,

    MissingParameter = 10,

    UserNotExist = 20,
    UserAlreadyExist = 21,

    LinkedUserSameWithCurrent = 25,

    InvalidEmail = 30,
    InvalidPassword = 31,

    InsufficientCoin = 40,
    CollectCoinLimitReached = 41,
    AlreadyCollectedCoinFromCrystalRush = 45,

    PetNameAlreadyExist = 60,

    LeaderboardUserScoreHigherThanPrevious = 130,
    LeaderboardNotEnoughUsers = 131,

    PaymentMissingUserId = 150,
    GoogleMissingIABReceipt = 151,
    AppleMissingIAPReceipt = 152,
    MissingProductType = 153,
    IAPDataUnavailable = 154,
    IAPVerifyFailed = 155,
    InvalidReceiptData = 156,
    InvalidStoreProduct = 157,
    MissingProductList = 158,
    InvalidProductList = 159,
    InvalidPurchaseId = 160,
    InvalidStoreType = 161,
    AlreadyPurchased = 162,

    InvalidHash = 180,

    // HTTP Codes
    OK = 200,

    BadRequest = 400,
    Unauthorized = 401,
    Forbidden = 403,
    RequestTimeout = 408,

    TooManyRequest = 429,

    InternalError = 500
}
