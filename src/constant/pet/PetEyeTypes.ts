/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum PetEyeTypes {
    Eye1 = 1,
    Eye2 = 2,
    Eye3 = 3,
    Eye4 = 4,
    Eye5 = 5,
    Eye6 = 6,
    Eye7 = 7,
    Eye8 = 8,
    Eye9 = 9,
    Eye10 = 10
}
