/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum PetFootTypes {
    Foot1 = 1,
    Foot2 = 2,
    Foot3 = 3,
    Foot4 = 4,
    Foot5 = 5,
    Foot6 = 6,
    Foot7 = 7,
    Foot8 = 8,
    Foot9 = 9,
    Foot10 = 10
}
