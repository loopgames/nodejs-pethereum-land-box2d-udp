/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum PetRarityTitles {
    Common = 1,
    Uncommon = 2,
    Rare = 3,
    Epic = 4,
    Legendary = 5
}
