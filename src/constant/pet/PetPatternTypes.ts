/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum PetPatternTypes {
    None = 0,
    Pattern1 = 1,
    Pattern2 = 2,
    Pattern3 = 3,
    Pattern4 = 4
}
