/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum PetEarTypes {
    None = 0,
    Ear1 = 1,
    Ear2 = 2,
    Ear3 = 3,
    Ear4 = 4,
    Ear5 = 5,
    Ear6 = 6,
    Ear7 = 7,
    Ear8 = 8,
    Ear9 = 9
}
