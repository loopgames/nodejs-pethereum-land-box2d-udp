/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum PetMouthTypes {
    Mouth1 = 1,
    Mouth2 = 2,
    Mouth3 = 3,
    Mouth4 = 4,
    Mouth5 = 5,
    Mouth6 = 6,
    Mouth7 = 7,
    Mouth8 = 8,
    Mouth9 = 9,
    Mouth10 = 10
}
