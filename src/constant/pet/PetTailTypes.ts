/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum PetTailTypes {
    Tail1 = 1,
    Tail2 = 2,
    Tail3 = 3,
    Tail4 = 4,
    Tail5 = 5,
    Tail6 = 6,
    Tail7 = 7,
    Tail8 = 8,
    Tail9 = 9,
    Tail10 = 10
}
