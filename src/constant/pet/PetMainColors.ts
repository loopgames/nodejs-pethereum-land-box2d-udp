/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum PetMainColors {
    Color1 = 1,
    Color2 = 2,
    Color3 = 3,
    Color4 = 4,
    Color5 = 5,
    Color6 = 6,
    Color7 = 7,
    Color8 = 8,
    Color9 = 9,
    Color10 = 10
}
