/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum PetHairTypes {
    Hair1 = 1,
    Hair2 = 2,
    Hair3 = 3,
    Hair4 = 4,
    Hair5 = 5,
    Hair6 = 6,
    Hair7 = 7,
    Hair8 = 8,
    Hair9 = 9,
    Hair10 = 10
}
