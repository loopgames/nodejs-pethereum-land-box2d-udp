/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum PetBodyShapes {
    OvalBody = 1,
    FringedBody = 2,
    SoftBody = 3,
    ZigZagBody = 4,
    FluffyBody = 5
}
