/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export abstract class PetConstants {
    public static readonly PET_DEFAULT_NAME_PREFIX: string =
        "Pet%s";

    public static readonly PET_RARITY_COMMON_CHANCE: number =
        100;
    public static readonly PET_RARITY_UNCOMMON_CHANCE: number =
        54.7;
    public static readonly PET_RARITY_RARE_CHANCE: number =
        28.7;
    public static readonly PET_RARITY_EPIC_CHANCE: number =
        13.7;
    public static readonly PET_RARITY_LEGENDARY_CHANCE: number =
        5;

    public static readonly PET_STAR_COMMON_LEVEL: number =
        1;
    public static readonly PET_STAR_UNCOMMON_LEVEL: number =
        2;
    public static readonly PET_STAR_RARE_LEVEL: number =
        3;
    public static readonly PET_STAR_EPIC_LEVEL: number =
        4;
    public static readonly PET_STAR_LEGENDARY_LEVEL: number =
        5;

    public static readonly PET_STAR_CALCULATION_MULTIPLIER: number =
        8;
}
