/**
 * Created by hulusionder on 24/10/2021.
 */

"use strict";

export abstract class RedisLockConstants {
    public static readonly REDIS_LOCK_LOBBY_CLIENT_PREFIX: string =
        "lock:client:%s";

    public static readonly REDIS_LOCK_LOBBY_CLIENT_TIME: number =
        2000;
}
