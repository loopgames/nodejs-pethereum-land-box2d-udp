/**
 * Created by hulusionder on 09/02/2022
 */

"use strict";

export abstract class SocketConstants {
    public static readonly SOCKET_PREDEFINED_EVENTS_UPGRADE: string =
        "upgrade";

    public static readonly SOCKET_PREDEFINED_EVENTS_LISTENING: string =
        "listening";
    public static readonly SOCKET_PREDEFINED_EVENTS_CONNECTION: string =
        "connection";
    public static readonly SOCKET_PREDEFINED_EVENTS_CLOSE: string =
        "close";

    public static readonly SOCKET_PREDEFINED_EVENTS_DISCONNECT: string =
        "disconnect";
    public static readonly SOCKET_PREDEFINED_EVENTS_MESSAGE: string =
        "message";

    public static readonly WS_REMOVE_INACTIVE_CHAT_USERS_INTERVAL_IN_MILLISECONDS: number =
        1000; // 1 seconds
}
