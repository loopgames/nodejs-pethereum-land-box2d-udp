/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum SerializationMode {
    Read = 0,
    Write = 1
}
