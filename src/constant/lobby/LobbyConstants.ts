/**
 * Created by hulusionder on 24/10/2021.
 */

"use strict";

//#region Imports

//#endregion

export abstract class LobbyConstants {
    public static readonly LOBBY_CLIENT_REMOVE_INACTIVE_USER_IN_LOBBY_SECONDS: number =
        10;
    public static readonly LOBBY_CLIENT_REMOVE_INACTIVE_USER_IN_MINI_GAME_SECONDS: number =
        180;

    public static readonly LOBBY_CLIENTS_STATE_UPDATE_INTERVAL_MILLISECONDS: number =
        1000 * 15;
}
