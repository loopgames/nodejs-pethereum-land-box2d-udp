/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum ServerLobbyMessageTypes {
    Welcome = 100,
    JoinSelf = 110,
    JoinOther = 120,
    LeaveSelf = 130,
    LeaveOther = 140,
    Reconnect = 150,
    ChangePetSelf = 160,
    ChangePetOther = 170,
    UpdateClientState = 180,
    UpdateWorldState = 200,
    StartCrystalRush = 250,
    FinishCrystalRush = 260,
    UpdateCrystalRush = 270
}
