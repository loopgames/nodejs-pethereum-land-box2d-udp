/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum LobbyClientLocations {
    InLobby = 10,
    InMiniGame = 20
}
