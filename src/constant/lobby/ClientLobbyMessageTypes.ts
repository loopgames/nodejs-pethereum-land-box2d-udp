/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum ClientLobbyMessageTypes {
    Ping = 0,
    Join = 10,
    Leave = 20,
    ChangePet = 30,
    EnterMiniGame = 40,
    Move = 50
}
