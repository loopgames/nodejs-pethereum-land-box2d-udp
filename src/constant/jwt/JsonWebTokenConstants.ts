/**
 * Created by hulusionder on 24/10/2021.
 */

"use strict";

//#region Imports

import jwt from "jsonwebtoken";

//#endregion

export abstract class JsonWebTokenConstants {
    public static readonly JWT_ALGORITHM: jwt.Algorithm =
        "RS256";
}
