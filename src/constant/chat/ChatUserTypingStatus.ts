/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum ChatUserTypingStatus {
    NotTyping = 10,
    Typing = 20
}
