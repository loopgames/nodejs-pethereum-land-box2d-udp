/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum ServerChatMessageTypes {
    Welcome = 100,
    JoinSelf = 110,
    JoinOther = 120,
    LeaveSelf = 130,
    LeaveOther = 140,
    Reconnect = 150,
    UpdateChat = 200
}
