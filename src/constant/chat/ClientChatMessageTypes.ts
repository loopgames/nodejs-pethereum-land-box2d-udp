/**
 * Created by hulusionder on 27/10/2021.
 */

"use strict";

export enum ClientChatMessageTypes {
    Join = 10,
    Leave = 20,
    SendMessage = 50
}
