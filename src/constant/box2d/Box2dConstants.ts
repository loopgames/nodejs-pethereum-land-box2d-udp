/**
 * Created by hulusionder on 24/10/2021.
 */

"use strict";

//#region Imports

import {b2Vec2} from "@box2d/core";

import {Box2dBodyData} from "../../model/box2d/Box2dBodyData";
import {Box2dBodyDefinitionData} from "../../model/box2d/Box2dBodyDefinitionData";
import {Box2dFixtureDefinitionData} from "../../model/box2d/Box2dFixtureDefinitionData";
import {Box2dShapeData} from "../../model/box2d/Box2dShapeData";

//#endregion

export abstract class Box2dConstants {
    public static readonly BOX2D_UPDATE_RATE: number =
        30;
    public static readonly BOX2D_UPDATE_MILLISECONDS: number =
        (1 / 30) * 1000;
    public static readonly BOX2D_VELOCITY_ITERATIONS: number =
        8;
    public static readonly BOX2D_POSITION_ITERATIONS: number =
        3;

    public static readonly BOX2D_PET_MOVEMENT_SPEED: number =
        0.8;
    public static readonly BOX2D_PET_MOVEMENT_FIX_SPEED_MULTIPLIER: number =
        0.25;

    public static readonly BOX2D_PET_POSITION_FIX_MIN_RANGE: number =
        0.06;
    public static readonly BOX2D_PET_POSITION_FIX_MAX_RANGE: number =
        0.8;

    public static readonly BOX2D_PET_MIN_STAR: number =
        1;
    public static readonly BOX2D_PET_MAX_STAR: number =
        5;

    public static readonly BOX2D_PET_INITIAL_SCALE: number =
        0.15;
    public static readonly BOX2D_PET_MIN_SCALE: number =
        0.075;
    public static readonly BOX2D_PET_MAX_SCALE: number =
        0.09;

    public static readonly BOX2D_PET_BODY_DATA: Box2dBodyData =
        new Box2dBodyData(
            new Box2dBodyDefinitionData(
                true,
                true,
                true,
                0,
                0.05,
                0,
                2,
                0,
                {x: 0, y: 0}),
            [
                new Box2dFixtureDefinitionData(
                    false,
                    2,
                    1,
                    1,
                    0,
                    0,
                    new Box2dShapeData(
                        0,
                        0.15,
                        {x: 0, y: 0},
                        []))
            ]);
}
