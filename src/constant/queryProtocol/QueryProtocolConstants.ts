/**
 * Created by hulusionder on 09/02/2022
 */

"use strict";

export abstract class QueryProtocolConstants {
    public static readonly SOCKET_PREDEFINED_EVENTS_CLOSE: string =
        "close";
    public static readonly SOCKET_PREDEFINED_EVENTS_CONNECT: string =
        "connect";
    public static readonly SOCKET_PREDEFINED_EVENTS_ERROR: string =
        "error";
    public static readonly SOCKET_PREDEFINED_EVENTS_LISTENING: string =
        "listening";
    public static readonly SOCKET_PREDEFINED_EVENTS_MESSAGE: string =
        "message";

    public static readonly QUERY_PROTOCOL_SERVER_EVENTS_CLOSE: string =
        "query_protocol_server_close";
    public static readonly QUERY_PROTOCOL_SERVER_EVENTS_CONNECT: string =
        "query_protocol_server_connect";
    public static readonly QUERY_PROTOCOL_SERVER_EVENTS_ERROR: string =
        "query_protocol_server_error";
    public static readonly QUERY_PROTOCOL_SERVER_EVENTS_LISTENING: string =
        "query_protocol_server_listening";
    public static readonly QUERY_PROTOCOL_SERVER_EVENTS_MESSAGE: string =
        "query_protocol_server_message";

    public static readonly QUERY_PROTOCOL_SERVER_UPDATE_MILLISECONDS: number =
        2000;
}
