/**
 * Created by hulusionder on 09/02/2022
 */

"use strict";

export abstract class UdpConstants {
    public static readonly SOCKET_PREDEFINED_EVENTS_CLOSE: string =
        "close";
    public static readonly SOCKET_PREDEFINED_EVENTS_CONNECT: string =
        "connect";
    public static readonly SOCKET_PREDEFINED_EVENTS_ERROR: string =
        "error";
    public static readonly SOCKET_PREDEFINED_EVENTS_LISTENING: string =
        "listening";
    public static readonly SOCKET_PREDEFINED_EVENTS_MESSAGE: string =
        "message";

    public static readonly UDP_SERVER_EVENTS_CLOSE: string =
        "udp_server_close";
    public static readonly UDP_SERVER_EVENTS_CONNECT: string =
        "udp_server_connect";
    public static readonly UDP_SERVER_EVENTS_ERROR: string =
        "udp_server_error";
    public static readonly UDP_SERVER_EVENTS_LISTENING: string =
        "udp_server_listening";
    public static readonly UDP_SERVER_EVENTS_MESSAGE: string =
        "udp_server_message";
}
