/**
 * Created by hulusionder on 09/02/2022
 */

"use strict";

export abstract class TcpConstants {
    public static readonly SOCKET_PREDEFINED_EVENTS_CLOSE: string =
        "close";
    public static readonly SOCKET_PREDEFINED_EVENTS_CONNECTION: string =
        "connection";
    public static readonly SOCKET_PREDEFINED_EVENTS_ERROR: string =
        "error";
    public static readonly SOCKET_PREDEFINED_EVENTS_LISTENING: string =
        "listening";
    public static readonly SOCKET_PREDEFINED_EVENTS_MESSAGE: string =
        "message";
}
