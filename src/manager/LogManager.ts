/**
 * Created by hulusionder on 01/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import {ReferenceController} from "../controller/ReferenceController";

//#endregion

export class LogManager {
    //#region Variables

    private readonly base!: string;
    private readonly isDebuggingEnable!: boolean;

    //#endregion

    //#region Constructor

    constructor(base: string,
                isDebuggingEnable: boolean = false) {
        this.base = base;
        this.isDebuggingEnable = isDebuggingEnable;
    }

    //#endregion

    //#region Methods

    public showLog(
        {log, forceToDebug = false}:
            { log: string; forceToDebug?: boolean }): void {
        if (!this.isDebuggingEnable && !forceToDebug) return;

        let logFormat = this.getLog(this.base, log);

        console.log(logFormat);

        ReferenceController.winstonLogController.writeLog(
            logFormat);
    }

    public showWarn(
        {warning, forceToDebug = false}:
            { warning: string; forceToDebug?: boolean }): void {
        if (!this.isDebuggingEnable && !forceToDebug) return;

        let warnFormat = this.getLog(this.base, warning);

        console.warn(warnFormat);

        ReferenceController.winstonLogController.writeWarn(
            warnFormat);
    }

    public showError(
        {error, forceToDebug = true}:
            { error: string; forceToDebug?: boolean }): void {
        if (!this.isDebuggingEnable && !forceToDebug) return;

        let errorFormat = this.getLog(this.base, error);

        console.error(errorFormat);

        ReferenceController.winstonLogController.writeError(
            errorFormat);
    }

    private getLog(base: string, log: string): string {
        let logFormat;
        if (base) {
            logFormat = util.format("[%s] -> %s", base, log);
        } else {
            logFormat = util.format("%s", log);
        }

        return logFormat;
    }

    //#endregion

    //#region Util Methods

    public toString(): string {
        return util.format("Base -> %s", this.base);
    }

    //#endregion
}
