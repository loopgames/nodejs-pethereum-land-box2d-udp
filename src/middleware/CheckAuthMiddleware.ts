/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {ExpressMiddlewareInterface} from "../interface/express/ExpressMiddlewareInterface";
import {ReferenceController} from "../controller/ReferenceController";
import {AuthResponse} from "../model/response/AuthResponse";
import {ServerReturnCodes} from "../constant/server/ServerReturnCodes";

//#endregion

export class CheckAuthMiddleware
    implements ExpressMiddlewareInterface {
    //#region Variables

    //#endregion

    //#region Methods

    public async use(request: any, response: any, next: (err?: any) => any): Promise<any> {
        const authorizationHeader =
            request.headers["authorization"];

        if (!authorizationHeader ||
            !authorizationHeader.startsWith("Bearer ")) {
            this.sendAuthFailResponse(
                response, 401, ServerReturnCodes.Unauthorized, "Missing Bearer");

            return;
        }

        const token =
            authorizationHeader.slice(7);

        if (!token) {
            this.sendAuthFailResponse(
                response, 401, ServerReturnCodes.Unauthorized, "Invalid Bearer");

            return;
        }

        const verifyTokenResponse =
            await ReferenceController.authController.verifyToken(
                token);

        if (!verifyTokenResponse.isSuccess) {
            this.sendAuthFailResponse(
                response, 403, ServerReturnCodes.Forbidden, "Invalid Token");

            return;
        }

        const payload =
            verifyTokenResponse.payload;

        if (!payload) {
            this.sendAuthFailResponse(
                response, 403, ServerReturnCodes.Forbidden, "Missing Payload");

            return;
        }

        const user = payload.user;

        if (!user) {
            this.sendAuthFailResponse(
                response, 403, ServerReturnCodes.Forbidden, "Missing User");

            return;
        }

        const oldUser = request.user;
        const newUser = (class extends null {
        }).prototype;
        Object.assign(newUser, user, oldUser);

        request.user = newUser;

        next();
    }

    private sendAuthFailResponse(
        response: any,
        statusCode: number,
        returnCode: ServerReturnCodes,
        message: string): void {
        const errorResponse = new AuthResponse(
            false, returnCode, message);

        response.data = errorResponse.serialize();

        response
            .status(statusCode)
            .send(response.data);
    }

    //#endregion
}
