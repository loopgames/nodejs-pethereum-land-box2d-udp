/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {ExpressMiddlewareInterface} from "../interface/express/ExpressMiddlewareInterface";
import {CryptoHelper} from "../helper/crypto/CryptoHelper";

//#endregion

//#region Variables

//#region Non-Lazy Initialization

//#endregion

//#endregion

export class DecryptRequestBodyMiddleware
    implements ExpressMiddlewareInterface {
    //#region Variables

    //#endregion

    //#region Methods

    public use(request: any, response: any, next: (err?: any) => any): any {
        if (request.body) {
            const encryptedRequestJson = request.body["encryptedRequestJson"];
            if (!encryptedRequestJson) {
                return response
                    .status(400)
                    .send("Unencrypted");
            }

            const decryptedBody = CryptoHelper.instance.decrypt(encryptedRequestJson);
            const parsedData = JSON.parse(decryptedBody);

            const newBody = (class extends null {
            }).prototype;
            Object.assign(newBody, parsedData);

            request.body = newBody;
            request.body.base64Data = decryptedBody;
        }

        next();
    }

    //#endregion
}
