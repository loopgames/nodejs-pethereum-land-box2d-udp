/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {ExpressMiddlewareInterface} from "../interface/express/ExpressMiddlewareInterface";

//#endregion

export class SendResponseMiddleware
    implements ExpressMiddlewareInterface {
    //#region Variables

    //#endregion

    //#region Methods


    public use(request: any, response: any, next: (err?: any) => any): any {
        response
            .status(200)
            .send(response.data);
    }

    //#endregion
}
