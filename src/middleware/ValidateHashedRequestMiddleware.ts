/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {ExpressMiddlewareInterface} from "../interface/express/ExpressMiddlewareInterface";
import {ServerHashHelper} from "../helper/hash/ServerHashHelper";

//#endregion

export class ValidateHashedRequestMiddleware
    implements ExpressMiddlewareInterface {
    //#region Variables

    //#endregion

    //#region Methods

    public use(request: any, response: any, next: (err?: any) => any): any {
        ServerHashHelper.instance.validateHashedRequest(request, response, next);
    }

    //#endregion
}
