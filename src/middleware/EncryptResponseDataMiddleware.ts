/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {ExpressMiddlewareInterface} from "../interface/express/ExpressMiddlewareInterface";
import {CryptoHelper} from "../helper/crypto/CryptoHelper";

//#endregion

//#region Variables

//#region Non-Lazy Initialization

//#endregion

//#endregion

export class EncryptResponseDataMiddleware
    implements ExpressMiddlewareInterface {
    //#region Variables

    //#endregion

    //#region Methods

    public use(request: any, response: any, next: (err?: any) => any): any {
        if (response.data) {
            const decryptedData = response.data;
            const stringifyData = JSON.stringify(decryptedData);
            const encryptedData = CryptoHelper.instance.encrypt(stringifyData);

            response.data = {
                encryptedResponseJson: encryptedData
            };
        }

        next();
    }

    //#endregion
}
