/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {ExpressMiddlewareInterface} from "../interface/express/ExpressMiddlewareInterface";
import {ServerHashHelper} from "../helper/hash/ServerHashHelper";

//#endregion

export class CreateHashedResponseMiddleware
    implements ExpressMiddlewareInterface {
    //#region Variables

    //#endregion

    //#region Methods

    public use(request: any, response: any, next: (err?: any) => any): any {
        ServerHashHelper.instance.createHashedResponse(request, response, next);
    }

    //#endregion
}
