/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

export interface ExpressMiddlewareInterface {
    //#region Methods

    use(request: any, response: any, next: (err?: any) => any): any;

    //#endregion
}
