/**
 * Created by hulusionder on 08/10/2021.
 */

"use strict";

export interface AxiosResponseInterface {
    //#region Variables

    isSuccess: boolean;
    error?: string;
    status?: number;
    data?: any;

    //#endregion
}
