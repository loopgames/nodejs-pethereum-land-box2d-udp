/**
 * Created by hulusionder on 08/10/2021.
 */

"use strict";

export interface RedisDatabaseResponseInterface {
    //#region Variables

    isSuccess: boolean;
    error?: string;
    result?: any;

    //#endregion
}
