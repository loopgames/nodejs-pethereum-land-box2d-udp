/**
 * Created by hulusionder on 08/10/2021.
 */

"use strict";

//#region Imports

import {Lock} from "redlock";

//#endregion

export interface RedisLockResponseInterface {
    //#region Variables

    isSuccess: boolean;
    error?: string;
    lock?: Lock;

    //#endregion
}
