/**
 * Created by hulusionder on 08/10/2021.
 */

"use strict";

//#region Imports

import {RateLimiterRes} from "rate-limiter-flexible";

//#endregion

export interface RedisRateLimiterResponseInterface {
    //#region Variables

    isSuccess: boolean;
    error?: string;
    result?: RateLimiterRes;

    //#endregion
}
