/**
 * Created by hulusionder on 02/16/2022
 */

"use strict";

export interface AuthVerifyResponseInterface {
    //#region Variables

    isSuccess: boolean;
    error?: string;
    payload?: any;

    //#endregion
}
