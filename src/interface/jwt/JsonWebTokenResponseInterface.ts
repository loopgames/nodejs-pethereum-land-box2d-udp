/**
 * Created by hulusionder on 02/16/2022
 */

"use strict";

export interface JsonWebTokenResponseInterface {
    //#region Variables

    isSuccess: boolean;
    error?: string;
    result?: any;

    //#endregion
}
