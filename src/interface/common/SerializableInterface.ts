/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

export interface SerializableInterface {
    //#region Methods

    serialize<T>(removeUndefined?: boolean): T;

    //#endregion
}
