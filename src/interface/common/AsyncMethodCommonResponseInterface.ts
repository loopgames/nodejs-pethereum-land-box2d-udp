/**
 * Created by hulusionder on 08/10/2021.
 */

"use strict";

export interface AsyncMethodCommonResponseInterface {
    //#region Variables

    isSuccess: boolean;
    error?: string;
    result?: any;

    //#endregion
}
