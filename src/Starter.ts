/**
 * Created by hulusionder on 29/09/2021.
 */

"use strict";

//#region Imports

import util from "util";
import path from "path";
import dotenv from "dotenv"

import {LogManager} from "./manager/LogManager";
import {CommonLogicUtil} from "./util/CommonLogicUtil";
import {MainServer} from "./model/server/MainServer";
import {SocketServer} from "./model/server/SocketServer";
import {UdpServer} from "./model/server/UdpServer";
import {QueryProtocolServer} from "./model/server/QueryProtocolServer";
import {ReferenceController} from "./controller/ReferenceController";

//#endregion

//#region Variables

//#region Non-Lazy Initialization

const logManager =
    new LogManager("Starter", true);

//#endregion

//#region Lazy Initialization

//#endregion

//#endregion

//#region Art

console.log("");
console.log("########  ######## ######## ##     ## ######## ########  ######## ##     ## ##     ##    ##          ###    ##    ## ########     ########   #######  ##     ##  #######  ########     ##     ## ########  ######## ");
console.log("##     ## ##          ##    ##     ## ##       ##     ## ##       ##     ## ###   ###    ##         ## ##   ###   ## ##     ##    ##     ## ##     ##  ##   ##  ##     ## ##     ##    ##     ## ##     ## ##     ##");
console.log("##     ## ##          ##    ##     ## ##       ##     ## ##       ##     ## #### ####    ##        ##   ##  ####  ## ##     ##    ##     ## ##     ##   ## ##          ## ##     ##    ##     ## ##     ## ##     ##");
console.log("########  ######      ##    ######### ######   ########  ######   ##     ## ## ### ##    ##       ##     ## ## ## ## ##     ##    ########  ##     ##    ###     #######  ##     ##    ##     ## ##     ## ######## ");
console.log("##        ##          ##    ##     ## ##       ##   ##   ##       ##     ## ##     ##    ##       ######### ##  #### ##     ##    ##     ## ##     ##   ## ##   ##        ##     ##    ##     ## ##     ## ##       ");
console.log("##        ##          ##    ##     ## ##       ##    ##  ##       ##     ## ##     ##    ##       ##     ## ##   ### ##     ##    ##     ## ##     ##  ##   ##  ##        ##     ##    ##     ## ##     ## ##       ");
console.log("##        ########    ##    ##     ## ######## ##     ## ########  #######  ##     ##    ######## ##     ## ##    ## ########     ########   #######  ##     ## ######### ########      #######  ########  ##       ");
console.log("");

//#endregion

//#region Initialize Environment Variables

const dotEnvConfigPath =
    path.join(__dirname, "../.env");

dotenv.config({path: dotEnvConfigPath});

//#endregion

//#region Initialize References

ReferenceController.init();

//#endregion

//#region Initialize Server

(async () => {
    try {
        ReferenceController.serverController.init();

        ReferenceController.winstonLogController.init();

        ReferenceController.jsonWebTokenController.init();

        // await ReferenceController.serverJsonController.init();
        await ReferenceController.queryProtocolController.init();

        await ReferenceController.chatController.init();

        await ReferenceController.box2dWorldController.init();
        await ReferenceController.lobbyController.init();

        const mainServer = new MainServer();
        await mainServer.init();

        const socketServer = new SocketServer();
        await socketServer.init(
            mainServer.httpServer);

        const udpServer = new UdpServer();
        await udpServer.init();

        const queryProtocolServer = new QueryProtocolServer();
        await queryProtocolServer.init();

        logManager.showLog({
            log: util.format("Server initialize success!")
        });
    } catch (exception) {
        const error =
            CommonLogicUtil.createErrorMessageFromException(exception);

        logManager.showError({
            error: util.format("Server initialize error! Err: %s",
                error)
        });
    }
})();

//#endregion
