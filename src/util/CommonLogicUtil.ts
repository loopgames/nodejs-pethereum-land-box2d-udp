/**
 * Created by hulusionder on 06/10/2021.
 */

"use strict";

//#region Imports

import moment from "moment";
import {Chance} from "chance";

//#endregion

export abstract class CommonLogicUtil {
    //#region Variables

    private static readonly ms_per_millisecond = 1;
    private static readonly ms_per_second = 1000;
    private static readonly ms_per_minute = 1000 * 60;
    private static readonly ms_per_hour = 1000 * 60 * 60;
    private static readonly ms_per_half_day = 1000 * 60 * 60 * 12;
    private static readonly ms_per_day = 1000 * 60 * 60 * 24;

    //#endregion

    //#region Methods

    //#region Date Methods

    //#region Millisecond Methods

    public static findMillisecondDifferenceBetweenDatesAsInteger(
        date1: Date, date2: Date): number {
        // Discard the time and time-zone information.
        const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(),
            date1.getDate(), date1.getHours(), date1.getMinutes(),
            date1.getSeconds());
        const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(),
            date2.getDate(), date2.getHours(), date2.getMinutes(),
            date2.getSeconds());

        const dateDiff = Math.abs(utc2 - utc1);
        return Math.floor(dateDiff / this.ms_per_millisecond);
    }

    public static findMillisecondDifferenceBetweenMillisecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return Math.floor(timestampDiff / this.ms_per_millisecond);
    }

    public static findMillisecondDifferenceBetweenMillisecondTimestampsAsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return (timestampDiff / this.ms_per_millisecond);
    }

    public static findMillisecondDifferenceBetweenSecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return Math.floor(timestampDiff);
    }

    public static findMillisecondDifferenceBetweenSecondTimestampsAsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return (timestampDiff);
    }

    public static findTotalMillisecondsFromMillisecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor(timestamp / this.ms_per_millisecond);
    }

    public static findTotalMillisecondsFromMillisecondTimestampAsFloat(
        timestamp: number): number {
        return (timestamp / this.ms_per_millisecond);
    }

    public static findTotalMillisecondsFromSecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor(timestamp);
    }

    public static findTotalMillisecondsFromSecondTimestampAsFloat(
        timestamp: number): number {
        return (timestamp);
    }

    public static addMillisecondsToDate(
        date: Date, secondsToAdd: number): Date {
        return new Date(date.getTime() + (secondsToAdd * this.ms_per_millisecond));
    }

    public static addMillisecondsToMillisecondTimestamp(
        timestamp: number, millisecondsToAdd: number): number {
        return timestamp + (millisecondsToAdd * this.ms_per_millisecond);
    }

    public static addMillisecondsToSecondTimestamp(
        timestamp: number, millisecondsToAdd: number): number {
        return timestamp + (millisecondsToAdd);
    }

    //#endregion

    //#region Second Methods

    public static findSecondDifferenceBetweenDatesAsInteger(
        date1: Date, date2: Date): number {
        // Discard the time and time-zone information.
        const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(),
            date1.getDate(), date1.getHours(), date1.getMinutes(),
            date1.getSeconds());
        const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(),
            date2.getDate(), date2.getHours(), date2.getMinutes(),
            date2.getSeconds());

        const dateDiff = Math.abs(utc2 - utc1);
        return Math.floor(dateDiff / this.ms_per_second);
    }

    public static findSecondDifferenceBetweenMillisecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return Math.floor(timestampDiff / this.ms_per_second);
    }

    public static findSecondDifferenceBetweenMillisecondTimestampsAsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return (timestampDiff / this.ms_per_second);
    }

    public static findSecondDifferenceBetweenSecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return Math.floor(timestampDiff);
    }

    public static findSecondDifferenceBetweenSecondTimestampsAsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return (timestampDiff);
    }

    public static findTotalSecondsFromMillisecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor(timestamp / this.ms_per_second);
    }

    public static findTotalSecondsFromMillisecondTimestampAsFloat(
        timestamp: number): number {
        return (timestamp / this.ms_per_second);
    }

    public static findTotalSecondsFromSecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor(timestamp);
    }

    public static findTotalSecondsFromSecondTimestampAsFloat(
        timestamp: number): number {
        return (timestamp);
    }

    public static addSecondsToDate(
        date: Date, secondsToAdd: number): Date {
        return new Date(date.getTime() + (secondsToAdd * this.ms_per_second));
    }

    public static addSecondsToMillisecondTimestamp(
        timestamp: number, secondsToAdd: number): number {
        return timestamp + (secondsToAdd * this.ms_per_second);
    }

    public static addSecondsToSecondTimestamp(
        timestamp: number, secondsToAdd: number): number {
        return timestamp + (secondsToAdd);
    }

    //#endregion

    //#region Minute Methods

    public static findMinuteDifferenceInDatesAsInteger(
        date1: Date, date2: Date): number {
        // Discard the time and time-zone information.
        const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(),
            date1.getDate(), date1.getHours(), date1.getMinutes());
        const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(),
            date2.getDate(), date2.getHours(), date2.getMinutes());

        const dateDiff = Math.abs(utc2 - utc1);
        return Math.floor(dateDiff / this.ms_per_minute);
    }

    public static findMinuteDifferenceBetweenMillisecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff: number = Math.abs(timestamp1 - timestamp2);
        return Math.floor(timestampDiff / this.ms_per_minute);
    }

    public static findMinuteDifferenceBetweenMillisecondTimestampsAsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff: number = Math.abs(timestamp1 - timestamp2);
        return (timestampDiff / this.ms_per_minute);
    }

    public static findMinuteDifferenceBetweenSecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff: number = Math.abs(timestamp1 - timestamp2);
        return Math.floor((timestampDiff * this.ms_per_second) / this.ms_per_minute);
    }

    public static findMinuteDifferenceBetweenSecondTimestampsAsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff: number = Math.abs(timestamp1 - timestamp2);
        return ((timestampDiff * this.ms_per_second) / this.ms_per_minute);
    }

    public static findTotalMinutesFromMillisecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor(timestamp / this.ms_per_minute);
    }

    public static findTotalMinutesFromMillisecondTimestampAsFloat(
        timestamp: number): number {
        return (timestamp / this.ms_per_minute);
    }

    public static findTotalMinutesFromSecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor((timestamp * this.ms_per_second) / this.ms_per_minute);
    }

    public static findTotalMinutesFromSecondTimestampAsFloat(
        timestamp: number): number {
        return ((timestamp * this.ms_per_second) / this.ms_per_minute);
    }

    public static addMinutesToDate(
        date: Date, minutesToAdd: number): Date {
        return new Date(date.getTime() + (minutesToAdd * this.ms_per_minute));
    }

    public static addMinutesToMillisecondTimestamp(
        timestamp: number, minutesToAdd: number): number {
        return timestamp + (minutesToAdd * this.ms_per_minute);
    }

    public static addMinutesToSecondTimestamp(
        timestamp: number, minutesToAdd: number): number {
        return timestamp + (minutesToAdd * (this.ms_per_minute / this.ms_per_second));
    }

    //#endregion

    //#region Hour Methods

    public static findHourDifferenceInDatesAsInteger(
        date1: Date, date2: Date): number {
        // Discard the time and time-zone information.
        const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(),
            date1.getDate(), date1.getHours());
        const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(),
            date2.getDate(), date2.getHours());

        const dateDiff = Math.abs(utc2 - utc1);
        return Math.floor(dateDiff / this.ms_per_hour);
    }

    public static findHourDifferenceBetweenMillisecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return Math.floor(timestampDiff / this.ms_per_hour);
    }

    public static findHourDifferenceBetweenMillisecondTimestampsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return (timestampDiff / this.ms_per_hour);
    }

    public static findHourDifferenceBetweenSecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return Math.floor((timestampDiff * this.ms_per_second) / this.ms_per_hour);
    }

    public static findHourDifferenceBetweenSecondTimestampsAsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return ((timestampDiff * this.ms_per_second) / this.ms_per_hour);
    }

    public static findTotalHoursFromMillisecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor(timestamp / this.ms_per_hour);
    }

    public static findTotalHoursFromMillisecondTimestampAsFloat(
        timestamp: number): number {
        return (timestamp / this.ms_per_hour);
    }

    public static findTotalHoursFromSecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor((timestamp * this.ms_per_second) / this.ms_per_hour);
    }

    public static findTotalHoursFromSecondTimestampAsFloat(
        timestamp: number): number {
        return ((timestamp * this.ms_per_second) / this.ms_per_hour);
    }

    public static addHoursToDate(
        date: Date, hoursToAdd: number): Date {
        return new Date(date.getTime() + (hoursToAdd * this.ms_per_hour));
    }

    public static addHoursToMillisecondTimestamp(
        timestamp: number, hoursToAdd: number): number {
        return timestamp + (hoursToAdd * this.ms_per_hour);
    }

    public static addHoursToSecondTimestamp(
        timestamp: number, hoursToAdd: number): number {
        return timestamp + (hoursToAdd * (this.ms_per_hour / this.ms_per_second));
    }

    //#endregion

    //#region Half Day Methods

    public static findHalfDayDifferenceInDatesAsInteger(
        date1: Date, date2: Date): number {
        // Discard the time and time-zone information.
        const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(),
            date1.getDate());
        const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(),
            date2.getDate());

        const dateDiff = Math.abs(utc2 - utc1);
        return Math.floor(dateDiff / this.ms_per_half_day);
    }

    public static findHalfDayDifferenceBetweenMillisecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return Math.floor(timestampDiff / this.ms_per_half_day);
    }

    public static findHalfDayDifferenceBetweenMillisecondTimestampsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return (timestampDiff / this.ms_per_half_day);
    }

    public static findHalfDayDifferenceBetweenSecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return Math.floor((timestampDiff * this.ms_per_second) / this.ms_per_half_day);
    }

    public static findHalfDayDifferenceBetweenSecondTimestampsAsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return ((timestampDiff * this.ms_per_second) / this.ms_per_half_day);
    }

    public static findTotalHalfDaysFromMillisecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor(timestamp / this.ms_per_half_day);
    }

    public static findTotalHalfDaysFromMillisecondTimestampAsFloat(
        timestamp: number): number {
        return (timestamp / this.ms_per_half_day);
    }

    public static findTotalHalfDaysFromSecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor((timestamp * this.ms_per_second) / this.ms_per_half_day);
    }

    public static findTotalHalfDaysFromSecondTimestampAsFloat(
        timestamp: number): number {
        return ((timestamp * this.ms_per_second) / this.ms_per_half_day);
    }

    public static findSecondTimestampAsIntegerFromTotalHalfDays(
        totalHalfDays: number): number {
        return Math.floor((totalHalfDays * this.ms_per_half_day) / this.ms_per_second);
    }

    public static addHalfDaysToDate(
        date: Date, daysToAdd: number): Date {
        return new Date(date.getTime() + (daysToAdd * this.ms_per_half_day));
    }

    public static addHalfDaysToMillisecondTimestamp(
        timestamp: number, daysToAdd: number): number {
        return timestamp + (daysToAdd * this.ms_per_half_day);
    }

    public static addHalfDaysToSecondTimestamp(
        timestamp: number, daysToAdd: number): number {
        return timestamp + (daysToAdd * (this.ms_per_half_day / this.ms_per_second));
    }

    //#endregion

    //#region Day Methods

    public static findDayDifferenceInDatesAsInteger(
        date1: Date, date2: Date): number {
        // Discard the time and time-zone information.
        const utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(),
            date1.getDate());
        const utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(),
            date2.getDate());

        const dateDiff = Math.abs(utc2 - utc1);
        return Math.floor(dateDiff / this.ms_per_day);
    }

    public static findDayDifferenceBetweenMillisecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return Math.floor(timestampDiff / this.ms_per_day);
    }

    public static findDayDifferenceBetweenMillisecondTimestampsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return (timestampDiff / this.ms_per_day);
    }

    public static findDayDifferenceBetweenSecondTimestampsAsInteger(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return Math.floor((timestampDiff * this.ms_per_second) / this.ms_per_day);
    }

    public static findDayDifferenceBetweenSecondTimestampsAsFloat(
        timestamp1: number, timestamp2: number): number {
        const timestampDiff = Math.abs(timestamp1 - timestamp2);
        return ((timestampDiff * this.ms_per_second) / this.ms_per_day);
    }

    public static findTotalDaysFromMillisecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor(timestamp / this.ms_per_day);
    }

    public static findTotalDaysFromMillisecondTimestampAsFloat(
        timestamp: number): number {
        return (timestamp / this.ms_per_day);
    }

    public static findTotalDaysFromSecondTimestampAsInteger(
        timestamp: number): number {
        return Math.floor((timestamp * this.ms_per_second) / this.ms_per_day);
    }

    public static findTotalDaysFromSecondTimestampAsFloat(
        timestamp: number): number {
        return ((timestamp * this.ms_per_second) / this.ms_per_day);
    }

    public static addDaysToDate(
        date: Date, daysToAdd: number): Date {
        return new Date(date.getTime() + (daysToAdd * this.ms_per_day));
    }

    public static addDaysToMillisecondTimestamp(
        timestamp: number, daysToAdd: number): number {
        return timestamp + (daysToAdd * this.ms_per_day);
    }

    public static addDaysToSecondTimestamp(
        timestamp: number, daysToAdd: number): number {
        return timestamp + (daysToAdd * (this.ms_per_day / this.ms_per_second));
    }

    //#endregion

    //#region Misc Methods

    public static getCurrentMillisecondTimestamp(): number {
        return Date.now();
    }

    public static getCurrentMillisecondTimestampForSpecificTimezone(
        timezone: string): number {
        if (this.isNullOrEmpty(timezone)) {
            return this.getCurrentMillisecondTimestamp();
        }

        const localDateString = new Date().toLocaleString("en-US", {timeZone: timezone});
        const localDate = new Date(localDateString);

        return localDate.getTime();
    }

    public static getCurrentSecondTimestamp(): number {
        return Math.floor(Date.now() / this.ms_per_second);
    }

    public static getCurrentSecondTimestampForSpecificTimezone(
        timezone: string): number {
        if (this.isNullOrEmpty(timezone)) {
            return this.getCurrentSecondTimestamp();
        }

        const localDateString = new Date().toLocaleString("en-US", {timeZone: timezone});
        const localDate = new Date(localDateString);

        return Math.floor(localDate.getTime() / this.ms_per_second);
    }

    public static convertMillisecondTimestampToSecondTimestamp(
        timestamp: number): number {
        return Math.floor(timestamp / this.ms_per_second);
    }

    public static convertSecondTimestampToMillisecondTimestamp(
        timestamp: number): number {
        return Math.floor(timestamp * this.ms_per_second);
    }

    //#endregion

    //#endregion

    //#region Moment Methods

    public static getIsoDayOfWeekWithMoment(): string {
        return moment()
            .utc()
            .format("d");
    }

    public static getIsoDayOfWeekWithMomentAsGivenPastDay(
        pastDay: number): string {
        return moment()
            .utc()
            .subtract(pastDay, "days")
            .format("d");
    }

    public static getIsoDayOfMonthWithMoment(): string {
        return moment()
            .utc()
            .format("DD");
    }

    public static getIsoDayOfMonthWithMomentAsGivenPastDay(
        pastDay: number): string {
        return moment()
            .utc()
            .subtract(pastDay, "days")
            .format("DD");
    }

    public static getIsoWeekOfYearWithMoment(): string {
        return moment()
            .utc()
            .format("WW");
    }

    public static getIsoWeekOfYearWithMomentAsGivenPastWeek(
        pastWeek: number): string {
        return moment()
            .utc()
            .subtract(pastWeek, "weeks")
            .format("WW");
    }

    public static getIsoMonthOfYearWithMoment(): string {
        return moment()
            .utc()
            .format("MM");
    }

    public static getIsoMonthOfYearWithMomentAsGivenPastWeek(
        pastMonth: number): string {
        return moment()
            .utc()
            .subtract(pastMonth, "months")
            .format("MM");
    }

    public static getIsoLongYearWithMoment(): string {
        return moment()
            .utc()
            .format("YYYY");
    }

    public static getIsoShortYearWithMoment(): string {
        return moment()
            .utc()
            .format("YY");
    }

    public static getIsoLongYearWithMomentAsGivenPastWeek(
        pastWeek: number): string {
        return moment()
            .utc()
            .subtract(pastWeek, "weeks")
            .format("YYYY");
    }

    public static getIsoShortYearWithMomentAsGivenPastWeek(
        pastWeek: number): string {
        return moment()
            .utc()
            .subtract(pastWeek, "weeks")
            .format("YY");
    }

    public static convertIsoDateStringToSecondTimestamp(
        isoDateString: string): number {
        return moment(isoDateString)
            .utc()
            .unix();
    }

    //#endregion

    //#region Random Methods

    public static getRandomNumber(
        min: number,
        max: number): number {
        const randomNumber =
            Math.random() * (max - min) + min;

        return randomNumber;
    }

    public static getRandomNumberNDigits(
        digits: number): number {
        const baseNumber =
            Math.pow(10, digits - 1);
        const randomNumberNDigits =
            Math.floor(baseNumber + Math.random() * (9 * baseNumber));

        return randomNumberNDigits;
    }

    public static getRandomElementFromArray<T>(
        arr: T[]): T {
        const randomElement =
            arr[Math.floor((Math.random() * arr.length))];

        return randomElement;
    }

    //#endregion

    //#region Chance Methods

    public static getRandomIntegerNumberWithChance(
        min: number,
        max: number): number {
        const chance = new Chance();
        const randomNumber =
            chance.integer({min, max});

        return randomNumber;
    }

    public static getRandomFloatNumberWithChance(
        min: number,
        max: number,
        fixed: number): number {
        const chance = new Chance();
        const randomNumber =
            chance.floating({min, max, fixed});

        return randomNumber;
    }

    public static getRandomIntegerNumberFromWordWithChance(
        word: string): number {
        const chance = new Chance(word);
        const randomNumber =
            chance.integer({min: 0, max: 100});

        return randomNumber;
    }

    //#endregion

    //#region Array Methods

    //#region Remove Methods

    public static removeElementFromArray<T>(
        arr: Array<T>,
        value: T): Array<T> {
        const index =
            arr.indexOf(value);

        if (index > -1) {
            arr.splice(index, 1);
        }

        return arr;
    }

    //#endregion

    //#region Sort Methods

    public static sortIntArrayInAscendingOrder(
        value1: number, value2: number): number {
        return value1 - value2;
    }

    public static sortIntArrayInDescendingOrder(
        value1: number, value2: number): number {
        return value2 - value1;
    }

    //#endregion

    //#endregion

    //#region Check Methods

    public static isUndefined(target: any): boolean {
        if (target === null) {
            return true;
        }

        if (target === undefined) {
            return true;
        }

        if (target === "undefined") {
            return true;
        }

        return false;
    }

    public static isUndefinedOrNaN(target: any): boolean {
        if (target === null) {
            return true;
        }

        if (target === undefined) {
            return true;
        }

        if (target === "undefined") {
            return true;
        }

        if (isNaN(parseInt(target))) {
            return true;
        }

        return false;
    }

    public static isNullOrEmpty(target: any): boolean {
        if (target === null) {
            return true;
        }

        if (target === undefined) {
            return true;
        }

        if (target === "undefined") {
            return true;
        }

        if (target === "") {
            return true;
        }

        return false;
    }

    //#endregion

    //#region Clone Methods

    public static cloneObject<T>(objectToClone: T): T {
        let clonedObject;

        // Handle the 3 simple types, and null or undefined
        if (objectToClone === null ||
            typeof objectToClone !== "object") {
            return objectToClone;
        }

        // Handle Date
        if (objectToClone instanceof Date) {
            clonedObject = new Date();

            clonedObject.setTime(objectToClone.getTime());

            return clonedObject as any;
        }

        // Handle Array
        if (objectToClone instanceof Array) {
            clonedObject = [];

            for (let i = 0, len = objectToClone.length; i < len; i++) {
                clonedObject[i] = this.cloneObject(objectToClone[i]);
            }

            return clonedObject as any;
        }

        // Handle Object
        if (objectToClone instanceof Object &&
            objectToClone !== {}) {
            clonedObject = {
                ...(objectToClone as { [key: string]: any })
            } as {
                [key: string]: any
            };

            for (let attr in objectToClone) {
                if (Object.prototype.hasOwnProperty.call(clonedObject, attr)) {
                    clonedObject[attr] = this.cloneObject(clonedObject[attr]);
                }
            }

            return clonedObject as T;
        }

        return null as any;
    }

    //#endregion

    //#region Char Methods

    public static convertCharToByte(
        convertChar: string): number {
        if (convertChar.length <= 0) {
            return 0;
        }

        const result =
            convertChar.charCodeAt(0);

        return result;
    }

    //#endregion

    //#region String Methods

    public static convertStringToByteArray(
        convertString: string): Uint8Array {
        const result =
            new Uint8Array(convertString.length);

        for (let i = 0; i < convertString.length; i++) {
            result[i] = convertString.charCodeAt(i);
        }

        return result;
    }

    //#endregion

    //#region Integer Methods

    public static convertInt32ToByteArray(
        convertInt32: number): ArrayBuffer {
        const arr =
            new ArrayBuffer(4);
        const view =
            new DataView(arr);

        view.setUint32(0, convertInt32, true);

        return arr;
    }

    //#endregion

    //#region Float Methods

    public static precisionRoundFloat(
        floatToPrecise: number,
        precision: number): number {
        const factor = Math.pow(10, precision);
        return Math.round(floatToPrecise * factor) / factor;
    }

    //#endregion

    //#region Misc Methods

    public static createErrorMessageFromException(
        exception: any): string {
        let errorDesc;

        if (exception instanceof Error) {
            errorDesc = exception.message;
        } else {
            errorDesc = "Unknown";
        }

        return errorDesc;
    }

    //#endregion

    //#endregion
}
