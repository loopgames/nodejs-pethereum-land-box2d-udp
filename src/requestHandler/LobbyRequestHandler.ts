/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import express from "express";

import {LogManager} from "../manager/LogManager";
import {CommonLogicUtil} from "../util/CommonLogicUtil";
import {ServerReturnCodes} from "../constant/server/ServerReturnCodes";
import {LobbyResponse} from "../model/response/LobbyResponse";
import {ReferenceController} from "../controller/ReferenceController";
import {Pet} from "../model/pet/Pet";
import {LobbyClientPosition} from "../model/lobby/LobbyClientPosition";

//#endregion

export class LobbyRequestHandler {
    //#region Variables

    private _expressRouter!: express.Router;

    private readonly logManager!: LogManager;

    //#endregion

    //#region Getters - Setters

    get expressRouter(): express.Router {
        return this._expressRouter;
    }

    //#endregion

    //#region Constructor

    constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);
    }

    //#endregion

    //#region Methods

    //#region Init

    public init(): void {
        this.logManager.showLog({
            log: util.format("Lobby request handler initialize start!")
        });

        this._expressRouter = express.Router();

        this.prepareRouters();

        this.logManager.showLog({
            log: util.format("Lobby request handler initialize success!")
        });
    }

    //#endregion

    //#region Prepare Routers

    prepareRouters(): void {
        this._expressRouter.post("/joinLobby",
            (...args) => this.joinLobby(...args));
        this._expressRouter.post("/leaveLobby",
            (...args) => this.leaveLobby(...args));

        this._expressRouter.post("/changePet",
            (...args) => this.changePet(...args));

        this._expressRouter.post("/enterMiniGame",
            (...args) => this.enterMiniGame(...args));
    }

    //#endregion

    //#region Route Handles

    private async joinLobby(request: any, response: any, next: any): Promise<void> {
        // this.logManager.showLog({
        //     log: util.format("***** JOIN LOBBY *****")
        // });

        let lobbyClientId = request.user["thirdPartyId"];
        if (CommonLogicUtil.isNullOrEmpty(lobbyClientId)) {
            const errorResponse = new LobbyResponse(
                false, ServerReturnCodes.MissingParameter);

            response.data = errorResponse.serialize();
            next();

            return;
        }

        let pet = request.body["pet"];
        if (CommonLogicUtil.isUndefined(pet)) {
            const errorResponse = new LobbyResponse(
                false, ServerReturnCodes.MissingParameter);

            response.data = errorResponse.serialize();
            next();

            return;
        }

        let lobbyClientPosition = request.body["lobbyClientPosition"];
        if (CommonLogicUtil.isUndefined(lobbyClientPosition)) {
            const errorResponse = new LobbyResponse(
                false, ServerReturnCodes.MissingParameter);

            response.data = errorResponse.serialize();
            next();

            return;
        }

        const joinLobbyResponse =
            await ReferenceController.lobbyController.joinLobby(
                lobbyClientId,
                Pet.toClass(pet),
                LobbyClientPosition.toClass(lobbyClientPosition));

        const joinLobbyResult =
            joinLobbyResponse.responseData ?? {};

        if (!joinLobbyResult.isSuccess) {
            const clientResponse = new LobbyResponse(
                false, joinLobbyResponse.returnCode);

            const responseData = Object.assign(
                clientResponse.serialize(),
                joinLobbyResult);

            response.data = responseData;
            next();

            return;
        }

        const clientResponse = new LobbyResponse(
            true, ServerReturnCodes.OK);

        const responseData = Object.assign(
            clientResponse.serialize(),
            joinLobbyResult);

        response.data = responseData;
        next();
    }

    private async leaveLobby(request: any, response: any, next: any): Promise<void> {
        // this.logManager.showLog({
        //     log: util.format("***** LEAVE LOBBY *****")
        // });

        let lobbyClientId = request.user["thirdPartyId"];
        if (CommonLogicUtil.isNullOrEmpty(lobbyClientId)) {
            const errorResponse = new LobbyResponse(
                false, ServerReturnCodes.MissingParameter);

            response.data = errorResponse.serialize();
            next();

            return;
        }

        const leaveLobbyResponse =
            await ReferenceController.lobbyController.leaveLobby(
                lobbyClientId);

        const leaveLobbyResult =
            leaveLobbyResponse.responseData ?? {};

        if (!leaveLobbyResult.isSuccess) {
            const clientResponse = new LobbyResponse(
                false, leaveLobbyResponse.returnCode);

            const responseData = Object.assign(
                clientResponse.serialize(),
                leaveLobbyResult);

            response.data = responseData;
            next();

            return;
        }

        const clientResponse = new LobbyResponse(
            true, ServerReturnCodes.OK);

        const responseData = Object.assign(
            clientResponse.serialize(),
            leaveLobbyResult);

        response.data = responseData;
        next();
    }

    private async changePet(request: any, response: any, next: any): Promise<void> {
        // this.logManager.showLog({
        //     log: util.format("***** CHANGE PET *****")
        // });

        let lobbyClientId = request.user["thirdPartyId"];
        if (CommonLogicUtil.isNullOrEmpty(lobbyClientId)) {
            const errorResponse = new LobbyResponse(
                false, ServerReturnCodes.MissingParameter);

            response.data = errorResponse.serialize();
            next();

            return;
        }

        let pet = request.body["pet"];
        if (CommonLogicUtil.isUndefined(pet)) {
            const errorResponse = new LobbyResponse(
                false, ServerReturnCodes.MissingParameter);

            response.data = errorResponse.serialize();
            next();

            return;
        }

        const changePetResponse =
            await ReferenceController.lobbyController.changePet(
                lobbyClientId,
                Pet.toClass(pet));

        const changePetResult =
            changePetResponse.responseData ?? {};

        if (!changePetResult.isSuccess) {
            const clientResponse = new LobbyResponse(
                false, changePetResponse.returnCode);

            const responseData = Object.assign(
                clientResponse.serialize(),
                changePetResult);

            response.data = responseData;
            next();

            return;
        }

        const clientResponse = new LobbyResponse(
            true, ServerReturnCodes.OK);

        const responseData = Object.assign(
            clientResponse.serialize(),
            changePetResult);

        response.data = responseData;
        next();
    }

    private async enterMiniGame(request: any, response: any, next: any): Promise<void> {
        // this.logManager.showLog({
        //     log: util.format("***** ENTER MINI GAME *****")
        // });

        let lobbyClientId = request.user["thirdPartyId"];
        if (CommonLogicUtil.isNullOrEmpty(lobbyClientId)) {
            const errorResponse = new LobbyResponse(
                false, ServerReturnCodes.MissingParameter);

            response.data = errorResponse.serialize();
            next();

            return;
        }

        const enterMiniGameResponse =
            await ReferenceController.lobbyController.enterMiniGame(
                lobbyClientId);

        const enterMiniGameResult =
            enterMiniGameResponse.responseData ?? {};

        if (!enterMiniGameResult.isSuccess) {
            const clientResponse = new LobbyResponse(
                false, enterMiniGameResponse.returnCode);

            const responseData = Object.assign(
                clientResponse.serialize(),
                enterMiniGameResult);

            response.data = responseData;
            next();

            return;
        }

        const clientResponse = new LobbyResponse(
            true, ServerReturnCodes.OK);

        const responseData = Object.assign(
            clientResponse.serialize(),
            enterMiniGameResult);

        response.data = responseData;
        next();
    }

    //#endregion

    //#endregion
}
