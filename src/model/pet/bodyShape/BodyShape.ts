/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {PetBodyShapes} from "../../../constant/pet/PetBodyShapes";
import {PetRarityTitles} from "../../../constant/pet/PetRarityTitles";

//#endregion

export class BodyShape {
    //#region Variables

    public name!: string;

    public star!: number;

    public shape!: PetBodyShapes;

    public rarity!: PetRarityTitles;

    //#endregion

    //#region Constructor

    constructor(
        name: string,
        star: number,
        shape: PetBodyShapes,
        rarity: PetRarityTitles) {
        this.name = name;

        this.star = star;

        this.shape = shape;

        this.rarity = rarity;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.name);

        totalBufferSize +=
            Serializer.floatSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.name);

        totalByteSize +=
            serializer.writeFloat(
                this.star);

        totalByteSize +=
            serializer.writeByte(
                this.shape);

        totalByteSize +=
            serializer.writeByte(
                this.rarity);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): BodyShape {
        const name =
            serializer.readString();

        const star =
            serializer.readFloat();

        const shape =
            serializer.readByte();

        const rarity =
            serializer.readByte();

        const bodyShape =
            new BodyShape(
                name, star, shape, rarity);

        return bodyShape;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        bodyShapeObj: BodyShape): BodyShape {
        const name =
            bodyShapeObj.name;

        const star =
            bodyShapeObj.star;

        const shape =
            bodyShapeObj.shape;

        const rarity =
            bodyShapeObj.rarity;

        const bodyShape =
            new BodyShape(
                name, star, shape, rarity);

        return bodyShape;
    }

    //#endregion

    //#endregion
}
