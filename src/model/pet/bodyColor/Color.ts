/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";

//#endregion

export class Color {
    //#region Variables

    public r!: number;

    public g!: number;

    public b!: number;

    public a!: number;

    //#endregion

    //#region Constructor

    constructor(
        r: number,
        g: number,
        b: number,
        a: number) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.byteSize;
        totalBufferSize +=
            Serializer.byteSize;
        totalBufferSize +=
            Serializer.byteSize;
        totalBufferSize +=
            Serializer.byteSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeByte(
                this.r);
        totalByteSize +=
            serializer.writeByte(
                this.g);
        totalByteSize +=
            serializer.writeByte(
                this.b);
        totalByteSize +=
            serializer.writeByte(
                this.a);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): Color {
        const r =
            serializer.readByte();
        const g =
            serializer.readByte();
        const b =
            serializer.readByte();
        const a =
            serializer.readByte();

        const color =
            new Color(
                r, g, b, a);

        return color;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        colorObj: Color): Color {
        const r =
            colorObj.r;
        const g =
            colorObj.g;
        const b =
            colorObj.b;
        const a =
            colorObj.a;

        const color =
            new Color(
                r, g, b, a);

        return color;
    }

    //#endregion

    //#endregion
}
