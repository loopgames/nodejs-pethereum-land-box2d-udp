/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {Color} from "./Color";
// import {PetMainColors} from "../../../constant/pet/PetMainColors";
import {BaseBodyColor} from "./BaseBodyColor";

//#endregion

export class EyeBodyColor
    extends BaseBodyColor {
    //#region Variables

    public color!: Color;

    //#endregion

    //#region Constructor

    constructor(
        name: string,
        star: number,
        color: Color) {
        super(name, star);

        this.color = color;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.name);

        totalBufferSize +=
            Serializer.floatSize;

        totalBufferSize +=
            this.color.getTotalBufferSize();

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.name);

        totalByteSize +=
            serializer.writeFloat(
                this.star);

        totalByteSize +=
            this.color.serialize(
                serializer);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): EyeBodyColor {
        const name =
            serializer.readString();

        const star =
            serializer.readFloat();

        const color =
            Color.deserialize(
                serializer);

        const eyeBodyColor =
            new EyeBodyColor(
                name, star, color);

        return eyeBodyColor;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        eyeBodyColorObj: EyeBodyColor): EyeBodyColor {
        const name =
            eyeBodyColorObj.name;

        const star =
            eyeBodyColorObj.star;

        const color =
            Color.toClass(
                eyeBodyColorObj.color);

        const eyeBodyColor =
            new EyeBodyColor(
                name, star, color);

        return eyeBodyColor;
    }

    //#endregion

    //#endregion
}
