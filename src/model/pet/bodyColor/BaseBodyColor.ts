/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";

//#endregion

export class BaseBodyColor {
    //#region Variables

    public name!: string;

    public star!: number;

    //#endregion

    //#region Constructor

    constructor(
        name: string,
        star: number) {
        this.name = name;

        this.star = star;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.name);

        totalBufferSize +=
            Serializer.floatSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.name);

        totalByteSize +=
            serializer.writeFloat(
                this.star);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): BaseBodyColor {
        const name =
            serializer.readString();

        const star =
            serializer.readFloat();

        const baseBodyColor =
            new BaseBodyColor(
                name, star);

        return baseBodyColor;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        baseBodyColorObj: BaseBodyColor): BaseBodyColor {
        const name =
            baseBodyColorObj.name;

        const star =
            baseBodyColorObj.star;

        const baseBodyColor =
            new BaseBodyColor(
                name, star);

        return baseBodyColor;
    }

    //#endregion

    //#endregion
}
