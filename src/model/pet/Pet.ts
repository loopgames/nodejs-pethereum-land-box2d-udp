/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";
import {BodyShape} from "./bodyShape/BodyShape";
import {MainBodyColor} from "./bodyColor/MainBodyColor";
import {SecondaryBodyColor} from "./bodyColor/SecondaryBodyColor";
import {LineBodyColor} from "./bodyColor/LineBodyColor";
import {EyeBodyColor} from "./bodyColor/EyeBodyColor";
import {EarBodyPart} from "./bodyPart/EarBodyPart";
import {EyeBodyPart} from "./bodyPart/EyeBodyPart";
import {FootBodyPart} from "./bodyPart/FootBodyPart";
import {HairBodyPart} from "./bodyPart/HairBodyPart";
import {MouthBodyPart} from "./bodyPart/MouthBodyPart";
import {PatternBodyPart} from "./bodyPart/PatternBodyPart";
import {TailBodyPart} from "./bodyPart/TailBodyPart";
import {PetRarityTitles} from "../../constant/pet/PetRarityTitles";

//#endregion

export class Pet {
    //#region Variables

    public petId!: string;

    public name!: string;

    public star!: number;
    public totalStar!: number;

    public rarity!: PetRarityTitles;

    public createdTimestamp!: number;

    public bodyShape!: BodyShape;

    public mainBodyColor!: MainBodyColor;
    public secondaryBodyColor!: SecondaryBodyColor;
    public lineBodyColor!: LineBodyColor;
    public eyeBodyColor!: EyeBodyColor;

    public earBodyPart!: EarBodyPart;
    public eyeBodyPart!: EyeBodyPart;
    public footBodyPart!: FootBodyPart;
    public hairBodyPart!: HairBodyPart;
    public mouthBodyPart!: MouthBodyPart;
    public tailBodyPart!: TailBodyPart;
    public patternBodyPart!: PatternBodyPart;

    //#endregion

    //#region Constructor

    constructor(
        petId: string,
        name: string,
        star: number,
        totalStar: number,
        rarity: PetRarityTitles,
        createdTimestamp: number,
        bodyShape: BodyShape,
        mainBodyColor: MainBodyColor,
        secondaryBodyColor: SecondaryBodyColor,
        lineBodyColor: LineBodyColor,
        eyeBodyColor: EyeBodyColor,
        earBodyPart: EarBodyPart,
        eyeBodyPart: EyeBodyPart,
        footBodyPart: FootBodyPart,
        hairBodyPart: HairBodyPart,
        mouthBodyPart: MouthBodyPart,
        tailBodyPart: TailBodyPart,
        patternBodyPart: PatternBodyPart) {
        this.petId = petId;

        this.name = name;

        this.star = star;
        this.totalStar = totalStar;

        this.rarity = rarity;

        this.createdTimestamp = createdTimestamp;

        this.bodyShape = bodyShape;

        this.mainBodyColor = mainBodyColor;
        this.secondaryBodyColor = secondaryBodyColor;
        this.lineBodyColor = lineBodyColor;
        this.eyeBodyColor = eyeBodyColor;

        this.earBodyPart = earBodyPart;
        this.eyeBodyPart = eyeBodyPart;
        this.footBodyPart = footBodyPart;
        this.hairBodyPart = hairBodyPart;
        this.mouthBodyPart = mouthBodyPart;
        this.tailBodyPart = tailBodyPart;
        this.patternBodyPart = patternBodyPart;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.petId);

        totalBufferSize +=
            Serializer.stringSize(
                this.name);

        totalBufferSize +=
            Serializer.floatSize;
        totalBufferSize +=
            Serializer.floatSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.intSize;

        totalBufferSize +=
            this.bodyShape.getTotalBufferSize();

        totalBufferSize +=
            this.mainBodyColor.getTotalBufferSize();
        totalBufferSize +=
            this.secondaryBodyColor.getTotalBufferSize();
        totalBufferSize +=
            this.lineBodyColor.getTotalBufferSize();
        totalBufferSize +=
            this.eyeBodyColor.getTotalBufferSize();

        totalBufferSize +=
            this.earBodyPart.getTotalBufferSize();
        totalBufferSize +=
            this.eyeBodyPart.getTotalBufferSize();
        totalBufferSize +=
            this.footBodyPart.getTotalBufferSize();
        totalBufferSize +=
            this.hairBodyPart.getTotalBufferSize();
        totalBufferSize +=
            this.mouthBodyPart.getTotalBufferSize();
        totalBufferSize +=
            this.tailBodyPart.getTotalBufferSize();
        totalBufferSize +=
            this.patternBodyPart.getTotalBufferSize();

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.petId);

        totalByteSize +=
            serializer.writeString(
                this.name);

        totalByteSize +=
            serializer.writeFloat(
                this.star);
        totalByteSize +=
            serializer.writeFloat(
                this.totalStar);

        totalByteSize +=
            serializer.writeByte(
                this.rarity);

        totalByteSize +=
            serializer.writeInt(
                this.createdTimestamp);

        totalByteSize +=
            this.bodyShape.serialize(
                serializer);

        totalByteSize +=
            this.mainBodyColor.serialize(
                serializer);
        totalByteSize +=
            this.secondaryBodyColor.serialize(
                serializer);
        totalByteSize +=
            this.lineBodyColor.serialize(
                serializer);
        totalByteSize +=
            this.eyeBodyColor.serialize(
                serializer);

        totalByteSize +=
            this.earBodyPart.serialize(
                serializer);
        totalByteSize +=
            this.eyeBodyPart.serialize(
                serializer);
        totalByteSize +=
            this.footBodyPart.serialize(
                serializer);
        totalByteSize +=
            this.hairBodyPart.serialize(
                serializer);
        totalByteSize +=
            this.mouthBodyPart.serialize(
                serializer);
        totalByteSize +=
            this.tailBodyPart.serialize(
                serializer);
        totalByteSize +=
            this.patternBodyPart.serialize(
                serializer);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): Pet {
        const petId =
            serializer.readString();

        const name =
            serializer.readString();

        const star =
            serializer.readFloat();
        const totalStar =
            serializer.readFloat();

        const rarity =
            serializer.readByte();

        const createdTimestamp =
            serializer.readInt();

        const bodyShape =
            BodyShape.deserialize(
                serializer);

        const mainBodyColor =
            MainBodyColor.deserialize(
                serializer);
        const secondaryBodyColor =
            SecondaryBodyColor.deserialize(
                serializer);
        const lineBodyColor =
            LineBodyColor.deserialize(
                serializer);
        const eyeBodyColor =
            EyeBodyColor.deserialize(
                serializer);

        const earBodyPart =
            EarBodyPart.deserialize(
                serializer);
        const eyeBodyPart =
            EyeBodyPart.deserialize(
                serializer);
        const footBodyPart =
            FootBodyPart.deserialize(
                serializer);
        const hairBodyPart =
            HairBodyPart.deserialize(
                serializer);
        const mouthBodyPart =
            MouthBodyPart.deserialize(
                serializer);
        const tailBodyPart =
            TailBodyPart.deserialize(
                serializer);
        const patternBodyPart =
            PatternBodyPart.deserialize(
                serializer);

        const pet =
            new Pet(
                petId,
                name,
                star, totalStar,
                rarity,
                createdTimestamp,
                bodyShape,
                mainBodyColor, secondaryBodyColor, lineBodyColor, eyeBodyColor,
                earBodyPart, eyeBodyPart, footBodyPart, hairBodyPart, mouthBodyPart, tailBodyPart, patternBodyPart);

        return pet;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        petObj: Pet): Pet {
        const petId =
            petObj.petId;

        const name =
            petObj.name;

        const star =
            petObj.star;
        const totalStar =
            petObj.totalStar;

        const rarity =
            petObj.rarity;

        const createdTimestamp =
            petObj.createdTimestamp;

        const bodyShape =
            BodyShape.toClass(
                petObj.bodyShape);

        const mainBodyColor =
            MainBodyColor.toClass(
                petObj.mainBodyColor);
        const secondaryBodyColor =
            SecondaryBodyColor.toClass(
                petObj.secondaryBodyColor);
        const lineBodyColor =
            LineBodyColor.toClass(
                petObj.lineBodyColor);
        const eyeBodyColor =
            EyeBodyColor.toClass(
                petObj.eyeBodyColor);

        const earBodyPart =
            EarBodyPart.toClass(
                petObj.earBodyPart);
        const eyeBodyPart =
            EyeBodyPart.toClass(
                petObj.eyeBodyPart);
        const footBodyPart =
            FootBodyPart.toClass(
                petObj.footBodyPart);
        const hairBodyPart =
            HairBodyPart.toClass(
                petObj.hairBodyPart);
        const mouthBodyPart =
            MouthBodyPart.toClass(
                petObj.mouthBodyPart);
        const tailBodyPart =
            TailBodyPart.toClass(
                petObj.tailBodyPart);
        const patternBodyPart =
            PatternBodyPart.toClass(
                petObj.patternBodyPart);

        const pet =
            new Pet(
                petId,
                name,
                star, totalStar,
                rarity,
                createdTimestamp,
                bodyShape,
                mainBodyColor, secondaryBodyColor, lineBodyColor, eyeBodyColor,
                earBodyPart, eyeBodyPart, footBodyPart, hairBodyPart, mouthBodyPart, tailBodyPart, patternBodyPart);

        return pet;
    }

    //#endregion

    //#endregion
}
