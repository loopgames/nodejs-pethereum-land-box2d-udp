/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {PetFootTypes} from "../../../constant/pet/PetFootTypes";
import {PetRarityTitles} from "../../../constant/pet/PetRarityTitles";
import {BaseBodyPart} from "./BaseBodyPart";

//#endregion

export class FootBodyPart
    extends BaseBodyPart {
    //#region Variables

    public type!: PetFootTypes;

    public rarity!: PetRarityTitles;

    //#endregion

    //#region Constructor

    constructor(
        name: string,
        star: number,
        type: PetFootTypes,
        rarity: PetRarityTitles) {
        super(name, star);

        this.type = type;

        this.rarity = rarity;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.name);

        totalBufferSize +=
            Serializer.floatSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.name);

        totalByteSize +=
            serializer.writeFloat(
                this.star);

        totalByteSize +=
            serializer.writeByte(
                this.type);

        totalByteSize +=
            serializer.writeByte(
                this.rarity);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): FootBodyPart {
        const name =
            serializer.readString();

        const star =
            serializer.readFloat();

        const type =
            serializer.readByte();

        const rarity =
            serializer.readByte();

        const footBodyPart =
            new FootBodyPart(
                name, star, type, rarity);

        return footBodyPart;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        footBodyPartObj: FootBodyPart): FootBodyPart {
        const name =
            footBodyPartObj.name;

        const star =
            footBodyPartObj.star;

        const type =
            footBodyPartObj.type;

        const rarity =
            footBodyPartObj.rarity;

        const footBodyPart =
            new FootBodyPart(
                name, star, type, rarity);

        return footBodyPart;
    }

    //#endregion

    //#endregion
}
