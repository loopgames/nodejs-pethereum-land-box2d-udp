/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {PetPatternTypes} from "../../../constant/pet/PetPatternTypes";
import {PetRarityTitles} from "../../../constant/pet/PetRarityTitles";
import {BaseBodyPart} from "./BaseBodyPart";

//#endregion

export class PatternBodyPart
    extends BaseBodyPart {
    //#region Variables

    public type!: PetPatternTypes;

    public rarity!: PetRarityTitles;

    //#endregion

    //#region Constructor

    constructor(
        name: string,
        star: number,
        type: PetPatternTypes,
        rarity: PetRarityTitles) {
        super(name, star);

        this.type = type;

        this.rarity = rarity;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.name);

        totalBufferSize +=
            Serializer.floatSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.name);

        totalByteSize +=
            serializer.writeFloat(
                this.star);

        totalByteSize +=
            serializer.writeByte(
                this.type);

        totalByteSize +=
            serializer.writeByte(
                this.rarity);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): PatternBodyPart {
        const name =
            serializer.readString();

        const star =
            serializer.readFloat();

        const type =
            serializer.readByte();

        const rarity =
            serializer.readByte();

        const patternBodyPart =
            new PatternBodyPart(
                name, star, type, rarity);

        return patternBodyPart;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        patternBodyPartObj: PatternBodyPart): PatternBodyPart {
        const name =
            patternBodyPartObj.name;

        const star =
            patternBodyPartObj.star;

        const type =
            patternBodyPartObj.type;

        const rarity =
            patternBodyPartObj.rarity;

        const patternBodyPart =
            new PatternBodyPart(
                name, star, type, rarity);

        return patternBodyPart;
    }

    //#endregion

    //#endregion
}
