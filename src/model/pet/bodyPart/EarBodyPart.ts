/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {PetEarTypes} from "../../../constant/pet/PetEarTypes";
import {PetRarityTitles} from "../../../constant/pet/PetRarityTitles";
import {BaseBodyPart} from "./BaseBodyPart";

//#endregion

export class EarBodyPart
    extends BaseBodyPart {
    //#region Variables

    public type!: PetEarTypes;

    public rarity!: PetRarityTitles;

    //#endregion

    //#region Constructor

    constructor(
        name: string,
        star: number,
        type: PetEarTypes,
        rarity: PetRarityTitles) {
        super(name, star);

        this.type = type;

        this.rarity = rarity;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.name);

        totalBufferSize +=
            Serializer.floatSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.name);

        totalByteSize +=
            serializer.writeFloat(
                this.star);

        totalByteSize +=
            serializer.writeByte(
                this.type);

        totalByteSize +=
            serializer.writeByte(
                this.rarity);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): EarBodyPart {
        const name =
            serializer.readString();

        const star =
            serializer.readFloat();

        const type =
            serializer.readByte();

        const rarity =
            serializer.readByte();

        const earBodyPart =
            new EarBodyPart(
                name, star, type, rarity);

        return earBodyPart;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        earBodyPartObj: EarBodyPart): EarBodyPart {
        const name =
            earBodyPartObj.name;

        const star =
            earBodyPartObj.star;

        const type =
            earBodyPartObj.type;

        const rarity =
            earBodyPartObj.rarity;

        const earBodyPart =
            new EarBodyPart(
                name, star, type, rarity);

        return earBodyPart;
    }

    //#endregion

    //#endregion
}
