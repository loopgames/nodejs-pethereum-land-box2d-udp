/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {PetEyeTypes} from "../../../constant/pet/PetEyeTypes";
import {PetRarityTitles} from "../../../constant/pet/PetRarityTitles";
import {BaseBodyPart} from "./BaseBodyPart";

//#endregion

export class EyeBodyPart
    extends BaseBodyPart {
    //#region Variables

    public type!: PetEyeTypes;

    public rarity!: PetRarityTitles;

    //#endregion

    //#region Constructor

    constructor(
        name: string,
        star: number,
        type: PetEyeTypes,
        rarity: PetRarityTitles) {
        super(name, star);

        this.type = type;

        this.rarity = rarity;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.name);

        totalBufferSize +=
            Serializer.floatSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.name);

        totalByteSize +=
            serializer.writeFloat(
                this.star);

        totalByteSize +=
            serializer.writeByte(
                this.type);

        totalByteSize +=
            serializer.writeByte(
                this.rarity);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): EyeBodyPart {
        const name =
            serializer.readString();

        const star =
            serializer.readFloat();

        const type =
            serializer.readByte();

        const rarity =
            serializer.readByte();

        const eyeBodyPart =
            new EyeBodyPart(
                name, star, type, rarity);

        return eyeBodyPart;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        eyeBodyPartObj: EyeBodyPart): EyeBodyPart {
        const name =
            eyeBodyPartObj.name;

        const star =
            eyeBodyPartObj.star;

        const type =
            eyeBodyPartObj.type;

        const rarity =
            eyeBodyPartObj.rarity;

        const eyeBodyPart =
            new EyeBodyPart(
                name, star, type, rarity);

        return eyeBodyPart;
    }

    //#endregion

    //#endregion
}
