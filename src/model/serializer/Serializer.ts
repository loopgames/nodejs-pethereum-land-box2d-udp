/**
 * Created by hulusionder on 06/10/2021.
 */

"use strict";

//#region Imports

import util from "util";

import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {SerializationMode} from "../../constant/serializer/SerializationMode";

//#endregion

export class Serializer {
    //#region Variables

    public static byteSize: number = 1;
    public static uShortSize: number = 2;
    public static shortSize: number = 2;
    public static uIntSize: number = 4;
    public static intSize: number = 4;
    public static floatSize: number = 4;
    public static uLongSize: number = 8;

    private readonly charMinValue: string = "\0";
    private readonly charMaxValue: string = "\uFFFF";

    private serializationMode!: SerializationMode;

    private serializerCursor!: number;
    private serializerBuffer!: Uint8Array;

    //#endregion

    //#region Constructor

    constructor(
        serializationMode: SerializationMode,
        serializerBuffer: Uint8Array) {
        this.serializationMode = serializationMode;

        this.serializerCursor = 0;
        this.serializerBuffer = serializerBuffer;
    }

    //#endregion

    //#region Methods

    //#region Get Serializer Buffer Methods

    public getSerializerBuffer(): Uint8Array {
        return this.serializerBuffer;
    }

    //#endregion

    //#region String Methods

    public static stringSize(
        str: string): number {
        if (str.length > 255) {
            throw new Error(
                util.format("String is to big to be serialized!"));
        }

        const stringSize =
            Buffer.byteLength(str, "utf8") + 1;

        return stringSize;
    }

    public writeString(
        str: string): number {
        if (str.length > 255) {
            throw new Error(
                util.format("String is to big to be serialized!"));
        }

        const strSize =
            Buffer.byteLength(str, "utf8") + 1;

        if (this.serializerCursor + strSize >= this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to serialize that data!"));
        }

        this.serializerBuffer[this.serializerCursor++] += str.length;

        const strBuffer =
            Buffer.from(str);
        strBuffer.copy(
            this.serializerBuffer, this.serializerCursor);

        this.serializerCursor += str.length;

        return strSize;
    }

    public writeStringA2S(
        str: string): number {
        if (str.length > 255) {
            throw new Error(
                util.format("String is to big to be serialized!"));
        }

        const strSize =
            Buffer.byteLength(str, "utf8") + 1;

        if (this.serializerCursor + strSize >= this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to serialize that data!"));
        }

        const strBuffer =
            Buffer.from(str);
        strBuffer.copy(
            this.serializerBuffer, this.serializerCursor);

        this.serializerCursor += str.length; // Verify the cursor gets pushed up here

        this.serializerBuffer[this.serializerCursor++] =
            CommonLogicUtil.convertCharToByte(this.charMinValue);

        return strSize;
    }

    public readString(): string {
        if (this.serializerCursor + 2 >
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to read!"));
        }

        const strLength =
            this.serializerBuffer[this.serializerCursor++];

        if (this.serializerCursor + strLength >=
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to read!"));
        }

        const strBuffer =
            Buffer.alloc(strLength);
        strBuffer.fill(
            this.serializerBuffer.slice(this.serializerCursor, this.serializerCursor + strLength));

        const str =
            strBuffer.toString();

        this.serializerCursor += strLength; // Verify the cursor gets pushed up here

        return str;
    }

    //#endregion

    //#region Byte Methods

    public writeByte(
        byte: number): number {
        if (this.serializerCursor + Serializer.byteSize >
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to serialize that data!"));
        }

        this.serializerBuffer[this.serializerCursor++] =
            byte;

        return Serializer.byteSize;
    }

    public readByte(): number {
        if (this.serializerCursor >= this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to read the data!"));
        }

        const byte =
            this.serializerBuffer[this.serializerCursor++];

        return byte;
    }

    //#endregion

    //#region Short Methods

    public writeShort(
        short: number): number {
        if (this.serializerCursor + Serializer.shortSize >
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to serialize that data!"));
        }

        const shortBuffer =
            Buffer.alloc(2);
        const byteValue =
            shortBuffer.writeInt16LE(short);

        shortBuffer.copy(
            this.serializerBuffer, this.serializerCursor);

        this.serializerCursor +=
            shortBuffer.length;

        return Serializer.shortSize;
    }

    public readShort(): number {
        if (this.serializerCursor + Serializer.shortSize >
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to read the data!"));
        }

        const shortDataView =
            new DataView(this.serializerBuffer.buffer);
        const short =
            shortDataView.getInt16(this.serializerCursor, true);

        this.serializerCursor +=
            Serializer.shortSize; // Verify the cursor gets pushed up here

        return short;
    }

    //#endregion

    //#region Int Methods

    public writeInt(
        integer: number): number {
        if (this.serializerCursor + Serializer.intSize >
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to serialize that data!"));
        }

        const integerBuffer =
            Buffer.alloc(4);
        const byteValue =
            integerBuffer.writeInt32LE(integer);

        integerBuffer.copy(
            this.serializerBuffer, this.serializerCursor);

        this.serializerCursor +=
            integerBuffer.length;

        return Serializer.intSize;
    }

    public readInt(): number {
        if (this.serializerCursor + Serializer.intSize >
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to read the data!"));
        }

        const integerDataView =
            new DataView(this.serializerBuffer.buffer);
        const integer =
            integerDataView.getInt32(this.serializerCursor, true);

        this.serializerCursor +=
            Serializer.intSize; // Verify the cursor gets pushed up here

        return integer;
    }

    //#endregion

    //#region Float Methods

    public writeFloat(
        float: number): number {
        if (this.serializerCursor + Serializer.floatSize >
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to serialize that data!"));
        }

        const floatBuffer =
            Buffer.alloc(4);
        const byteValue =
            floatBuffer.writeFloatLE(float);

        floatBuffer.copy(
            this.serializerBuffer, this.serializerCursor);

        this.serializerCursor +=
            floatBuffer.length;

        return Serializer.floatSize;
    }

    public readFloat(): number {
        if (this.serializerCursor + Serializer.floatSize >
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to read the data!"));
        }

        const floatDataView =
            new DataView(this.serializerBuffer.buffer);
        const float =
            floatDataView.getFloat32(this.serializerCursor, true);

        this.serializerCursor +=
            Serializer.floatSize; // Verify the cursor gets pushed up here

        return float;
    }

    //#endregion

    //#region Long Methods

    public writeLong(
        long: bigint): number {
        if (this.serializerCursor + Serializer.uLongSize >
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to serialize that data!"));
        }

        const longBuffer =
            Buffer.alloc(4);
        const byteValue =
            longBuffer.writeBigUint64LE(long);

        longBuffer.copy(
            this.serializerBuffer, this.serializerCursor);

        this.serializerCursor +=
            longBuffer.length;

        return Serializer.uLongSize;
    }

    public readLong(): bigint {
        if (this.serializerCursor + Serializer.uLongSize >
            this.serializerBuffer.length) {
            throw new Error(
                util.format("Buffer is too small to read the data!"));
        }

        const longDataView =
            new DataView(this.serializerBuffer.buffer);
        const long =
            longDataView.getBigUint64(this.serializerCursor, true);

        this.serializerCursor +=
            Serializer.uLongSize; // Verify the cursor gets pushed up here

        return long;
    }

    //#endregion

    //#endregion
}
