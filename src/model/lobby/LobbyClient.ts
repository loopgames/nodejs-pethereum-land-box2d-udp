/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";
import {Pet} from "../pet/Pet";
import {LobbyClientPosition} from "./LobbyClientPosition";
import {LobbyClientLocations} from "../../constant/lobby/LobbyClientLocations";

//#endregion

export class LobbyClient {
    //#region Variables

    public id!: string;

    public movementSpeed!: number;

    public pet!: Pet;

    public lobbyClientPosition!: LobbyClientPosition;

    public lobbyClientLocation!: LobbyClientLocations;

    //#endregion

    //#region Constructor

    constructor(
        id: string,
        movementSpeed: number,
        pet: Pet,
        lobbyClientPosition: LobbyClientPosition,
        lobbyClientLocation: LobbyClientLocations) {
        this.id = id;

        this.movementSpeed = movementSpeed;

        this.pet = pet;

        this.lobbyClientPosition = lobbyClientPosition;

        this.lobbyClientLocation = lobbyClientLocation;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.id);

        totalBufferSize +=
            Serializer.floatSize;

        totalBufferSize +=
            this.pet.getTotalBufferSize();

        totalBufferSize +=
            this.lobbyClientPosition.getTotalBufferSize();

        totalBufferSize +=
            Serializer.byteSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.id);

        totalByteSize +=
            serializer.writeFloat(
                this.movementSpeed);

        totalByteSize +=
            this.pet.serialize(
                serializer);

        totalByteSize +=
            this.lobbyClientPosition.serialize(
                serializer);

        totalByteSize +=
            serializer.writeByte(
                this.lobbyClientLocation);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): LobbyClient {
        const id =
            serializer.readString();

        const movementSpeed =
            serializer.readFloat();

        const pet =
            Pet.deserialize(
                serializer);

        const lobbyClientPosition =
            LobbyClientPosition.deserialize(
                serializer);

        const lobbyClientLocation =
            serializer.readByte();

        const lobbyClient =
            new LobbyClient(
                id,
                movementSpeed,
                pet,
                lobbyClientPosition,
                lobbyClientLocation);

        return lobbyClient;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        lobbyClientObj: LobbyClient): LobbyClient {
        const id =
            lobbyClientObj.id;

        const movementSpeed =
            lobbyClientObj.movementSpeed;

        const pet =
            Pet.toClass(
                lobbyClientObj.pet);

        const lobbyClientPosition =
            LobbyClientPosition.toClass(
                lobbyClientObj.lobbyClientPosition);

        const lobbyClientLocation =
            lobbyClientObj.lobbyClientLocation;

        const lobbyClient =
            new LobbyClient(
                id,
                movementSpeed,
                pet,
                lobbyClientPosition,
                lobbyClientLocation);

        return lobbyClient;
    }

    //#endregion

    //#endregion
}
