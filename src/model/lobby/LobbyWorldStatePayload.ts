/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";
import {LobbyClientPosition} from "./LobbyClientPosition";
import {LobbyClientInput} from "./LobbyClientInput";

//#endregion

export class LobbyWorldStatePayload {
    //#region Variables

    public id!: string;

    public tick!: number;

    public lobbyClientInput!: LobbyClientInput;

    public lobbyClientPosition!: LobbyClientPosition;

    //#endregion

    //#region Constructor

    constructor(
        id: string,
        tick: number,
        lobbyClientInput: LobbyClientInput,
        lobbyClientPosition: LobbyClientPosition) {
        this.id = id;

        this.tick = tick;

        this.lobbyClientInput = lobbyClientInput;

        this.lobbyClientPosition = lobbyClientPosition;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.id);

        totalBufferSize +=
            Serializer.intSize;

        totalBufferSize +=
            this.lobbyClientInput.getTotalBufferSize();

        totalBufferSize +=
            this.lobbyClientPosition.getTotalBufferSize();

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.id);

        totalByteSize +=
            serializer.writeInt(
                this.tick);

        totalByteSize +=
            this.lobbyClientInput.serialize(
                serializer);

        totalByteSize +=
            this.lobbyClientPosition.serialize(
                serializer);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): LobbyWorldStatePayload {
        const id =
            serializer.readString();

        const tick =
            serializer.readInt();

        const lobbyClientInput =
            LobbyClientInput.deserialize(
                serializer);

        const lobbyClientPosition =
            LobbyClientPosition.deserialize(
                serializer);

        const lobbyWorldStatePayload =
            new LobbyWorldStatePayload(
                id,
                tick,
                lobbyClientInput,
                lobbyClientPosition);

        return lobbyWorldStatePayload;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        lobbyWorldStatePayloadObj: LobbyWorldStatePayload): LobbyWorldStatePayload {
        const id =
            lobbyWorldStatePayloadObj.id;

        const tick =
            lobbyWorldStatePayloadObj.tick;

        const lobbyClientInput =
            LobbyClientInput.toClass(
                lobbyWorldStatePayloadObj.lobbyClientInput);

        const lobbyClientPosition =
            LobbyClientPosition.toClass(
                lobbyWorldStatePayloadObj.lobbyClientPosition);

        const lobbyWorldStatePayload =
            new LobbyWorldStatePayload(
                id,
                tick,
                lobbyClientInput,
                lobbyClientPosition);

        return lobbyWorldStatePayload;
    }

    //#endregion

    //#endregion
}
