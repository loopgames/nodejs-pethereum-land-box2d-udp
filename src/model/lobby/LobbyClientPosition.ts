/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";

//#endregion

export class LobbyClientPosition {
    //#region Variables

    public x!: number;
    public y!: number;

    //#endregion

    //#region Constructor

    constructor(
        x: number,
        y: number) {
        this.x = x;
        this.y = y;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.floatSize;
        totalBufferSize +=
            Serializer.floatSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeFloat(
                this.x);
        totalByteSize +=
            serializer.writeFloat(
                this.y);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): LobbyClientPosition {
        const x =
            serializer.readFloat();
        const y =
            serializer.readFloat();

        const lobbyClientPosition =
            new LobbyClientPosition(
                x, y);

        return lobbyClientPosition;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        lobbyClientPositionObj: LobbyClientPosition): LobbyClientPosition {
        const x =
            lobbyClientPositionObj.x;
        const y =
            lobbyClientPositionObj.y;

        const lobbyClientPosition =
            new LobbyClientPosition(
                x, y);

        return lobbyClientPosition;
    }

    //#endregion

    //#endregion
}
