/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";

//#endregion

export class LobbyClientSenderInfo {
    //#region Variables

    public id!: string;

    public address!: string;
    public port!: number;

    //#endregion

    //#region Constructor

    constructor(
        id: string,
        address: string,
        port: number) {
        this.id = id;

        this.address = address;
        this.port = port;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.id);

        totalBufferSize +=
            Serializer.stringSize(
                this.address);
        totalBufferSize +=
            Serializer.shortSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeString(
                this.id);

        totalByteSize +=
            serializer.writeString(
                this.address);
        totalByteSize +=
            serializer.writeShort(
                this.port);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): LobbyClientSenderInfo {
        const id =
            serializer.readString();

        const address =
            serializer.readString();
        const port =
            serializer.readShort();

        const lobbyClientSenderInfo =
            new LobbyClientSenderInfo(
                id,
                address, port);

        return lobbyClientSenderInfo;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        lobbyClientSenderInfoObj: LobbyClientSenderInfo): LobbyClientSenderInfo {
        const id =
            lobbyClientSenderInfoObj.id;

        const address =
            lobbyClientSenderInfoObj.address;
        const port =
            lobbyClientSenderInfoObj.port;

        const lobbyClientSenderInfo =
            new LobbyClientSenderInfo(
                id,
                address, port);

        return lobbyClientSenderInfo;
    }

    //#endregion

    //#endregion
}
