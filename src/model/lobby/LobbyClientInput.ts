/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";

//#endregion

export class LobbyClientInput {
    //#region Variables

    public horizontal!: number;
    public vertical!: number;

    //#endregion

    //#region Constructor

    constructor(
        horizontal: number,
        vertical: number) {
        this.horizontal = horizontal;
        this.vertical = vertical;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.floatSize;
        totalBufferSize +=
            Serializer.floatSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeFloat(
                this.horizontal);
        totalByteSize +=
            serializer.writeFloat(
                this.vertical);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): LobbyClientInput {
        const horizontal =
            serializer.readFloat();
        const vertical =
            serializer.readFloat();

        const lobbyClientInput =
            new LobbyClientInput(
                horizontal, vertical);

        return lobbyClientInput;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        lobbyClientInputObj: LobbyClientInput): LobbyClientInput {
        const horizontal =
            lobbyClientInputObj.horizontal;
        const vertical =
            lobbyClientInputObj.vertical;

        const lobbyClientInput =
            new LobbyClientInput(
                horizontal, vertical);

        return lobbyClientInput;
    }

    //#endregion

    //#endregion
}
