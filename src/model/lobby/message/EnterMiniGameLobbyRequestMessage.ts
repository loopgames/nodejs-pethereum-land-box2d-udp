/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {ClientLobbyMessageTypes} from "../../../constant/lobby/ClientLobbyMessageTypes";
import {BaseLobbyRequestMessage} from "./BaseLobbyRequestMessage";

//#endregion

export class EnterMiniGameLobbyRequestMessage
    extends BaseLobbyRequestMessage {
    //#region Variables

    public id!: string;

    //#endregion

    //#region Constructor

    constructor(
        lobbyMessageType: ClientLobbyMessageTypes,
        id: string) {
        super(lobbyMessageType);

        this.id = id;
    }

    //#endregion

    //#region Methods

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): EnterMiniGameLobbyRequestMessage {
        const lobbyMessageType =
            serializer.readShort();

        const id =
            serializer.readString();

        const enterMiniGameLobbyRequestMessage =
            new EnterMiniGameLobbyRequestMessage(
                lobbyMessageType,
                id);

        return enterMiniGameLobbyRequestMessage;
    }

    //#endregion

    //#endregion
}
