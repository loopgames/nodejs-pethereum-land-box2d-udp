/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {ServerReturnCodes} from "../../../constant/server/ServerReturnCodes";
import {ServerLobbyMessageTypes} from "../../../constant/lobby/ServerLobbyMessageTypes";
import {BaseLobbyResponseMessage} from "./BaseLobbyResponseMessage";
import {LobbyWorldStatePayload} from "../LobbyWorldStatePayload";

//#endregion

export class UpdateLobbyWorldStateResponseMessage
    extends BaseLobbyResponseMessage {
    //#region Variables

    public lobbyWorldStatePayloads!: LobbyWorldStatePayload[];

    //#endregion

    //#region Constructor

    constructor(
        returnCode: ServerReturnCodes,
        lobbyWorldStatePayloads: LobbyWorldStatePayload[]) {
        super(returnCode);

        this.lobbyMessageType = ServerLobbyMessageTypes.UpdateWorldState;

        this.lobbyWorldStatePayloads = lobbyWorldStatePayloads;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            Serializer.shortSize;

        if (this.lobbyWorldStatePayloads) {
            for (let i = 0; i < this.lobbyWorldStatePayloads.length; i++) {
                const lobbyWorldStatePayload =
                    this.lobbyWorldStatePayloads[i];
                if (!lobbyWorldStatePayload) {
                    continue;
                }

                totalBufferSize +=
                    lobbyWorldStatePayload.getTotalBufferSize();
            }
        }

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeShort(
                this.returnCode);

        totalByteSize +=
            serializer.writeShort(
                this.lobbyMessageType);

        const lobbyWorldStatePayloadsCount =
            this.lobbyWorldStatePayloads ? this.lobbyWorldStatePayloads.length : 0;
        totalByteSize +=
            serializer.writeShort(
                lobbyWorldStatePayloadsCount);

        if (this.lobbyWorldStatePayloads) {
            for (let i = 0; i < this.lobbyWorldStatePayloads.length; i++) {
                const lobbyWorldStatePayload =
                    this.lobbyWorldStatePayloads[i];
                if (!lobbyWorldStatePayload) {
                    continue;
                }

                totalByteSize +=
                    lobbyWorldStatePayload.serialize(
                        serializer);
            }
        }

        return totalByteSize;
    }

    //#endregion

    //#endregion
}
