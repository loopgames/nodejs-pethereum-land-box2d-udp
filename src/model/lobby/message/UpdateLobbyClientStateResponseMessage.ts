/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {ServerReturnCodes} from "../../../constant/server/ServerReturnCodes";
import {ServerLobbyMessageTypes} from "../../../constant/lobby/ServerLobbyMessageTypes";
import {BaseLobbyResponseMessage} from "./BaseLobbyResponseMessage";
import {LobbyClient} from "../LobbyClient";

//#endregion

export class UpdateLobbyClientStateResponseMessage
    extends BaseLobbyResponseMessage {
    //#region Variables

    public lobbyClientIds!: string[];

    public lobbyClients!: LobbyClient[];

    //#endregion

    //#region Constructor

    constructor(
        returnCode: ServerReturnCodes,
        lobbyClientIds: string[],
        lobbyClients: LobbyClient[]) {
        super(returnCode);

        this.lobbyMessageType = ServerLobbyMessageTypes.UpdateClientState;

        this.lobbyClientIds = lobbyClientIds;

        this.lobbyClients = lobbyClients;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            Serializer.shortSize;

        if (this.lobbyClientIds) {
            for (let i = 0; i < this.lobbyClientIds.length; i++) {
                const lobbyClientId =
                    this.lobbyClientIds[i];
                if (!lobbyClientId) {
                    continue;
                }

                totalBufferSize +=
                    Serializer.stringSize(
                        lobbyClientId);
            }
        }

        totalBufferSize +=
            Serializer.shortSize;

        if (this.lobbyClients) {
            for (let i = 0; i < this.lobbyClients.length; i++) {
                const lobbyClient =
                    this.lobbyClients[i];
                if (!lobbyClient) {
                    continue;
                }

                totalBufferSize +=
                    lobbyClient.getTotalBufferSize();
            }
        }

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeShort(
                this.returnCode);

        totalByteSize +=
            serializer.writeShort(
                this.lobbyMessageType);

        const lobbyClientIdsCountCount =
            this.lobbyClientIds ? this.lobbyClientIds.length : 0;
        totalByteSize +=
            serializer.writeShort(
                lobbyClientIdsCountCount);

        if (this.lobbyClientIds) {
            for (let i = 0; i < this.lobbyClientIds.length; i++) {
                const lobbyClientId =
                    this.lobbyClientIds[i];
                if (!lobbyClientId) {
                    continue;
                }

                serializer.writeString(
                    lobbyClientId);
            }
        }

        const lobbyClientsCount =
            this.lobbyClients ? this.lobbyClients.length : 0;
        totalByteSize +=
            serializer.writeShort(
                lobbyClientsCount);

        if (this.lobbyClients) {
            for (let i = 0; i < this.lobbyClients.length; i++) {
                const lobbyClient =
                    this.lobbyClients[i];
                if (!lobbyClient) {
                    continue;
                }

                totalByteSize +=
                    lobbyClient.serialize(
                        serializer);
            }
        }

        return totalByteSize;
    }

    //#endregion

    //#endregion
}
