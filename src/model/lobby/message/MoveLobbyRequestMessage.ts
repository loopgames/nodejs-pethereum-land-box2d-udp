/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {ClientLobbyMessageTypes} from "../../../constant/lobby/ClientLobbyMessageTypes";
import {BaseLobbyRequestMessage} from "./BaseLobbyRequestMessage";
import {LobbyInputPayload} from "../LobbyInputPayload";

//#endregion

export class MoveLobbyRequestMessage
    extends BaseLobbyRequestMessage {
    //#region Variables

    public id!: string;

    public lobbyInputPayload!: LobbyInputPayload;

    //#endregion

    //#region Constructor

    constructor(
        lobbyMessageType: ClientLobbyMessageTypes,
        id: string,
        lobbyInputPayload: LobbyInputPayload) {
        super(lobbyMessageType);

        this.id = id;

        this.lobbyInputPayload = lobbyInputPayload;
    }

    //#endregion

    //#region Methods

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): MoveLobbyRequestMessage {
        const lobbyMessageType =
            serializer.readShort();

        const id =
            serializer.readString();

        const lobbyInputPayload =
            LobbyInputPayload.deserialize(
                serializer);

        const moveLobbyRequestMessage =
            new MoveLobbyRequestMessage(
                lobbyMessageType, id, lobbyInputPayload);

        return moveLobbyRequestMessage;
    }

    //#endregion

    //#endregion
}
