/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {ClientLobbyMessageTypes} from "../../../constant/lobby/ClientLobbyMessageTypes";

//#endregion

export class BaseLobbyRequestMessage {
    //#region Variables

    public lobbyMessageType!: ClientLobbyMessageTypes;

    //#endregion

    //#region Constructor

    constructor(
        lobbyMessageType: ClientLobbyMessageTypes) {
        this.lobbyMessageType = lobbyMessageType;
    }

    //#endregion

    //#region Methods

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): BaseLobbyRequestMessage {
        const lobbyMessageType =
            serializer.readShort();

        const baseLobbyRequestMessage =
            new BaseLobbyRequestMessage(
                lobbyMessageType);

        return baseLobbyRequestMessage;
    }

    //#endregion

    //#endregion
}
