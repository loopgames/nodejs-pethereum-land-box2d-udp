/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {ServerReturnCodes} from "../../../constant/server/ServerReturnCodes";
import {ServerLobbyMessageTypes} from "../../../constant/lobby/ServerLobbyMessageTypes";
import {BaseLobbyResponseMessage} from "./BaseLobbyResponseMessage";

//#endregion

export class LeaveLobbySelfResponseMessage
    extends BaseLobbyResponseMessage {
    //#region Variables

    public id!: string;

    //#endregion

    //#region Constructor

    constructor(
        returnCode: ServerReturnCodes,
        id: string) {
        super(returnCode);

        this.lobbyMessageType = ServerLobbyMessageTypes.LeaveSelf;

        this.id = id;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            Serializer.stringSize(
                this.id);

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeShort(
                this.returnCode);

        totalByteSize +=
            serializer.writeShort(
                this.lobbyMessageType);

        totalByteSize +=
            serializer.writeString(
                this.id);

        return totalByteSize;
    }

    //#endregion

    //#endregion
}
