/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {Pet} from "../../pet/Pet";
import {ClientLobbyMessageTypes} from "../../../constant/lobby/ClientLobbyMessageTypes";
import {BaseLobbyRequestMessage} from "./BaseLobbyRequestMessage";

//#endregion

export class ChangePetLobbyRequestMessage
    extends BaseLobbyRequestMessage {
    //#region Variables

    public id!: string;

    public pet!: Pet;

    //#endregion

    //#region Constructor

    constructor(
        lobbyMessageType: ClientLobbyMessageTypes,
        id: string,
        pet: Pet) {
        super(lobbyMessageType);

        this.id = id;

        this.pet = pet;
    }

    //#endregion

    //#region Methods

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): ChangePetLobbyRequestMessage {
        const lobbyMessageType =
            serializer.readShort();

        const id =
            serializer.readString();

        const pet =
            Pet.deserialize(
                serializer);

        const changePetLobbyRequestMessage =
            new ChangePetLobbyRequestMessage(
                lobbyMessageType,
                id,
                pet);

        return changePetLobbyRequestMessage;
    }

    //#endregion

    //#endregion
}
