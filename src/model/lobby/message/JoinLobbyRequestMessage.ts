/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {Pet} from "../../pet/Pet";
import {ClientLobbyMessageTypes} from "../../../constant/lobby/ClientLobbyMessageTypes";
import {BaseLobbyRequestMessage} from "./BaseLobbyRequestMessage";
import {LobbyClientPosition} from "../LobbyClientPosition";

//#endregion

export class JoinLobbyRequestMessage
    extends BaseLobbyRequestMessage {
    //#region Variables

    public token!: string;

    public pet!: Pet;

    public lobbyClientPosition!: LobbyClientPosition;

    //#endregion

    //#region Constructor

    constructor(
        lobbyMessageType: ClientLobbyMessageTypes,
        token: string,
        pet: Pet,
        lobbyClientPosition: LobbyClientPosition) {
        super(lobbyMessageType);

        this.token = token;

        this.pet = pet;

        this.lobbyClientPosition = lobbyClientPosition;
    }

    //#endregion

    //#region Methods

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): JoinLobbyRequestMessage {
        const lobbyMessageType =
            serializer.readShort();

        const token =
            serializer.readString();

        const pet =
            Pet.deserialize(
                serializer);

        const lobbyClientPosition =
            LobbyClientPosition.deserialize(
                serializer);

        const joinLobbyRequestMessage =
            new JoinLobbyRequestMessage(
                lobbyMessageType,
                token,
                pet,
                lobbyClientPosition);

        return joinLobbyRequestMessage;
    }

    //#endregion

    //#endregion
}
