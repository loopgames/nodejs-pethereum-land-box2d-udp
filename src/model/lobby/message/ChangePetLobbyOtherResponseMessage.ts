/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {ServerReturnCodes} from "../../../constant/server/ServerReturnCodes";
import {ServerLobbyMessageTypes} from "../../../constant/lobby/ServerLobbyMessageTypes";
import {BaseLobbyResponseMessage} from "./BaseLobbyResponseMessage";
import {LobbyClient} from "../LobbyClient";

//#endregion

export class ChangePetLobbyOtherResponseMessage
    extends BaseLobbyResponseMessage {
    //#region Variables

    public lobbyClient!: LobbyClient;

    //#endregion

    //#region Constructor

    constructor(
        returnCode: ServerReturnCodes,
        lobbyClient: LobbyClient) {
        super(returnCode);

        this.lobbyMessageType = ServerLobbyMessageTypes.ChangePetOther;

        this.lobbyClient = lobbyClient;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            this.lobbyClient.getTotalBufferSize();

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeShort(
                this.returnCode);

        totalByteSize +=
            serializer.writeShort(
                this.lobbyMessageType);

        totalByteSize +=
            this.lobbyClient.serialize(
                serializer);

        return totalByteSize;
    }

    //#endregion

    //#endregion
}
