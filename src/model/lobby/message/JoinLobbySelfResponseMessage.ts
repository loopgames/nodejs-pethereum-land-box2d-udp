/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../../serializer/Serializer";
import {ServerReturnCodes} from "../../../constant/server/ServerReturnCodes";
import {ServerLobbyMessageTypes} from "../../../constant/lobby/ServerLobbyMessageTypes";
import {BaseLobbyResponseMessage} from "./BaseLobbyResponseMessage";
import {LobbyClient} from "../LobbyClient";

//#endregion

export class JoinLobbySelfResponseMessage
    extends BaseLobbyResponseMessage {
    //#region Variables

    public selfLobbyClient!: LobbyClient;

    public otherLobbyClients!: LobbyClient[];

    //#endregion

    //#region Constructor

    constructor(
        returnCode: ServerReturnCodes,
        selfLobbyClient: LobbyClient,
        otherLobbyClients: LobbyClient[]) {
        super(returnCode);

        this.lobbyMessageType = ServerLobbyMessageTypes.JoinSelf;

        this.selfLobbyClient = selfLobbyClient;

        this.otherLobbyClients = otherLobbyClients;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            this.selfLobbyClient.getTotalBufferSize();

        totalBufferSize +=
            Serializer.shortSize;

        if (this.otherLobbyClients) {
            for (let i = 0; i < this.otherLobbyClients.length; i++) {
                const otherLobbyClient =
                    this.otherLobbyClients[i];
                if (!otherLobbyClient) {
                    continue;
                }

                totalBufferSize +=
                    otherLobbyClient.getTotalBufferSize();
            }
        }

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeShort(
                this.returnCode);

        totalByteSize +=
            serializer.writeShort(
                this.lobbyMessageType);

        totalByteSize +=
            this.selfLobbyClient.serialize(
                serializer);

        const otherLobbyClientsCount =
            this.otherLobbyClients ? this.otherLobbyClients.length : 0;
        totalByteSize +=
            serializer.writeShort(
                otherLobbyClientsCount);

        if (this.otherLobbyClients) {
            for (let i = 0; i < this.otherLobbyClients.length; i++) {
                const otherLobbyClient =
                    this.otherLobbyClients[i];
                if (!otherLobbyClient) {
                    continue;
                }

                totalByteSize +=
                    otherLobbyClient.serialize(
                        serializer);
            }
        }

        return totalByteSize;
    }

    //#endregion

    //#endregion
}
