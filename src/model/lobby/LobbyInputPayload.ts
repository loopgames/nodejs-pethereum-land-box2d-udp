/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";
import {LobbyClientInput} from "./LobbyClientInput";
import {LobbyClientPosition} from "./LobbyClientPosition";

//#endregion

export class LobbyInputPayload {
    //#region Variables

    public tick!: number;

    public lobbyClientInput!: LobbyClientInput;

    public lobbyClientPosition!: LobbyClientPosition;

    //#endregion

    //#region Constructor

    constructor(
        tick: number,
        lobbyClientInput: LobbyClientInput,
        lobbyClientPosition: LobbyClientPosition) {
        this.tick = tick;

        this.lobbyClientInput = lobbyClientInput;

        this.lobbyClientPosition = lobbyClientPosition;
    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.intSize;

        totalBufferSize +=
            this.lobbyClientInput.getTotalBufferSize();

        totalBufferSize +=
            this.lobbyClientPosition.getTotalBufferSize();

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeInt(
                this.tick);

        totalByteSize +=
            this.lobbyClientInput.serialize(
                serializer);

        totalByteSize +=
            this.lobbyClientPosition.serialize(
                serializer);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): LobbyInputPayload {
        const tick =
            serializer.readInt();

        const lobbyClientInput =
            LobbyClientInput.deserialize(
                serializer);

        const lobbyClientPosition =
            LobbyClientPosition.deserialize(
                serializer);

        const lobbyInputPayload =
            new LobbyInputPayload(
                tick,
                lobbyClientInput,
                lobbyClientPosition);

        return lobbyInputPayload;
    }

    //#endregion

    //#region To Class Methods

    public static toClass(
        lobbyInputPayloadObj: LobbyInputPayload): LobbyInputPayload {
        const tick =
            lobbyInputPayloadObj.tick;

        const lobbyClientInput =
            LobbyClientInput.toClass(
                lobbyInputPayloadObj.lobbyClientInput);

        const lobbyClientPosition =
            LobbyClientPosition.toClass(
                lobbyInputPayloadObj.lobbyClientPosition);

        const lobbyInputPayload =
            new LobbyInputPayload(
                tick,
                lobbyClientInput,
                lobbyClientPosition);

        return lobbyInputPayload;
    }

    //#endregion

    //#endregion
}
