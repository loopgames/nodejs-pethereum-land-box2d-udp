/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {A2SRulesResponsePacketKeyValue} from "./A2SRulesResponsePacketKeyValue";

//#endregion

export class A2SRulesData {
    //#region Variables

    public numRules!: number;

    public rules!: A2SRulesResponsePacketKeyValue[];

    //#endregion

    //#region Constructor

    constructor() {
        this.numRules = 0;

        this.rules = [];
    }

    //#endregion

    //#region Methods

    //#endregion
}
