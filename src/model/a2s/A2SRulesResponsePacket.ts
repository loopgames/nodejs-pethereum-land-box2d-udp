/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";
import {A2SRulesResponsePacketKeyValue} from "./A2SRulesResponsePacketKeyValue";

//#endregion

export class A2SRulesResponsePacket {
    //#region Variables

    public defaultHeader!: number;
    public responseHeader!: number;

    public numRules!: number;

    public rules!: A2SRulesResponsePacketKeyValue[];

    //#endregion

    //#region Constructor

    constructor() {

    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.intSize;
        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.shortSize;

        if (this.rules) {
            for (let i = 0; i < this.rules.length; i++) {
                const rule =
                    this.rules[i];
                if (!rule) {
                    continue;
                }

                totalBufferSize +=
                    rule.getTotalBufferSize();
            }
        }

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            serializer.writeInt(
                -1);
        totalBufferSize +=
            serializer.writeByte(
                this.responseHeader);

        totalBufferSize +=
            serializer.writeShort(
                this.numRules);

        if (this.rules) {
            for (let i = 0; i < this.rules.length; i++) {
                const rule =
                    this.rules[i];
                if (!rule) {
                    continue;
                }

                totalBufferSize +=
                    rule.serialize(
                        serializer);
            }
        }

        return 0;
    }

    //#endregion

    //#endregion
}
