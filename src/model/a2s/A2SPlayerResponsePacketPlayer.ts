/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";

//#endregion

export class A2SPlayerResponsePacketPlayer {
    //#region Variables

    public index!: number;

    public playerName!: string;

    public score!: number;

    public duration!: number;

    //#endregion

    //#region Constructor

    constructor() {

    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.stringSize(
                this.playerName);

        totalBufferSize +=
            Serializer.intSize;

        totalBufferSize +=
            Serializer.floatSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeByte(
                this.index);

        totalByteSize +=
            serializer.writeStringA2S(
                this.playerName);

        totalByteSize +=
            serializer.writeInt(
                this.score);

        totalByteSize +=
            serializer.writeFloat(
                this.duration);

        return totalByteSize;
    }

    //#endregion

    //#endregion
}
