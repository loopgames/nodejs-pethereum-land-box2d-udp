/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";
import {A2SPlayerResponsePacketPlayer} from "./A2SPlayerResponsePacketPlayer";

//#endregion

export class A2SPlayerResponsePacket {
    //#region Variables

    public defaultHeader!: number;
    public responseHeader!: number;

    public numPlayers!: number;

    public players!: A2SPlayerResponsePacketPlayer[];

    //#endregion

    //#region Constructor

    constructor() {

    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.intSize;
        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        if (this.players) {
            for (let i = 0; i < this.players.length; i++) {
                const player =
                    this.players[i];
                if (!player) {
                    continue;
                }

                totalBufferSize +=
                    player.getTotalBufferSize();
            }
        }

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeInt(
                -1);
        totalByteSize +=
            serializer.writeByte(
                this.responseHeader);

        totalByteSize +=
            serializer.writeByte(
                this.numPlayers);

        if (this.players) {
            for (let i = 0; i < this.players.length; i++) {
                const player =
                    this.players[i];
                if (!player) {
                    continue;
                }

                totalByteSize +=
                    player.serialize(
                        serializer);
            }
        }

        return totalByteSize;
    }

    //#endregion

    //#endregion
}
