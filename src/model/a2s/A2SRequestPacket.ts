/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";

//#endregion

export class A2SRequestPacket {
    //#region Variables

    public defaultHeader!: number;

    public requestType!: number;

    public challengeNumber!: number;

    //#endregion

    //#region Constructor

    constructor() {

    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.intSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.intSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeInt(
                -1);

        totalByteSize +=
            serializer.writeByte(
                this.requestType);

        totalByteSize +=
            serializer.writeInt(
                this.challengeNumber);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): A2SRequestPacket {
        const a2sRequestPacket =
            new A2SRequestPacket();

        a2sRequestPacket.defaultHeader =
            serializer.readInt();
        a2sRequestPacket.requestType =
            serializer.readByte();
        a2sRequestPacket.challengeNumber =
            serializer.readInt();

        return a2sRequestPacket;
    }

    //#endregion

    //#endregion
}
