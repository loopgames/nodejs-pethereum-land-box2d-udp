/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";

//#endregion

export class A2SChallengeResponsePacket {
    //#region Variables

    public defaultHeader!: number;
    public responseHeader!: number;

    public challengeNumber!: number;

    //#endregion

    //#region Constructor

    constructor() {

    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.intSize;
        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.intSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeInt(
                -1);
        totalByteSize +=
            serializer.writeByte(
                this.responseHeader);

        totalByteSize +=
            serializer.writeInt(
                this.challengeNumber);

        return totalByteSize;
    }

    //#endregion

    //#region Deserialize Methods

    public static deserialize(
        serializer: Serializer): A2SChallengeResponsePacket {
        const a2sChallengeResponsePacket =
            new A2SChallengeResponsePacket();

        a2sChallengeResponsePacket.defaultHeader =
            serializer.readInt();
        a2sChallengeResponsePacket.responseHeader =
            serializer.readByte();

        a2sChallengeResponsePacket.challengeNumber =
            serializer.readInt();

        return a2sChallengeResponsePacket;
    }

    //#endregion

    //#endregion
}
