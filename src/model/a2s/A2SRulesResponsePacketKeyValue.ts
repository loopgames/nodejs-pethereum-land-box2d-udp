/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";

//#endregion

export class A2SRulesResponsePacketKeyValue {
    //#region Variables

    public ruleName!: string;
    public ruleValue!: string;

    //#endregion

    //#region Constructor

    constructor() {

    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.stringSize(
                this.ruleName);

        totalBufferSize +=
            Serializer.stringSize(
                this.ruleValue);

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeStringA2S(
                this.ruleName);
        totalByteSize +=
            serializer.writeStringA2S(
                this.ruleValue);

        return totalByteSize;
    }

    //#endregion

    //#endregion
}
