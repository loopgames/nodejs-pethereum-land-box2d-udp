/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

//#endregion

export class A2SServerData {
    //#region Variables

    public protocol!: number;

    public serverName!: string;
    public serverMap!: string;

    public folder!: string;

    public gameName!: string;

    public steamId!: number;

    public playerCount!: number;
    public maxPlayers!: number;
    public botCount!: number;

    public serverType!: number;

    public visibility!: number;

    public valveAntiCheat!: number;

    public version!: string;

    public extraDataFlag!: number;

    //#endregion

    //#region Constructor

    constructor() {
        this.protocol = 0;

        this.serverName = "";
        this.serverMap = "";

        this.folder = "";

        this.gameName = "";

        this.steamId = 0;

        this.playerCount = 0;
        this.maxPlayers = Number.MAX_SAFE_INTEGER;
        this.botCount = 0;

        this.serverType = "d".charCodeAt(0);

        this.visibility = 0;

        this.valveAntiCheat = 0;

        this.version = "001";

        this.extraDataFlag = 0;
    }

    //#endregion

    //#region Methods

    //#endregion
}
