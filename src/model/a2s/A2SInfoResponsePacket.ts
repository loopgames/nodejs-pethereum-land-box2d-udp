/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {Serializer} from "../serializer/Serializer";

//#endregion

export class A2SInfoResponsePacket {
    //#region Variables

    public defaultHeader!: number;
    public responseHeader!: number;

    public protocol!: number;

    public serverName!: string;
    public serverMap!: string;

    public folder!: string;

    public gameName!: string;

    public steamId!: number;

    public playerCount!: number;
    public maxPlayers!: number;
    public botCount!: number;

    public serverType!: number;

    public environment!: number;

    public visibility!: number;

    public valveAntiCheat!: number;

    public version!: string;

    public extraDataFlag!: number;

    //#endregion

    //#region Constructor

    constructor() {

    }

    //#endregion

    //#region Methods

    //#region Get Total Buffer Size Methods

    public getTotalBufferSize(): number {
        let totalBufferSize = 0;

        totalBufferSize +=
            Serializer.intSize;
        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.stringSize(
                this.serverName);
        totalBufferSize +=
            Serializer.stringSize(
                this.serverMap);

        totalBufferSize +=
            Serializer.stringSize(
                this.folder);

        totalBufferSize +=
            Serializer.stringSize(
                this.gameName);

        totalBufferSize +=
            Serializer.shortSize;

        totalBufferSize +=
            Serializer.byteSize;
        totalBufferSize +=
            Serializer.byteSize;
        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.byteSize;

        totalBufferSize +=
            Serializer.stringSize(
                this.version);

        totalBufferSize +=
            Serializer.byteSize;

        return totalBufferSize;
    }

    //#endregion

    //#region Serialize Methods

    public serialize(
        serializer: Serializer): number {
        let totalByteSize = 0;

        totalByteSize +=
            serializer.writeInt(
                -1);
        totalByteSize +=
            serializer.writeByte(
                this.responseHeader);

        totalByteSize +=
            serializer.writeByte(
                this.protocol);

        totalByteSize +=
            serializer.writeStringA2S(
                this.serverName);
        totalByteSize +=
            serializer.writeStringA2S(
                this.serverMap);

        totalByteSize +=
            serializer.writeStringA2S(
                this.folder);

        totalByteSize +=
            serializer.writeStringA2S(
                this.gameName);

        totalByteSize +=
            serializer.writeShort(
                this.steamId);

        totalByteSize +=
            serializer.writeByte(
                this.playerCount);
        totalByteSize +=
            serializer.writeByte(
                this.maxPlayers);
        totalByteSize +=
            serializer.writeByte(
                this.botCount);

        totalByteSize +=
            serializer.writeByte(
                this.serverType);

        totalByteSize +=
            serializer.writeByte(
                this.environment);

        totalByteSize +=
            serializer.writeByte(
                this.visibility);

        totalByteSize +=
            serializer.writeByte(
                this.valveAntiCheat);

        totalByteSize +=
            serializer.writeStringA2S(
                this.version);

        totalByteSize +=
            serializer.writeByte(
                this.extraDataFlag);

        return totalByteSize;
    }

    //#endregion

    //#endregion
}
