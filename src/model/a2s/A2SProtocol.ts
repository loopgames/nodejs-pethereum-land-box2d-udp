/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";

import {LogManager} from "../../manager/LogManager";
import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {Serializer} from "../serializer/Serializer";
import {SerializationMode} from "../../constant/serializer/SerializationMode";
import {A2SRequestPacket} from "./A2SRequestPacket";
import {A2SInfoResponsePacket} from "./A2SInfoResponsePacket";
import {A2SPlayerResponsePacket} from "./A2SPlayerResponsePacket";
import {A2SRulesResponsePacket} from "./A2SRulesResponsePacket";
import {A2SChallengeResponsePacket} from "./A2SChallengeResponsePacket";
import {ReferenceController} from "../../controller/ReferenceController";

//#endregion

export class A2SProtocol {
    //#region Variables

    private readonly kA2SChallengeNumber!: number;

    private readonly a2sInfoRequestType!: number;
    private readonly a2sPlayerInfoRequestType!: number;
    private readonly a2sRuleRequestType!: number;

    private readonly logManager!: LogManager;

    //#endregion

    //#region Getters - Setters


    //#endregion

    //#region Constructor

    constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);

        this.kA2SChallengeNumber = 427;

        this.a2sInfoRequestType =
            CommonLogicUtil.convertCharToByte("T");
        this.a2sPlayerInfoRequestType =
            CommonLogicUtil.convertCharToByte("U");
        this.a2sRuleRequestType =
            CommonLogicUtil.convertCharToByte("V");
    }

    //#endregion

    //#region Methods

    //#region Receive Data Methods

    public receiveData(
        data: Uint8Array,
        clientId: string,
        serverId: number): Uint8Array | null {
        try {
            // Don't proceed if we've received a packet thats too small
            if (data.length < 9) return null;

            // Parse packet header
            const readSerializer =
                new Serializer(
                    SerializationMode.Read, data);
            const requestPacket =
                A2SRequestPacket.deserialize(
                    readSerializer);

            // The packet is a challenge request
            switch (requestPacket.requestType) {
                case this.a2sInfoRequestType:
                    return this.sendA2SInfoPacket(serverId);
                case this.a2sPlayerInfoRequestType:
                    if (requestPacket.challengeNumber === -1) {
                        return this.issueA2SChallengeNumber();
                    } else if (requestPacket.challengeNumber ===
                        this.kA2SChallengeNumber) {
                        return this.sendA2SPlayerInfoPacket(serverId);
                    } else {
                        return null;
                    }
                case this.a2sRuleRequestType:
                    if (requestPacket.challengeNumber === -1) {
                        return this.issueA2SChallengeNumber();
                    } else if (requestPacket.challengeNumber ===
                        this.kA2SChallengeNumber) {
                        return this.sendA2SRulesPacket(serverId);
                    } else {
                        return null;
                    }
                default:
                    this.logManager.showWarn({
                        warning: util.format("Unsupported/Unimplemented A2S request type received!")
                    });

                    return null;
            }
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Receive A2S data error! Err: %s",
                    error)
            });

            return null;
        }
    }

    //#endregion

    //#region Send A2S Info Packet Methods

    private sendA2SInfoPacket(
        serverId: number): Uint8Array | null {
        const serverQueryProtocolData =
            ReferenceController.queryProtocolController.getServerQueryProtocolData(
                serverId);

        if (!serverQueryProtocolData) {
            return null;
        }

        const a2SServerInfo =
            serverQueryProtocolData.a2sServerInfo;

        const a2sInfoResponsePacket =
            new A2SInfoResponsePacket();

        a2sInfoResponsePacket.defaultHeader = -1;
        a2sInfoResponsePacket.responseHeader = CommonLogicUtil.convertCharToByte("I");
        a2sInfoResponsePacket.protocol = a2SServerInfo.protocol;
        a2sInfoResponsePacket.serverName = a2SServerInfo.serverName;
        a2sInfoResponsePacket.serverMap = a2SServerInfo.serverMap;
        a2sInfoResponsePacket.folder = a2SServerInfo.folder;
        a2sInfoResponsePacket.gameName = a2SServerInfo.gameName;
        a2sInfoResponsePacket.steamId = a2SServerInfo.steamId;
        a2sInfoResponsePacket.playerCount = a2SServerInfo.playerCount;
        a2sInfoResponsePacket.maxPlayers = a2SServerInfo.maxPlayers;
        a2sInfoResponsePacket.botCount = a2SServerInfo.botCount;
        a2sInfoResponsePacket.serverType = a2SServerInfo.serverType;
        a2sInfoResponsePacket.environment = CommonLogicUtil.convertCharToByte("l");
        a2sInfoResponsePacket.visibility = a2SServerInfo.visibility; // All servers on multiplay are considered public
        a2sInfoResponsePacket.valveAntiCheat = a2SServerInfo.valveAntiCheat;
        a2sInfoResponsePacket.version = a2SServerInfo.version;
        a2sInfoResponsePacket.extraDataFlag = a2SServerInfo.extraDataFlag;

        const a2sInfoResponseBuffer =
            new Uint8Array(
                a2sInfoResponsePacket.getTotalBufferSize() + 1);
        const a2sInfoWriteSerializer =
            new Serializer(
                SerializationMode.Write, a2sInfoResponseBuffer);

        a2sInfoResponsePacket.serialize(
            a2sInfoWriteSerializer);

        return a2sInfoResponseBuffer;
    }

    //#endregion

    //#region Send A2S Player Packet Methods

    private sendA2SPlayerInfoPacket(
        serverId: number): Uint8Array | null {
        const serverQueryProtocolData =
            ReferenceController.queryProtocolController.getServerQueryProtocolData(
                serverId);

        if (!serverQueryProtocolData) {
            return null;
        }

        const a2SPlayerInfo =
            serverQueryProtocolData.a2sPlayerInfo;

        const a2sPlayerInfoResponsePacket =
            new A2SPlayerResponsePacket();

        a2sPlayerInfoResponsePacket.defaultHeader = -1;
        a2sPlayerInfoResponsePacket.responseHeader = CommonLogicUtil.convertCharToByte("D");
        a2sPlayerInfoResponsePacket.numPlayers = a2SPlayerInfo.numPlayers;
        a2sPlayerInfoResponsePacket.players = a2SPlayerInfo.players;

        const a2SPlayerInfoResponseBuffer =
            new Uint8Array(
                a2sPlayerInfoResponsePacket.getTotalBufferSize() + 1);
        const a2SPlayerInfoWriteSerializer =
            new Serializer(
                SerializationMode.Write, a2SPlayerInfoResponseBuffer);

        a2sPlayerInfoResponsePacket.serialize(
            a2SPlayerInfoWriteSerializer);

        return a2SPlayerInfoResponseBuffer;
    }

    //#endregion

    //#region Send A2S Rules Packet Methods

    private sendA2SRulesPacket(
        serverId: number): Uint8Array | null {
        const serverQueryProtocolData =
            ReferenceController.queryProtocolController.getServerQueryProtocolData(
                serverId);

        if (!serverQueryProtocolData) {
            return null;
        }

        const a2SServerRules =
            serverQueryProtocolData.a2sServerRules;

        const a2sServerRulesResponsePacket =
            new A2SRulesResponsePacket();

        a2sServerRulesResponsePacket.defaultHeader = -1;
        a2sServerRulesResponsePacket.responseHeader = CommonLogicUtil.convertCharToByte("E");
        a2sServerRulesResponsePacket.numRules = a2SServerRules.numRules;
        a2sServerRulesResponsePacket.rules = a2SServerRules.rules;

        const a2sServerRulesResponseBuffer =
            new Uint8Array(
                a2sServerRulesResponsePacket.getTotalBufferSize() + 1);
        const a2SPlayerInfoWriteSerializer =
            new Serializer(
                SerializationMode.Write, a2sServerRulesResponseBuffer);

        a2sServerRulesResponsePacket.serialize(
            a2SPlayerInfoWriteSerializer);

        return a2sServerRulesResponseBuffer;
    }

    //#endregion

    //#region Issue A2S Challenge Number Methods

    private issueA2SChallengeNumber(): Uint8Array | null {
        const a2sChallengeResponsePacket =
            new A2SChallengeResponsePacket();

        a2sChallengeResponsePacket.defaultHeader = -1;
        a2sChallengeResponsePacket.responseHeader = CommonLogicUtil.convertCharToByte("A");
        a2sChallengeResponsePacket.challengeNumber = this.kA2SChallengeNumber;

        const issueA2SChallengeNumberResponseBuffer =
            new Uint8Array(
                a2sChallengeResponsePacket.getTotalBufferSize() + 1);
        const issueA2SChallengeNumberWriteSerializer =
            new Serializer(
                SerializationMode.Write, issueA2SChallengeNumberResponseBuffer);

        a2sChallengeResponsePacket.serialize(
            issueA2SChallengeNumberWriteSerializer);

        return issueA2SChallengeNumberResponseBuffer;
    }

    //#endregion

    //#endregion
}
