/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {A2SPlayerResponsePacketPlayer} from "./A2SPlayerResponsePacketPlayer";

//#endregion

export class A2SPlayerData {
    //#region Variables

    public numPlayers!: number;

    public players!: A2SPlayerResponsePacketPlayer[];

    //#endregion

    //#region Constructor

    constructor() {
        this.numPlayers = 0;

        this.players = [];
    }

    //#endregion

    //#region Methods

    //#endregion
}
