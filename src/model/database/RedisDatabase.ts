/**
 * Created by hulusionder on 06/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import IORedis, {Redis, KeyType, RedisOptions, ValueType} from "ioredis";

import {LogManager} from "../../manager/LogManager";
import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {RedisDatabaseResponseInterface} from "../../interface/redis/RedisDatabaseResponseInterface";

//#endregion

export class RedisDatabase {
    //#region Variables

    private _client!: Redis;

    private readonly logManager!: LogManager;

    //#endregion

    //#region Getters - Setters

    get client(): Redis {
        return this._client;
    }

    //#endregion

    //#region Constructor

    constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(
        host?: string,
        options?: RedisOptions): Promise<void> {
        try {
            this._client = new IORedis(host, options);

            await this.connect();
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            throw new Error(
                util.format("Redis database initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Connect - Close Database

    private async connect(): Promise<void> {
        this._client.on("error", (error: string) => {
            this.logManager.showError({
                error: util.format("Redis database error! Err: %s",
                    error)
            });
        });

        await this._client.connect();
    }

    private async close(): Promise<void> {
        await this._client.disconnect();
    }

    //#endregion

    //#region Database Methods

    //#region Health Check Methods

    public async healthCheck(): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.ping();

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#region Select Database Methods

    public async selectDatabase(
        index: number): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.select(
                    index);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#region Flush Database Methods

    public async flushDatabase(): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.flushdb();

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async flushAllDatabases(): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.flushall();

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#region Rename Methods

    public async renameKey(
        key: KeyType,
        newKey: KeyType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.renamenx(
                    key, newKey);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#region Expire Methods

    public async expireKeyAt(
        key: KeyType,
        timestamp: number): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.expireat(
                    key, timestamp);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getKeyRemainingTime(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.ttl(
                    key);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#region Key Value Methods

    public async isKeyExist(
        keys: KeyType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.exists(
                    ...keys);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async setKeyValue(
        key: KeyType,
        value: KeyType,
        expiryMode?: string | any[] | undefined,
        time?: string | number | undefined,
        setMode?: string | number | undefined): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.set(
                    key, value, expiryMode, time);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getKeyValue(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.get(
                    key);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async scan(
        cursor: number | string,
        matchOption: "match" | "MATCH",
        pattern: string,
        countOption: "count" | "COUNT",
        count: number): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.scan(
                    cursor, matchOption, pattern, countOption, count);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async deleteKey(
        keys: KeyType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.del(
                    ...keys);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#region List Methods

    public async getListLength(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.llen(
                    key);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getListElement(
        key: KeyType,
        index: number): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lindex(
                    key, index);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async setListElement(
        key: KeyType,
        index: number,
        value: ValueType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lset(
                    key, index, value);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async insertListElementToHead(
        key: KeyType,
        element: ValueType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lpush(
                    key, element);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async insertListElementsToHead(
        key: KeyType,
        elements: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lpush(
                    key, ...elements);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async insertListElementToHeadOnlyIfKeyExist(
        key: KeyType,
        element: ValueType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lpushx(
                    key, element);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async insertListElementsToHeadOnlyIfKeyExist(
        key: KeyType,
        elements: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lpushx(
                    key, ...elements);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async insertListElementToTail(
        key: KeyType,
        element: ValueType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.rpush(
                    key, element);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async insertListElementsToTail(
        key: KeyType,
        elements: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.rpush(
                    key, ...elements);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async insertListElementToTailOnlyIfKeyExist(
        key: KeyType,
        element: ValueType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.rpushx(
                    key, element);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async insertListElementsToTailOnlyIfKeyExist(
        key: KeyType,
        elements: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.rpushx(
                    key, elements);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async insertListElementWithPivot(
        key: KeyType,
        direction: "BEFORE" | "AFTER",
        pivot: string,
        value: ValueType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.linsert(
                    key, direction, pivot, value);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async removeAndGetFirstListElement(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lpop(
                    key);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async removeAndGetFirstListElements(
        key: KeyType,
        count: number): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lpop(
                    key, count);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async removeAndGetLastListElement(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.rpop(
                    key);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async removeAndGetLastListElements(
        key: KeyType,
        count: number): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.rpop(
                    key, count);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getListElementsInRange(
        key: KeyType,
        start: number,
        stop: number): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lrange(
                    key, start, stop);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async removeListElement(
        key: KeyType,
        count: number,
        value: ValueType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lrem(
                    key, count, value);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async trimListElements(
        key: KeyType,
        start: number,
        stop: number): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.ltrim(
                    key, start, stop);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async findListElementIndex(
        key: KeyType,
        value: ValueType,
        rank?: number,
        count?: number,
        maxlen?: number): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.lpos(
                    key, value, rank, count, maxlen);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#region Hash Methods

    public async isHashFieldExists(
        key: KeyType,
        field: string): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.hexists(
                    key, field);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getHashFields(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.hkeys(
                    key);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getHashValue(
        key: KeyType,
        field: string): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.hget(
                    key, field);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getHashValues(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.hvals(
                    key);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getHashValuesWithGivenKeys(
        key: KeyType,
        args: KeyType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.hmget(
                    key, args);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getAllHashValues(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.hgetall(
                    key);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async setHashValue(
        key: KeyType,
        args: ValueType[] | {[p: string]: ValueType} | Map<string, ValueType>): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.hset(
                    key, args);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async scanHash(
        key: KeyType,
        args: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.hscan(
                    key, ...args);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async deleteHashKey(
        key: KeyType,
        args: KeyType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.hdel(
                    key, ...args);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#region Sorted Methods

    public async getSortedMembers(
        key: KeyType,
        args: KeyType | number[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.sort(
                    key, ...args);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async setSortedMember(
        key: KeyType,
        args: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.sadd(
                    key, ...args);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getSortedMemberWithScore(
        key: KeyType,
        member: string): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.zscore(
                    key, member);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async setSortedMemberWithScore(
        key: KeyType,
        args: (KeyType | number)[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.zadd(
                    key, args);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getSortedKeyCount(
        key: KeyType): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.zcard(
                    key);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getSortedMembersInRangeByAscendingOrder(
        key: KeyType,
        start: number,
        stop: number,
        withScores: "WITHSCORES"): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.zrange(
                    key, start, stop, withScores);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getSortedMembersInRangeByDescendingOrder(
        key: KeyType,
        start: number,
        stop: number,
        withScores: "WITHSCORES"): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.zrevrange(
                    key, start, stop, withScores);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getRankOfMemberByAscendingOrder(
        key: KeyType,
        member: string): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.zrank(
                    key, member);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getRankOfMemberByDescendingOrder(
        key: KeyType,
        member: string): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.zrevrank(
                    key, member);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async getScoreOfMember(
        key: KeyType,
        member: string): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.zscore(
                    key, member);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    public async deleteSortedMember(
        key: KeyType,
        args: ValueType[]): Promise<RedisDatabaseResponseInterface> {
        try {
            const result =
                await this._client.zrem(
                    key, ...args);

            return {
                isSuccess: true,
                result
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#endregion

    //#endregion
}
