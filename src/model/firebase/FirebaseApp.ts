/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import admin from "firebase-admin";

import {CommonLogicUtil} from "../../util/CommonLogicUtil";

//#endregion

export class FirebaseApp {
    //#region Variables

    private _admin!: admin.app.App;

    //#endregion

    //#region Getters - Setters

    get admin(): admin.app.App {
        return this._admin;
    }

    //#endregion

    //#region Constructor

    constructor() {

    }

    //#endregion

    //#region Methods

    //#region Init

    public init(
        options?: admin.AppOptions | undefined,
        name?: string | undefined): void {
        try {
            this.connect(options, name);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            throw new Error(
                util.format("Firestore app initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Connect Firebase

    private connect(
        options?: admin.AppOptions | undefined,
        name?: string | undefined): void {
        this._admin = admin.initializeApp(
            options, name);
    }

    //#endregion

    //#endregion
}
