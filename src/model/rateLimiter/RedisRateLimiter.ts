/**
 * Created by hulusionder on 06/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import {IRateLimiterStoreOptions, RateLimiterRedis} from "rate-limiter-flexible";

import {LogManager} from "../../manager/LogManager";
import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {RedisRateLimiterResponseInterface} from "../../interface/rateLimiter/RedisRateLimiterResponseInterface";

//#endregion

export class RedisRateLimiter {
    //#region Variables

    private _rateLimiterRedis!: RateLimiterRedis;

    private readonly logManager!: LogManager;

    //#endregion

    //#region Getters - Setters

    get rateLimiterRedis(): RateLimiterRedis {
        return this._rateLimiterRedis;
    }

    //#endregion

    //#region Constructor

    constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);
    }

    //#endregion

    //#region Methods

    //#region Init

    public init(
        options: IRateLimiterStoreOptions): void {
        try {
            this._rateLimiterRedis = new RateLimiterRedis(options);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            throw new Error(
                util.format("Redis rate limiter initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Consume Methods

    public async consume(
        remoteAddress: string,
        pointsToConsume?: number | undefined): Promise<RedisRateLimiterResponseInterface> {
        try {
            const rateLimiterConsumeResponse =
                await this._rateLimiterRedis.consume(
                    remoteAddress, pointsToConsume);

            return {
                isSuccess: true,
                result: rateLimiterConsumeResponse
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#endregion
}
