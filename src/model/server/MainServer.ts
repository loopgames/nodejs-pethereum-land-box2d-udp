/**
 * Created by hulusionder on 29/09/2021.
 */

"use strict";

//#region Imports

import fs from "fs";
import path from "path";
import util from "util";
import http from "http";
import https from "https";
import express, {Express, Router} from "express";

import {LogManager} from "../../manager/LogManager";
import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {CommandLineHelper} from "../../helper/command/CommandLineHelper";
import {LobbyServer} from "./LobbyServer";

//#endregion

//#region Variables

//#region Non-Lazy Initialization

const terminateSignalArr = [
    "SIGINT",
    "SIGTERM"
];
const insignificantSignalArr = [
    "SIGPIPE"
];

//#endregion

//#region Lazy Initialization

//#endregion

//#endregion

export class MainServer {
    //#region Variables

    private _express!: Express;
    private _httpServer!: http.Server;
    private _httpsServer!: https.Server;

    private readonly logManager!: LogManager;

    //#endregion

    //#region Constructor

    constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);
    }

    //#endregion

    //#region Getters - Setters

    get express(): Router {
        return this._express;
    }

    get httpServer(): http.Server {
        return this._httpServer;
    }

    get httpsServer(): https.Server {
        return this._httpsServer;
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Main server initialize start!")
            });

            const portCommandLineArgument =
                parseInt(CommandLineHelper.instance.getCommandLineArgument("HTTP_PORT"));

            const httpPort =
                portCommandLineArgument || 8080;
            const httpsPort =
                portCommandLineArgument + 1 || 8081;
            const serverSslPrivateKeyPassword =
                process.env.SERVER_SSL_PRIVATE_KEY_PASSWORD || "";

            const serverKeyPemPath =
                path.join(__dirname, "../../../ssl/server/key.pem");
            const serverCertPemPath =
                path.join(__dirname, "../../../ssl/server/cert.pem");

            const serverSslPrivateKey =
                fs.readFileSync(serverKeyPemPath, "utf8");
            const serverSslPublicKey =
                fs.readFileSync(serverCertPemPath, "utf8");

            const serverSslOptions = {
                key: serverSslPrivateKey,
                cert: serverSslPublicKey,
                passphrase: serverSslPrivateKeyPassword
            };

            this._express = express();

            this.prepareRouters();

            await this.startServer(
                httpPort, httpsPort,
                serverSslOptions);

            this.logManager.showLog({
                log: util.format("Main server initialize success! Port: %s",
                    httpPort)
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Main server initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Main server initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Prepare Routers

    private prepareRouters(): void {
        const lobbyServer = new LobbyServer();
        lobbyServer.init();
        this._express.use("/lobby",
            lobbyServer.express);
    }

    //#endregion

    //#region Start - Stop Server

    private async startServer(
        httpPort: string | number,
        httpsPort: string | number,
        options: https.ServerOptions): Promise<void> {
        process.on("exit", (exitCode) => this.stopServer(exitCode));

        terminateSignalArr.forEach(signal => {
            process.on(signal, () => this.receiveTerminateSignal(signal));
        });
        insignificantSignalArr.forEach(signal => {
            process.on(signal, () => this.receiveInsignificantSignal(signal));
        });

        this._httpServer = http.createServer(
            this._express);
        this._httpServer.listen(httpPort);

        this._httpsServer = https.createServer(
            options, this._express);
        this._httpsServer.listen(httpsPort);
    }

    private stopServer(
        exitCode: number): void {
        try {
            this.logManager.showError({
                error: util.format("Main server stop! Time: %s - Exit code: %s",
                    new Date(Date.now()), exitCode)
            });
        } catch (exception) {
            this.logManager.showError({
                error: util.format("Main server stop exception! Exception: %s",
                    exception)
            });
        }
    }

    private receiveTerminateSignal(
        signal: string): void {
        try {
            this.logManager.showError({
                error: util.format("Main server terminate signal received! Time: %s - Signal: %s",
                    new Date(Date.now()), signal)
            });

            process.exit(0);
        } catch (exception) {
            this.logManager.showError({
                error: util.format("Main server terminate signal receive exception! Exception: %s",
                    exception)
            });

            process.exit(1);
        }
    }

    private receiveInsignificantSignal(
        signal: string): void {
        try {
            this.logManager.showError({
                error: util.format("Main server insignificant signal received! Time: %s - Signal: %s",
                    new Date(Date.now()), signal)
            });
        } catch (exception) {
            this.logManager.showError({
                error: util.format("Main server insignificant signal receive exception! Exception: %s",
                    exception)
            });
        }
    }

    //#endregion

    //#endregion
}
