/**
 * Created by hulusionder on 29/09/2021.
 */

"use strict";

//#region Imports

import util from "util";
import dgram, {Socket, SocketOptions} from 'dgram';

import {LogManager} from "../../manager/LogManager";
import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {UdpConstants} from "../../constant/udp/UdpConstants";
import {EventHelper} from "../../helper/event/EventHelper";
import {CommandLineHelper} from "../../helper/command/CommandLineHelper";

//#endregion

//#region Variables

//#region Non-Lazy Initialization

//#endregion

//#region Lazy Initialization

//#endregion

//#endregion

export class UdpServer {
    //#region Variables

    private _socket!: Socket;

    private readonly logManager!: LogManager;

    //#endregion

    //#region Constructor

    constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);
    }

    //#endregion

    //#region Getters - Setters

    get socket(): Socket {
        return this._socket;
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Udp server initialize start!")
            });

            const hostCommandLineArgument =
                CommandLineHelper.instance.getCommandLineArgument("UDP_HOST");
            const portCommandLineArgument =
                parseInt(CommandLineHelper.instance.getCommandLineArgument("UDP_PORT"));

            const host =
                hostCommandLineArgument || "0.0.0.0";
            const port =
                portCommandLineArgument || 9000;
            const portInt =
                parseInt(port.toString());

            const socketOptions: SocketOptions = {
                type: "udp4",
                reuseAddr: true
            };

            await this.startServer(
                host, portInt, socketOptions);

            this.logManager.showLog({
                log: util.format("Udp server initialize success! Port: %s",
                    port)
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Udp server initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Udp server initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Start - Stop Server

    private async startServer(
        host: string,
        port: number,
        options: SocketOptions): Promise<void> {
        this._socket =
            dgram.createSocket(options);
        this._socket.bind({port, exclusive: true});

        this._socket.addListener(UdpConstants.SOCKET_PREDEFINED_EVENTS_CLOSE,
            () => {
                // this.logManager.showLog({
                //     log: util.format("Udp server close!")
                // });

                EventHelper.instance.emit(UdpConstants.UDP_SERVER_EVENTS_CLOSE);
            });

        this._socket.addListener(UdpConstants.SOCKET_PREDEFINED_EVENTS_CONNECT,
            () => {
                // this.logManager.showLog({
                //     log: util.format("Udp server connect!")
                // });

                EventHelper.instance.emit(UdpConstants.UDP_SERVER_EVENTS_CONNECT);
            });

        this._socket.addListener(UdpConstants.SOCKET_PREDEFINED_EVENTS_ERROR,
            (error) => {
                // this.logManager.showLog({
                //     log: util.format("Udp server error! Err: %s", error)
                // });

                EventHelper.instance.emit(UdpConstants.UDP_SERVER_EVENTS_ERROR, error);

                this.stopServer(error);
            });

        this._socket.addListener(UdpConstants.SOCKET_PREDEFINED_EVENTS_LISTENING,
            () => {
                const addressInfo = this._socket.address();
                this.logManager.showLog({
                    log: util.format("Udp server listen! Address: %s - Port: %s",
                        addressInfo.address, addressInfo.port)
                });

                EventHelper.instance.emit(UdpConstants.UDP_SERVER_EVENTS_LISTENING, this._socket);
            });

        this._socket.addListener(UdpConstants.SOCKET_PREDEFINED_EVENTS_MESSAGE,
            (message, senderInfo) => {
                // this.logManager.showLog({
                //     log: util.format("Udp server got message! Message: %s - Address: %s - Port: %s",
                //         message, senderInfo.address, senderInfo.port)
                // });

                EventHelper.instance.emit(UdpConstants.UDP_SERVER_EVENTS_MESSAGE, message, senderInfo);
            });
    }

    private stopServer(err?: string): void {
        try {
            this.logManager.showError({
                error: util.format("Udp server terminate! Error: %s",
                    err)
            });

            this._socket.removeAllListeners();

            this._socket.close();
        } catch (exception) {

        } finally {
            this.logManager.showError({
                error: util.format("Udp server stop! Time: %s",
                    new Date(Date.now()))
            });
        }
    }

    //#endregion

    //#endregion
}
