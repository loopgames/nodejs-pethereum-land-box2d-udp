/**
 * Created by hulusionder on 29/09/2021.
 */

"use strict";

//#region Imports

import util from "util";
import http from "http";
import express, {Router} from "express";
import helmet from "helmet";
import cors from "cors";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import {Server, ServerOptions, Namespace} from "socket.io";

import {LogManager} from "../../manager/LogManager";
import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {SocketConstants} from "../../constant/socket/SocketConstants";
import {EventHelper} from "../../helper/event/EventHelper";
import {RequestHelper} from "../../helper/request/RequestHelper";
import {RedisDatabase} from "../database/RedisDatabase";
import {ReferenceController} from "../../controller/ReferenceController";

//#endregion

export class SocketServer {
    //#region Variables

    private _express!: Router;

    private _io!: Server;
    private _nsp!: Namespace;

    private readonly socketMiddlewareWrap!: any;

    private readonly serverSecret!: string;
    private readonly functionServerVerifyAuthTokenUrl!: string;

    private readonly logManager!: LogManager;

    //#endregion

    //#region Constructor

    constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);

        const serverSecret =
            process.env.SERVER_SECRET || "";
        const functionServerVerifyAuthTokenUrl =
            process.env.FUNCTION_SERVER_VERIFY_AUTH_TOKEN_URL || "";

        this.serverSecret = serverSecret;
        this.functionServerVerifyAuthTokenUrl = functionServerVerifyAuthTokenUrl;

        this.socketMiddlewareWrap =
            (middleware: any) => (socket: any, next: any) => {
                middleware(socket.request, {}, next);
            };
    }

    //#endregion

    //#region Getters - Setters

    get express(): Router {
        return this._express;
    }

    get io(): Server {
        return this._io;
    }

    get nsp(): Namespace {
        return this._nsp;
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(
        httpServer: http.Server): Promise<void> {
        try {
            this.logManager.showLog({
                log: util.format("Socket server initialize start!")
            });

            this._express = express.Router();

            await this.startServer(
                httpServer);

            this.prepareUses();

            this.logManager.showLog({
                log: util.format("Socket server initialize success!")
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            this.logManager.showError({
                error: util.format("Socket server initialize error! Err: %s",
                    error)
            });

            throw new Error(
                util.format("Socket server initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Prepare Uses

    private prepareUses(): void {
        const bodyParserJsonOptions = {
            limit: "5mb"
        };
        this._express.use(bodyParser.json(bodyParserJsonOptions));

        const bodyParserUrlOptions = {
            limit: "5mb",
            extended: true,
            parameterLimit: 50000
        };
        this._express.use(bodyParser.urlencoded(bodyParserUrlOptions));

        const corsOptions = {
            origin: true
        };
        this._express.use(cors(corsOptions));

        this._express.use(cookieParser());
        this._express.use(helmet());
    }

    //#endregion

    //#region Start - Stop Server

    private async startServer(
        httpServer: http.Server,
        options?: ServerOptions): Promise<void> {
        this._io =
            new Server(httpServer, options);

        // this._io.adapter(redisAdapter);

        this._nsp =
            this._io.of('/lobbychat');

        // this._io.use(
        //     this.socketMiddlewareWrap(
        //         this.expressSession));

        this._nsp.use(
            async (socket, next) => {
                const token =
                    socket.handshake.auth.token;

                if (!token) {
                    const err =
                        new Error("not authorized");

                    return next(err);
                }

                const verifyTokenResponse =
                    await ReferenceController.authController.verifyToken(
                        token);

                if (!verifyTokenResponse.isSuccess) {
                    const err =
                        new Error("not authorized");

                    return next(err);
                }

                const payload =
                    verifyTokenResponse.payload;
                const user =
                    payload.user;
                const thirdPartyId =
                    user.thirdPartyId;

                if (!thirdPartyId) {
                    const err =
                        new Error("not authorized");

                    return next(err);
                }

                (socket as any).chatUserId = thirdPartyId;

                next();
            });

        EventHelper.instance.emit(SocketConstants.SOCKET_PREDEFINED_EVENTS_LISTENING, this._io, this._nsp);

        this._nsp.addListener(SocketConstants.SOCKET_PREDEFINED_EVENTS_CONNECTION,
            (socket) => {
                // this.logManager.showLog({
                //     log: util.format("Socket server connection!")
                // });

                EventHelper.instance.emit(SocketConstants.SOCKET_PREDEFINED_EVENTS_CONNECTION, socket);
            });
    }

    private stopServer(err?: string): void {
        try {
            this.logManager.showError({
                error: util.format("Socket server terminate! Error: %s",
                    err)
            });

            this._io.removeAllListeners();
            this._nsp.removeAllListeners();

            this._io.close();
        } catch (exception) {

        } finally {
            this.logManager.showError({
                error: util.format("Socket server stop! Time: %s",
                    new Date(Date.now()))
            });
        }
    }

    //#endregion

    //#endregion
}
