/**
 * Created by hulusionder on 29/09/2021.
 */

"use strict";

//#region Imports

import util from "util";
import express, {Router} from "express";
import helmet from "helmet";
import cors from "cors";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";

import {LogManager} from "../../manager/LogManager";
import {DecryptRequestBodyMiddleware} from "../../middleware/DecryptRequestBodyMiddleware";
import {EncryptResponseDataMiddleware} from "../../middleware/EncryptResponseDataMiddleware";
import {CheckAuthMiddleware} from "../../middleware/CheckAuthMiddleware";
import {SendResponseMiddleware} from "../../middleware/SendResponseMiddleware";
import {LobbyRequestHandler} from "../../requestHandler/LobbyRequestHandler";

//#endregion

export class LobbyServer {
    //#region Variables

    private _express!: Router;

    private readonly logManager!: LogManager;

    //#endregion

    //#region Getters - Setters

    get express(): Router {
        return this._express;
    }

    //#endregion

    //#region Constructor

    constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);
    }

    //#endregion

    //#region Methods

    //#region Init

    public init(): void {
        this.logManager.showLog({
            log: util.format("Lobby server initialize start!")
        });

        this._express = express.Router();

        this.prepareUses();
        this.prepareRequestMiddlewares();
        this.prepareRouters();
        this.prepareResponseMiddlewares();

        this.logManager.showLog({
            log: util.format("Lobby server initialize success!")
        });
    }

    //#endregion

    //#region Prepare Uses

    private prepareUses(): void {
        const bodyParserJsonOptions = {
            limit: "5mb"
        };
        this._express.use(bodyParser.json(bodyParserJsonOptions));

        const bodyParserUrlOptions = {
            limit: "5mb",
            extended: true,
            parameterLimit: 50000
        };
        this._express.use(bodyParser.urlencoded(bodyParserUrlOptions));

        const corsOptions = {
            origin: true
        };
        this._express.use(cors(corsOptions));

        this._express.use(cookieParser());
        this._express.use(helmet());
    }

    //#endregion

    //#region Prepare Middlewares

    private prepareRequestMiddlewares(): void {
        const decryptRequestBodyMiddleware = new DecryptRequestBodyMiddleware();
        this._express.use(
            (...args) => decryptRequestBodyMiddleware.use(...args));

        const checkAuthMiddleware = new CheckAuthMiddleware();
        this._express.use(
            (...args) => checkAuthMiddleware.use(...args));
    }

    private prepareResponseMiddlewares(): void {
        const encryptResponseDataMiddleware = new EncryptResponseDataMiddleware();
        this._express.use(
            (...args) => encryptResponseDataMiddleware.use(...args));

        const sendResponseMiddleware = new SendResponseMiddleware();
        this._express.use(
            (...args) => sendResponseMiddleware.use(...args));
    }

    //#endregion

    //#region Prepare Routers

    private prepareRouters(): void {
        const lobbyRequestHandler = new LobbyRequestHandler();
        lobbyRequestHandler.init();
        this._express.use(
            lobbyRequestHandler.expressRouter);
    }

    //#endregion

    //#endregion
}
