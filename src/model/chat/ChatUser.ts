/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {
    JsonProperty,
    Serializable,
    serialize
} from "typescript-json-serializer";

import {SerializableInterface} from "../../interface/common/SerializableInterface";

//#endregion

@Serializable()
export class ChatUser
    implements SerializableInterface {
    //#region Variables

    @JsonProperty()
    public id!: string;

    //#endregion

    //#region Constructor

    constructor(
        id: string) {
        this.id = id;
    }

    //#endregion

    //#region Methods

    //#region Serialize Methods

    public serialize<ChatUser>(
        removeUndefined?: boolean): ChatUser {
        return serialize(this, removeUndefined);
    }

    //#endregion

    //#endregion
}
