/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {JsonProperty, Serializable} from "typescript-json-serializer";

import {ClientChatMessageTypes} from "../../../constant/chat/ClientChatMessageTypes";
import {BaseChatRequestMessage} from "./BaseChatRequestMessage";

//#endregion

@Serializable()
export class LeaveChatRequestMessage
    extends BaseChatRequestMessage {
    //#region Variables

    @JsonProperty()
    public id!: string;

    //#endregion

    //#region Constructor

    constructor(
        chatMessageType: ClientChatMessageTypes,
        id: string) {
        super(chatMessageType);

        this.id = id;
    }

    //#endregion
}
