/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {JsonProperty, Serializable} from "typescript-json-serializer";

import {ClientChatMessageTypes} from "../../../constant/chat/ClientChatMessageTypes";
import {BaseChatRequestMessage} from "./BaseChatRequestMessage";

//#endregion

@Serializable()
export class SendMessageChatRequestMessage
    extends BaseChatRequestMessage {
    //#region Variables

    @JsonProperty()
    public id!: string;

    @JsonProperty()
    public message!: string;

    //#endregion

    //#region Constructor

    constructor(
        chatMessageType: ClientChatMessageTypes,
        id: string,
        message: string) {
        super(chatMessageType);

        this.id = id;

        this.message = message;
    }

    //#endregion
}
