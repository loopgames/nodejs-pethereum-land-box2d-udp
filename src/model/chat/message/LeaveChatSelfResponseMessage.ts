/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {JsonProperty, Serializable} from "typescript-json-serializer";

import {ServerReturnCodes} from "../../../constant/server/ServerReturnCodes";
import {ServerChatMessageTypes} from "../../../constant/chat/ServerChatMessageTypes";
import {BaseChatResponseMessage} from "./BaseChatResponseMessage";

//#endregion

@Serializable()
export class LeaveChatSelfResponseMessage
    extends BaseChatResponseMessage {
    //#region Variables

    @JsonProperty()
    public id?: string;

    //#endregion

    //#region Constructor

    constructor(
        isSuccess: boolean,
        returnCode: ServerReturnCodes,
        id?: string) {
        super(isSuccess, returnCode);

        this.chatMessageType = ServerChatMessageTypes.LeaveSelf;

        this.id = id;
    }

    //#endregion
}
