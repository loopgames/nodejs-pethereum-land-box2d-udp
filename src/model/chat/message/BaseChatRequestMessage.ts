/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {
    JsonProperty,
    Serializable,
    serialize
} from "typescript-json-serializer";

import {SerializableInterface} from "../../../interface/common/SerializableInterface";
import {ClientChatMessageTypes} from "../../../constant/chat/ClientChatMessageTypes";

//#endregion

@Serializable()
export class BaseChatRequestMessage
    implements SerializableInterface {
    //#region Variables

    @JsonProperty()
    public chatMessageType!: ClientChatMessageTypes;

    //#endregion

    //#region Constructor

    constructor(
        chatMessageType: ClientChatMessageTypes) {
        this.chatMessageType = chatMessageType;
    }

    //#endregion

    //#region Methods

    //#region Serialize Methods

    public serialize<BaseChatRequestMessage>(
        removeUndefined?: boolean): BaseChatRequestMessage {
        return serialize(this, removeUndefined);
    }

    //#endregion

    //#endregion
}
