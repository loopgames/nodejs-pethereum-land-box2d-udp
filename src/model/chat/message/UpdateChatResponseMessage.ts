/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {JsonProperty, Serializable} from "typescript-json-serializer";

import {ServerReturnCodes} from "../../../constant/server/ServerReturnCodes";
import {ServerChatMessageTypes} from "../../../constant/chat/ServerChatMessageTypes";
import {BaseChatResponseMessage} from "./BaseChatResponseMessage";
import {ChatMessagePayload} from "../ChatMessagePayload";

//#endregion

@Serializable()
export class UpdateChatResponseMessage
    extends BaseChatResponseMessage {
    //#region Variables

    @JsonProperty({type: ChatMessagePayload})
    public chatMessagePayload?: ChatMessagePayload;

    //#endregion

    //#region Constructor

    constructor(
        isSuccess: boolean,
        returnCode: ServerReturnCodes,
        chatMessagePayload?: ChatMessagePayload) {
        super(isSuccess, returnCode);

        this.chatMessageType = ServerChatMessageTypes.UpdateChat;

        this.chatMessagePayload = chatMessagePayload;
    }

    //#endregion
}
