/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {JsonProperty, Serializable} from "typescript-json-serializer";

import {ServerReturnCodes} from "../../../constant/server/ServerReturnCodes";
import {ServerChatMessageTypes} from "../../../constant/chat/ServerChatMessageTypes";
import {BaseChatResponseMessage} from "./BaseChatResponseMessage";
import {ChatUser} from "../ChatUser";

//#endregion

@Serializable()
export class JoinChatSelfResponseMessage
    extends BaseChatResponseMessage {
    //#region Variables

    @JsonProperty({type: ChatUser})
    public selfChatUser?: ChatUser;

    @JsonProperty({type: ChatUser})
    public otherChatUsers?: ChatUser[];

    //#endregion

    //#region Constructor

    constructor(
        isSuccess: boolean,
        returnCode: ServerReturnCodes,
        selfChatUser?: ChatUser,
        otherChatUsers?: ChatUser[]) {
        super(isSuccess, returnCode);

        this.chatMessageType = ServerChatMessageTypes.JoinSelf;

        this.selfChatUser = selfChatUser;
        this.otherChatUsers = otherChatUsers;
    }

    //#endregion
}
