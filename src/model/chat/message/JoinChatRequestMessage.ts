/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {JsonProperty, Serializable} from "typescript-json-serializer";

import {ClientChatMessageTypes} from "../../../constant/chat/ClientChatMessageTypes";
import {BaseChatRequestMessage} from "./BaseChatRequestMessage";

//#endregion

@Serializable()
export class JoinChatRequestMessage
    extends BaseChatRequestMessage {
    //#region Variables

    @JsonProperty()
    public id!: string;
    @JsonProperty()
    public name!: string;

    //#endregion

    //#region Constructor

    constructor(
        chatMessageType: ClientChatMessageTypes,
        id: string,
        name: string) {
        super(chatMessageType);

        this.id = id;
        this.name = name;
    }

    //#endregion
}
