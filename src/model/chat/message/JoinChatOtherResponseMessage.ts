/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {JsonProperty, Serializable} from "typescript-json-serializer";

import {ServerReturnCodes} from "../../../constant/server/ServerReturnCodes";
import {ServerChatMessageTypes} from "../../../constant/chat/ServerChatMessageTypes";
import {BaseChatResponseMessage} from "./BaseChatResponseMessage";
import {ChatUser} from "../ChatUser";

//#endregion

@Serializable()
export class JoinChatOtherResponseMessage
    extends BaseChatResponseMessage {
    //#region Variables

    @JsonProperty({type: ChatUser})
    public chatUser?: ChatUser;

    //#endregion

    //#region Constructor

    constructor(
        isSuccess: boolean,
        returnCode: ServerReturnCodes,
        chatUser?: ChatUser) {
        super(isSuccess, returnCode);

        this.chatMessageType = ServerChatMessageTypes.JoinOther;

        this.chatUser = chatUser;
    }

    //#endregion
}
