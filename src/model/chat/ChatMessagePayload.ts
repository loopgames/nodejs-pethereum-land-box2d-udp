/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {
    JsonProperty,
    Serializable,
    serialize
} from "typescript-json-serializer";

import {SerializableInterface} from "../../interface/common/SerializableInterface";

//#endregion

@Serializable()
export class ChatMessagePayload
    implements SerializableInterface {
    //#region Variables

    @JsonProperty()
    public id!: string;

    @JsonProperty()
    public lastMessage!: string;

    //#endregion

    //#region Constructor

    constructor(
        id: string,
        lastMessage: string) {
        this.id = id;

        this.lastMessage = lastMessage;
    }

    //#endregion

    //#region Methods

    //#region Serialize Methods

    public serialize<ChatMessagePayload>(
        removeUndefined?: boolean): ChatMessagePayload {
        return serialize(this, removeUndefined);
    }

    //#endregion

    //#endregion
}
