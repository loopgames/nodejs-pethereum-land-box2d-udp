/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {A2SServerData} from "../a2s/A2SServerData";
import {A2SRulesData} from "../a2s/A2SRulesData";
import {A2SPlayerData} from "../a2s/A2SPlayerData";

//#endregion

export class QueryProtocolData {
    //#region Variables

    public a2sServerInfo!: A2SServerData;

    public a2sServerRules!: A2SRulesData;

    public a2sPlayerInfo!: A2SPlayerData;

    //#endregion

    //#region Constructor

    constructor() {
        this.a2sServerInfo = new A2SServerData();

        this.a2sServerRules = new A2SRulesData();

        this.a2sPlayerInfo = new A2SPlayerData();
    }

    //#endregion

    //#region Methods

    //#endregion
}
