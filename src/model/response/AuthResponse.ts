/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {JsonProperty, Serializable} from "typescript-json-serializer";

import {ServerReturnCodes} from "../../constant/server/ServerReturnCodes";
import {BaseResponse} from "./BaseResponse";

//#endregion

@Serializable()
export class AuthResponse
    extends BaseResponse {
    //#region Variables

    @JsonProperty()
    public message?: string;

    //#endregion

    //#region Constructor

    constructor(
        isSuccess: boolean,
        returnCode: ServerReturnCodes,
        message?: string) {
        super(isSuccess, returnCode);

        this.message = message;
    }

    //#endregion
}
