/**
 * Created by hulusionder on 03/13/2022
 */

"use strict";

//#region Imports

import {JsonProperty, Serializable} from "typescript-json-serializer";

import {ServerReturnCodes} from "../../constant/server/ServerReturnCodes";
import {BaseResponse} from "./BaseResponse";

//#endregion

@Serializable()
export class LobbyResponse
    extends BaseResponse {
    //#region Variables

    @JsonProperty()
    public responseData?: any;

    //#endregion

    //#region Constructor

    constructor(
        isSuccess: boolean,
        returnCode: ServerReturnCodes,
        responseData?: any) {
        super(isSuccess, returnCode);

        this.responseData = responseData;
    }

    //#endregion
}
