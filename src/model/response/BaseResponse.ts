/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {
    JsonProperty,
    Serializable,
    serialize
} from "typescript-json-serializer";

import {SerializableInterface} from "../../interface/common/SerializableInterface";
import {ServerReturnCodes} from "../../constant/server/ServerReturnCodes";

//#endregion

@Serializable()
export abstract class BaseResponse
    implements SerializableInterface {
    //#region Variables

    @JsonProperty()
    public isSuccess!: boolean;

    @JsonProperty()
    public returnCode!: number;

    //#endregion

    //#region Constructor

    protected constructor(
        isSuccess: boolean,
        returnCode: ServerReturnCodes) {
        this.isSuccess = isSuccess;

        this.returnCode = returnCode;
    }

    //#endregion

    //#region Methods

    //#region Serialize Methods

    public serialize<BaseResponse>(
        removeUndefined?: boolean): BaseResponse {
        return serialize(this, removeUndefined);
    }

    //#endregion

    //#endregion
}
