/**
 * Created by hulusionder on 06/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import Redlock, {Lock} from "redlock";

import {LogManager} from "../../manager/LogManager";
import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {RedisLockResponseInterface} from "../../interface/redis/RedisLockResponseInterface";

//#endregion

export class RedisLock {
    //#region Variables

    private _lock!: Redlock;

    private readonly logManager!: LogManager;

    //#endregion

    //#region Getters - Setters

    get lock(): Redlock {
        return this._lock;
    }

    //#endregion

    //#region Constructor

    constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(
        clients: Redlock.CompatibleRedisClient[],
        options?: Redlock.Options): Promise<void> {
        try {
            this._lock = new Redlock(clients, options);

            this._lock.on("clientError", (error: any) => {
                this.logManager.showError({
                    error: util.format("Redis lock error! Err: %s",
                        error)
                });
            });
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            throw new Error(
                util.format("Redis lock initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Lock Methods

    //#region Acquire Lock Methods

    public async acquireLock(
        resources: string | string[],
        ttl: number)
        : Promise<RedisLockResponseInterface> {
        try {
            let lock =
                await this._lock.acquire(resources, ttl);

            return {
                isSuccess: true,
                lock,
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#region Release Lock Methods

    public async releaseLock(
        lock: Lock)
        : Promise<RedisLockResponseInterface> {
        try {
            await this._lock.release(lock);

            return {
                isSuccess: true,
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error,
            };
        }
    }

    //#endregion

    //#region Extend Lock Methods

    public async extendLock(
        lock: Lock,
        ttl: number)
        : Promise<RedisLockResponseInterface> {
        try {
            lock =
                await this._lock.extend(lock, ttl);

            return {
                isSuccess: true,
                lock,
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error,
            };
        }
    }

    //#endregion

    //#endregion

    //#endregion
}
