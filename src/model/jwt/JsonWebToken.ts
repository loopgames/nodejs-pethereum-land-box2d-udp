/**
 * Created by hulusionder on 02/16/2022
 */

"use strict";

//#region Imports

import jwt from "jsonwebtoken";

import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {JsonWebTokenResponseInterface} from "../../interface/jwt/JsonWebTokenResponseInterface";

//#endregion

export class JsonWebToken {
    //#region Variables

    private verifyPublicKeySecret!: string;

    //#endregion

    //#region Constructor

    constructor() {

    }

    //#endregion

    //#region Methods

    //#region Init

    public init(
        verifyPublicKeySecret: string): void {
        this.verifyPublicKeySecret = verifyPublicKeySecret;
    }

    //#endregion

    //#region Json Web Token Methods

    //#region Verify Methods

    public async verify(
        payload: any,
        options?: jwt.VerifyOptions | undefined): Promise<JsonWebTokenResponseInterface> {
        try {
            const verifyToken =
                jwt.verify(payload, this.verifyPublicKeySecret, options);

            if (!verifyToken) {
                return {
                    isSuccess: false,
                    error: "Cannot verify token"
                };
            }

            return {
                isSuccess: true,
                result: verifyToken
            };
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            return {
                isSuccess: false,
                error
            };
        }
    }

    //#endregion

    //#endregion

    //#endregion
}
