/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {
    JsonProperty,
    Serializable,
    serialize
} from "typescript-json-serializer";

import {SerializableInterface} from "../../interface/common/SerializableInterface";

//#endregion

@Serializable()
export class ServerJsonData
    implements SerializableInterface {
    //#region Variables

    @JsonProperty({required: false})
    public SessionId!: string;

    @JsonProperty({required: false})
    public SessionAuth!: string;

    @JsonProperty({required: false})
    public ConnectionIP!: string;
    @JsonProperty({required: false})
    public ConnectionPort!: string;

    //#endregion

    //#region Constructor

    constructor(
        allocationUUID: string,
        sessionAuth: string,
        connectionIP: string,
        connectionPort: string) {
        this.SessionId = allocationUUID;

        this.SessionAuth = sessionAuth;

        this.ConnectionIP = connectionIP;
        this.ConnectionPort = connectionPort;
    }

    //#endregion

    //#region Methods

    //#region Serialize Methods

    public serialize<ServerJsonData>(
        removeUndefined?: boolean): ServerJsonData {
        return serialize(this, removeUndefined);
    }

    //#endregion

    //#endregion
}
