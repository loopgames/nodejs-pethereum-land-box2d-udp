/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {
    JsonProperty,
    Serializable,
    serialize
} from "typescript-json-serializer";

import {SerializableInterface} from "../../interface/common/SerializableInterface";
import {Box2dBodyDefinitionData} from "./Box2dBodyDefinitionData";
import {Box2dFixtureDefinitionData} from "./Box2dFixtureDefinitionData";

//#endregion

@Serializable()
export class Box2dBodyData
    implements SerializableInterface {
    //#region Variables

    @JsonProperty({type: Box2dBodyDefinitionData})
    public box2DBodyDefinitionData!: Box2dBodyDefinitionData;
    @JsonProperty({type: Box2dFixtureDefinitionData})
    public box2DFixtureDefinitionDatas!: Box2dFixtureDefinitionData[];

    //#endregion

    //#region Constructor

    constructor(
        box2DBodyDefinitionData: Box2dBodyDefinitionData,
        box2DFixtureDefinitionDatas: Box2dFixtureDefinitionData[]) {
        this.box2DBodyDefinitionData = box2DBodyDefinitionData;
        this.box2DFixtureDefinitionDatas = box2DFixtureDefinitionDatas;
    }

    //#endregion

    //#region Methods

    //#region Serialize Methods

    public serialize<Box2dBodyData>(
        removeUndefined?: boolean): Box2dBodyData {
        return serialize(this, removeUndefined);
    }

    //#endregion

    //#endregion
}
