/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {
    JsonProperty,
    Serializable,
    serialize
} from "typescript-json-serializer";

import {SerializableInterface} from "../../interface/common/SerializableInterface";
import {Box2dShapeData} from "./Box2dShapeData";

//#endregion

@Serializable()
export class Box2dFixtureDefinitionData
    implements SerializableInterface {
    //#region Variables

    @JsonProperty()
    public isSensor!: boolean;

    @JsonProperty()
    public categoryBits!: number;
    @JsonProperty()
    public maskBits!: number;

    @JsonProperty()
    public density!: number;
    @JsonProperty()
    public friction!: number;
    @JsonProperty()
    public restitution!: number;

    @JsonProperty({type: Box2dShapeData})
    public box2DShapeData!: Box2dShapeData;

    //#endregion

    //#region Constructor

    constructor(
        isSensor: boolean,
        categoryBits: number,
        maskBits: number,
        density: number,
        friction: number,
        restitution: number,
        box2DShapeData: Box2dShapeData) {
        this.isSensor = isSensor;

        this.categoryBits = categoryBits;
        this.maskBits = maskBits;

        this.density = density;
        this.friction = friction;
        this.restitution = restitution;

        this.box2DShapeData = box2DShapeData;
    }

    //#endregion

    //#region Methods

    //#region Serialize Methods

    public serialize<Box2dFixtureDefinitionData>(
        removeUndefined?: boolean): Box2dFixtureDefinitionData {
        return serialize(this, removeUndefined);
    }

    //#endregion

    //#endregion
}
