/**
 * Created by hulusionder on 05/10/2021.
 */

"use strict";

//#region Imports

import util from "util";
import {
    b2Body,
    b2BodyDef,
    b2BodyType,
    b2CircleShape,
    b2ContactListener,
    b2DestructionListener,
    b2Fixture,
    b2FixtureDef,
    b2PolygonShape,
    b2Shape,
    b2ShapeType,
    b2Vec2,
    b2World,
} from "@box2d/core";

import {LogManager} from "../../manager/LogManager";
import {CommonLogicUtil} from "../../util/CommonLogicUtil";
import {Box2dBodyData} from "./Box2dBodyData";

//#endregion

export class Box2dWorld {
    //#region Variables

    private _world!: b2World;

    private readonly logManager!: LogManager;

    //#endregion

    //#region Getters - Setters

    get world(): b2World {
        return this._world;
    }

    //#endregion

    //#region Constructor

    constructor() {
        this.logManager =
            new LogManager(this.constructor.name, true);
    }

    //#endregion

    //#region Methods

    //#region Init

    public async init(
        worldJson?: any): Promise<void> {
        try {
            this._world =
                await this.createWorld(worldJson);
        } catch (exception) {
            const error =
                CommonLogicUtil.createErrorMessageFromException(exception);

            throw new Error(
                util.format("Box2d world initialize error! Err: %s",
                    error));
        }
    }

    //#endregion

    //#region Create World Methods

    public async createWorld(
        worldJson?: any): Promise<b2World> {
        this._world = b2World.Create(
            new b2Vec2(0, worldJson.gravity));

        this._world.SetAutoClearForces(true);
        this._world.SetAllowSleeping(worldJson.allowSleep);
        this._world.SetWarmStarting(worldJson.warmStarting);
        this._world.SetContinuousPhysics(worldJson.continuousPhysics);
        this._world.SetSubStepping(worldJson.subStepping);

        const destructionListener = new b2DestructionListener();
        this._world.SetDestructionListener(destructionListener);

        const contactListener = new b2ContactListener();
        this._world.SetContactListener(contactListener);

        const bodyDataArr = worldJson.bodyDatas;

        for (let i = 0; i < bodyDataArr.length; i++) {
            const bodyData = bodyDataArr[i];
            if (!bodyData) continue;

            this.createBodyFromBox2dBodyData(bodyData);
        }

        return this._world;
    }

    public createBodyFromBox2dBodyData(
        box2dBodyData: Box2dBodyData): b2Body {
        const box2dBodyDefinitionData = box2dBodyData.box2DBodyDefinitionData;

        const bodyDefinition = {
            position: new b2Vec2(box2dBodyDefinitionData.position.x, box2dBodyDefinitionData.position.y),
            angle: box2dBodyDefinitionData.angle,
            type: box2dBodyDefinitionData.bodyType,
            linearDamping: box2dBodyDefinitionData.linearDamping,
            angularDamping: box2dBodyDefinitionData.angularDamping,
            gravityScale: box2dBodyDefinitionData.gravityScale,
            fixedRotation: box2dBodyDefinitionData.fixedRotation,
            allowSleep: box2dBodyDefinitionData.allowSleep,
            awake: box2dBodyDefinitionData.awake
        }
        const body =
            this._world.CreateBody(bodyDefinition);

        let bodyFixtureDefinitionDataArr = box2dBodyData.box2DFixtureDefinitionDatas;

        for (let j = 0; j < bodyFixtureDefinitionDataArr.length; j++) {
            let bodyFixtureDefinitionData = bodyFixtureDefinitionDataArr[j];
            if (!bodyFixtureDefinitionData) continue;

            const box2dShapeData = bodyFixtureDefinitionData.box2DShapeData;
            const shapeTypeData = box2dShapeData.shapeType;

            let shape;
            if (shapeTypeData === b2ShapeType.e_circle) {
                shape = new b2CircleShape();

                shape.Set(
                    new b2Vec2(box2dShapeData.circlePosition.x, box2dShapeData.circlePosition.y),
                    box2dShapeData.circleRadius);
            } else {
                shape = new b2PolygonShape();

                const dataVerticesArr = box2dShapeData.polygonVertices;
                let verticesArr = [];

                for (let k = 0; k < dataVerticesArr.length; k++) {
                    let dataVertices = dataVerticesArr[k];
                    if (!dataVertices) continue;

                    verticesArr.push(
                        new b2Vec2(dataVertices.x, dataVertices.y));
                }

                shape.Set(verticesArr, verticesArr.length);
            }

            const fixtureDefinition: b2FixtureDef = {
                isSensor: bodyFixtureDefinitionData.isSensor,
                filter: {
                    categoryBits: bodyFixtureDefinitionData.categoryBits,
                    maskBits: bodyFixtureDefinitionData.maskBits
                },
                density: bodyFixtureDefinitionData.density,
                restitution: bodyFixtureDefinitionData.restitution,
                friction: bodyFixtureDefinitionData.friction,
                shape: shape
            }

            const bodyFixture =
                body.CreateFixture(fixtureDefinition);
        }

        return body;
    }

    public changeBodyShapeRadius(
        box2dBody: b2Body,
        radius: number): void {
        const fixtureList =
            box2dBody.GetFixtureList();

        if (fixtureList) {
            const shape =
                fixtureList.GetShape();

            shape.m_radius = radius;
        }
    }

    //#endregion

    //#endregion
}
