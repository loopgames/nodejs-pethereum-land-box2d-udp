/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {
    JsonProperty,
    Serializable,
    serialize
} from "typescript-json-serializer";

import {SerializableInterface} from "../../interface/common/SerializableInterface";

//#endregion

@Serializable()
export class Box2dShapeData
    implements SerializableInterface {
    //#region Variables

    @JsonProperty()
    public shapeType!: number;

    @JsonProperty()
    public circleRadius!: number;
    @JsonProperty()
    public circlePosition!: any;

    @JsonProperty()
    public polygonVertices!: any[];

    //#endregion

    //#region Constructor

    constructor(
        shapeType: number,
        circleRadius: number,
        circlePosition: any,
        polygonVertices: any[]) {
        this.shapeType = shapeType;

        this.circleRadius = circleRadius;
        this.circlePosition = circlePosition;

        this.polygonVertices = polygonVertices;
    }

    //#endregion

    //#region Methods

    //#region Serialize Methods

    public serialize<Box2dShapeData>(
        removeUndefined?: boolean): Box2dShapeData {
        return serialize(this, removeUndefined);
    }

    //#endregion

    //#endregion
}
