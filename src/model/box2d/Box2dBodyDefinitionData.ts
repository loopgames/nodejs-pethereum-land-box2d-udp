/**
 * Created by hulusionder on 07/10/2021.
 */

"use strict";

//#region Imports

import {
    JsonProperty,
    Serializable,
    serialize
} from "typescript-json-serializer";

import {SerializableInterface} from "../../interface/common/SerializableInterface";

//#endregion

@Serializable()
export class Box2dBodyDefinitionData
    implements SerializableInterface {
    //#region Variables

    @JsonProperty()
    public awake!: boolean;
    @JsonProperty()
    public allowSleep!: boolean;
    @JsonProperty()
    public fixedRotation!: boolean;

    @JsonProperty()
    public gravityScale!: number;
    @JsonProperty()
    public angularDamping!: number;
    @JsonProperty()
    public linearDamping!: number;
    @JsonProperty()
    public bodyType!: number;
    @JsonProperty()
    public angle!: number;

    @JsonProperty()
    public position!: any;

    //#endregion

    //#region Constructor

    constructor(
        awake: boolean,
        allowSleep: boolean,
        fixedRotation: boolean,
        gravityScale: number,
        angularDamping: number,
        linearDamping: number,
        bodyType: number,
        angle: number,
        position: any) {
        this.awake = awake;
        this.allowSleep = allowSleep;
        this.fixedRotation = fixedRotation;

        this.gravityScale = gravityScale;
        this.angularDamping = angularDamping;
        this.linearDamping = linearDamping;
        this.bodyType = bodyType;
        this.angle = angle;

        this.position = position;
    }

    //#endregion

    //#region Methods

    //#region Serialize Methods

    public serialize<Box2dBodyDefinitionData>(
        removeUndefined?: boolean): Box2dBodyDefinitionData {
        return serialize(this, removeUndefined);
    }

    //#endregion

    //#endregion
}
